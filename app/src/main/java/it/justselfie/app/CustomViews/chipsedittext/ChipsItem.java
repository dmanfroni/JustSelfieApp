package it.justselfie.app.CustomViews.chipsedittext;

public class ChipsItem {

	private String title;
	private int imageid;
	private String imageurl;


	public ChipsItem(String title,String imageurl){
		this.title = title;
		this.setImageurl(imageurl);
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getImageid() {
		return imageid;
	}
	
	@Override
	public String toString() {
		return getTitle();
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
}
