package it.justselfie.app.CustomViews.chipsedittext;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.justselfie.app.R;

public class ChipsAdapter extends BaseAdapter implements Filterable {

	private ArrayList<ChipsItem> amici;
	private ArrayList<ChipsItem> suggerimenti;
	private Context context;
	private LayoutInflater inflater;
	private String TAG = this.getClass().getSimpleName();

	public ChipsAdapter(Context context,ArrayList<ChipsItem> amici) {
		super();
		this.context = context;
		this.amici = amici;
		this.suggerimenti = new ArrayList<>();
	}

	public void removeItem(ChipsItem chipsItem){
		for(int i = 0; i < amici.size(); i++){
			if(amici.get(i).getTitle().equalsIgnoreCase(chipsItem.getTitle())){
				amici.remove(i);
				break;
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return suggerimenti.size();
	}

	@Override
	public ChipsItem getItem(int position) {
		return suggerimenti.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public String getImage(String title){
		String img = "";
		for(int i=0;i<amici.size();i++){
			if(amici.get(i).getTitle().trim().toLowerCase().startsWith(title.trim().toLowerCase())){
				img = amici.get(i).getImageurl();
				break;
			}
		}
		
		return img;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;
		View view = convertView;
		if (view == null) {
			if (inflater == null)
				inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.chips_adapter, null);
			vh = new ViewHolder();
			vh.img = (ImageView) view.findViewById(R.id.imageView1);
			vh.tv = (TextView) view.findViewById(R.id.textView1);

			view.setTag(vh);
		} else {
			vh = (ViewHolder) view.getTag();
		}

		Log.i(TAG, suggerimenti.get(position).getTitle() +  " = " + suggerimenti.get(position).getImageid());
		Picasso.with(context).load(suggerimenti.get(position).getImageurl()).into(vh.img);
		vh.tv.setText(suggerimenti.get(position).getTitle());

		return view;
	}

	class ViewHolder {
		TextView tv;
		ImageView img;
	}

	@Override
	public Filter getFilter() {
		return nameFilter;
	}

	Filter nameFilter = new Filter() {

		@Override
		public CharSequence convertResultToString(Object resultValue) {
			String str = ((ChipsItem) resultValue).getTitle();
			return str;
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults filterResults = new FilterResults();
			if (constraint != null) {
				suggerimenti.clear();
				try {
					for (int i = 0; i < amici.size(); i++) {
						if (amici.get(i).getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
							suggerimenti.add(amici.get(i));
						}
					}
				} catch (Exception e) {
				}
				filterResults.values = suggerimenti;
				filterResults.count = suggerimenti.size();
			}
			return filterResults;
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			if (results != null && results.count > 0) {
				notifyDataSetChanged();
			} else {
				notifyDataSetInvalidated();
			}

		}
	};

}
