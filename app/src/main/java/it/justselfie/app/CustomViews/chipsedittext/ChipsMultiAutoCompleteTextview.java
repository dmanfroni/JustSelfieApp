package it.justselfie.app.CustomViews.chipsedittext;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import it.justselfie.app.R;


public class ChipsMultiAutoCompleteTextview extends android.support.v7.widget.AppCompatMultiAutoCompleteTextView implements OnItemClickListener {

	private Context context;


	public ChipsMultiAutoCompleteTextview(Context context) {
		super(context);
		init(context);
	}
	public ChipsMultiAutoCompleteTextview(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	public ChipsMultiAutoCompleteTextview(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	public void init(Context context){
		setTokenizer(new CommaTokenizer());
		setOnItemClickListener(this);
		addTextChangedListener(textWather);
	}


	private TextWatcher textWather = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(count >=1){
				if(s.charAt(start) == ',')
					setChips();
			}
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
		@Override
		public void afterTextChanged(Editable s) {}
	};

	public void setChips(){
		if(getText().toString().contains(","))
		{
			
			SpannableStringBuilder ssb = new SpannableStringBuilder(getText());
			String chips[] = getText().toString().trim().split(",");
			int x =0;
			for(String c : chips){
				LayoutInflater lf = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
				LinearLayout layout = (LinearLayout) lf.inflate(R.layout.chips_edittext, null);
				TextView textView2 = (TextView) layout.findViewById(R.id.textView1);
				ImageView imageView = (ImageView) layout.findViewById(R.id.imageView1);
				Picasso.with(getContext()).load(((ChipsAdapter)getAdapter()).getImage(c)).into(imageView);
				textView2.setText(c);
				String image = ((ChipsAdapter) getAdapter()).getImage(c);
				int spec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
				layout.measure(spec, spec);
				layout.layout(0, 0, layout.getMeasuredWidth(), layout.getMeasuredHeight());
				Bitmap b = Bitmap.createBitmap(layout.getMeasuredWidth(), layout.getMeasuredHeight(),Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(b);
				canvas.translate(-layout.getScrollX(), -layout.getScrollY());
				layout.draw(canvas);
				layout.setDrawingCacheEnabled(true);
				Bitmap cacheBmp = layout.getDrawingCache();
				Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
				layout.destroyDrawingCache();
				BitmapDrawable bmpDrawable = new BitmapDrawable(getContext().getResources(), viewBmp);
				bmpDrawable.setBounds(0, 0,bmpDrawable.getIntrinsicWidth()/2,bmpDrawable.getIntrinsicHeight()/2);
				ssb.setSpan(new ImageSpan(bmpDrawable),x ,x + c.length() , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				x = x+ c.length() +1;
			}
			setText(ssb);
			setSelection(getText().length());
		}
		
		
	}
	
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ChipsItem ci = (ChipsItem) getAdapter().getItem(position);
		setChips();
	}
	
		
	
	
}
