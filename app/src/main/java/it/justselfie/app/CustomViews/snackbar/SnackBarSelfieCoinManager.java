package it.justselfie.app.CustomViews.snackbar;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * Created by Davide on 10/01/2017.
 */

public class SnackBarSelfieCoinManager {
    static final int MSG_TIMEOUT = 0;

    private static final int SHORT_DURATION_MS = 1500;
    private static final int LONG_DURATION_MS = 2750;

    private static SnackBarSelfieCoinManager sJustSelfieSnackBarManager;

    public static SnackBarSelfieCoinManager getInstance() {
        if (sJustSelfieSnackBarManager == null) {
            sJustSelfieSnackBarManager = new SnackBarSelfieCoinManager();
        }
        return sJustSelfieSnackBarManager;
    }

    private final Object mLock;
    private final Handler mHandler;

    private SnackBarSelfieCoinManager.JustSelfieSnackBarRecord mCurrentJustSelfieSnackBar;
    private SnackBarSelfieCoinManager.JustSelfieSnackBarRecord mNextJustSelfieSnackBar;

    private SnackBarSelfieCoinManager() {
        mLock = new Object();
        mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                switch (message.what) {
                    case MSG_TIMEOUT:
                        handleTimeout((SnackBarSelfieCoinManager.JustSelfieSnackBarRecord) message.obj);
                        return true;
                }
                return false;
            }
        });
    }

    interface Callback {
        void show();
        void dismiss(int event);
    }

    public void show(int duration, SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            if (isCurrentJustSelfieSnackBarLocked(callback)) {
                // Means that the callback is already in the queue. We'll just update the duration
                mCurrentJustSelfieSnackBar.duration = duration;

                // If this is the SnackBarCarrello currently being shown, call re-schedule it's
                // timeout
                mHandler.removeCallbacksAndMessages(mCurrentJustSelfieSnackBar);
                scheduleTimeoutLocked(mCurrentJustSelfieSnackBar);
                return;
            } else if (isNextJustSelfieSnackBarLocked(callback)) {
                // We'll just update the duration
                mNextJustSelfieSnackBar.duration = duration;
            } else {
                // Else, we need to create a new record and queue it
                mNextJustSelfieSnackBar = new SnackBarSelfieCoinManager.JustSelfieSnackBarRecord(duration, callback);
            }

            if (mCurrentJustSelfieSnackBar != null && cancelJustSelfieSnackBarLocked(mCurrentJustSelfieSnackBar,
                    SnackBarCarrello.Callback.DISMISS_EVENT_CONSECUTIVE)) {
                // If we currently have a SnackBarCarrello, try and cancel it and wait in line
                return;
            } else {
                // Clear out the current SnackBarCarrello
                mCurrentJustSelfieSnackBar = null;
                // Otherwise, just show it now
                showNextJustSelfieSnackBarLocked();
            }
        }
    }

    public void dismiss(SnackBarSelfieCoinManager.Callback callback, int event) {
        synchronized (mLock) {
            if (isCurrentJustSelfieSnackBarLocked(callback)) {
                cancelJustSelfieSnackBarLocked(mCurrentJustSelfieSnackBar, event);
            } else if (isNextJustSelfieSnackBarLocked(callback)) {
                cancelJustSelfieSnackBarLocked(mNextJustSelfieSnackBar, event);
            }
        }
    }

    /**
     * Should be called when a SnackBarCarrello is no longer displayed. This is after any exit
     * animation has finished.
     */
    public void onDismissed(SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            if (isCurrentJustSelfieSnackBarLocked(callback)) {
                // If the callback is from a SnackBarCarrello currently show, remove it and show a new one
                mCurrentJustSelfieSnackBar = null;
                if (mNextJustSelfieSnackBar != null) {
                    showNextJustSelfieSnackBarLocked();
                }
            }
        }
    }

    /**
     * Should be called when a SnackBarCarrello is being shown. This is after any entrance animation has
     * finished.
     */
    public void onShown(SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            if (isCurrentJustSelfieSnackBarLocked(callback)) {
                scheduleTimeoutLocked(mCurrentJustSelfieSnackBar);
            }
        }
    }

    public void cancelTimeout(SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            if (isCurrentJustSelfieSnackBarLocked(callback)) {
                mHandler.removeCallbacksAndMessages(mCurrentJustSelfieSnackBar);
            }
        }
    }

    public void restoreTimeout(SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            if (isCurrentJustSelfieSnackBarLocked(callback)) {
                scheduleTimeoutLocked(mCurrentJustSelfieSnackBar);
            }
        }
    }

    public boolean isCurrent(SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            return isCurrentJustSelfieSnackBarLocked(callback);
        }
    }

    public boolean isCurrentOrNext(SnackBarSelfieCoinManager.Callback callback) {
        synchronized (mLock) {
            return isCurrentJustSelfieSnackBarLocked(callback) || isNextJustSelfieSnackBarLocked(callback);
        }
    }

    private static class JustSelfieSnackBarRecord {
        final WeakReference<SnackBarSelfieCoinManager.Callback> callback;
        int duration;

        JustSelfieSnackBarRecord(int duration, SnackBarSelfieCoinManager.Callback callback) {
            this.callback = new WeakReference<>(callback);
            this.duration = duration;
        }

        boolean isJustSelfieSnackBar(SnackBarSelfieCoinManager.Callback callback) {
            return callback != null && this.callback.get() == callback;
        }
    }

    private void showNextJustSelfieSnackBarLocked() {
        if (mNextJustSelfieSnackBar != null) {
            mCurrentJustSelfieSnackBar = mNextJustSelfieSnackBar;
            mNextJustSelfieSnackBar = null;

            final SnackBarSelfieCoinManager.Callback callback = mCurrentJustSelfieSnackBar.callback.get();
            if (callback != null) {
                callback.show();
            } else {
                // The callback doesn't exist any more, clear out the SnackBarCarrello
                mCurrentJustSelfieSnackBar = null;
            }
        }
    }

    private boolean cancelJustSelfieSnackBarLocked(SnackBarSelfieCoinManager.JustSelfieSnackBarRecord record, int event) {
        final SnackBarSelfieCoinManager.Callback callback = record.callback.get();
        if (callback != null) {
            // Make sure we remove any timeouts for the JustSelfieSnackBarRecord
            mHandler.removeCallbacksAndMessages(record);
            callback.dismiss(event);
            return true;
        }
        return false;
    }

    private boolean isCurrentJustSelfieSnackBarLocked(SnackBarSelfieCoinManager.Callback callback) {
        return mCurrentJustSelfieSnackBar != null && mCurrentJustSelfieSnackBar.isJustSelfieSnackBar(callback);
    }

    private boolean isNextJustSelfieSnackBarLocked(SnackBarSelfieCoinManager.Callback callback) {
        return mNextJustSelfieSnackBar != null && mNextJustSelfieSnackBar.isJustSelfieSnackBar(callback);
    }

    private void scheduleTimeoutLocked(SnackBarSelfieCoinManager.JustSelfieSnackBarRecord r) {
        if (r.duration == SnackBarCarrello.LENGTH_INDEFINITE) {
            // If we're set to indefinite, we don't want to set a timeout
            return;
        }

        int durationMs = LONG_DURATION_MS;
        if (r.duration > 0) {
            durationMs = r.duration;
        } else if (r.duration == SnackBarCarrello.LENGTH_SHORT) {
            durationMs = SHORT_DURATION_MS;
        }
        mHandler.removeCallbacksAndMessages(r);
        mHandler.sendMessageDelayed(Message.obtain(mHandler, MSG_TIMEOUT, r), durationMs);
    }

    void handleTimeout(SnackBarSelfieCoinManager.JustSelfieSnackBarRecord record) {
        synchronized (mLock) {
            if (mCurrentJustSelfieSnackBar == record || mNextJustSelfieSnackBar == record) {
                cancelJustSelfieSnackBarLocked(record, SnackBarCarrello.Callback.DISMISS_EVENT_TIMEOUT);
            }
        }
    }
}
