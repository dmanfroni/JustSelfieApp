package it.justselfie.app.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import it.justselfie.app.App;
import it.justselfie.app.R;

/**
 * Created by davide on 01/03/17.
 */

public class CampaignReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = "http://justselfie.it/?";
        String source = null;
        String medium = null;
        String campaign = null;
        if(intent.hasExtra("referrer")) {
            referrer += intent.getStringExtra("referrer");
            Log.i("TRACK", referrer);
            referrer = referrer.replace("%26", "&");
            referrer = referrer.replace("%3D", "=");
            Uri uri = Uri.parse(referrer);
            source = uri.getQueryParameter("utm_source");
            campaign = uri.getQueryParameter("utm_campaign");
            medium = uri.getQueryParameter("utm_medium");
        }else{
            Log.i("TRACK", "Organic");
        }
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        Tracker tracker = analytics.newTracker(R.xml.global_tracker);
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Download_Android_" + ( (source != null && !source.isEmpty()) ? source : "Organic"))
                .setAction( (campaign != null && !campaign.isEmpty()) ? campaign : "Organic")
                .setLabel( (medium != null && !medium.isEmpty()) ? medium : "Organic")
                .build());

    }
}
