package it.justselfie.app.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.R;
import it.justselfie.app.Tools.PreferenceManager;
import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by Davide on 11/11/2016.
 */

public class NotificationService extends FirebaseMessagingService {

    public static String TAG = "gcm notification";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> payloadData = remoteMessage.getData();
            notifyUser(payloadData.get("titolo"), payloadData.get("testo"), Integer.parseInt(payloadData.get("view")), Integer.parseInt(payloadData.get("id")));
        }
    }


    public void notifyUser(String title, String message, int tipoView, int id) {

        Intent intent = new Intent(getApplicationContext(), JustSelfieActivity.class);
        intent.putExtra("view", tipoView);
        intent.putExtra("id", id);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.BigTextStyle inboxStyle =
                new NotificationCompat.BigTextStyle();

        inboxStyle.bigText(message);
        inboxStyle.setBigContentTitle(title);
        inboxStyle.setSummaryText("#ShareYourExperience");


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setColor(Color.parseColor("#F19004"))
                        .setSmallIcon(R.mipmap.icona_notifiche_no_cerchio)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(inboxStyle)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(uri);
        mBuilder.setVibrate(new long[]{1000, 1000});
        int count = PreferenceManager.getCountNotification(getApplicationContext());
        count++;
        PreferenceManager.setCountNotification(count, getApplicationContext());
        ShortcutBadger.applyCount(getApplicationContext(), count);
        notificationManager.notify(count, mBuilder.build());


    }


}
