package it.justselfie.app.Service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import it.justselfie.app.Tools.FCMManager;

/**
 * Created by Davide on 11/11/2016.
 */

public class InstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {


        FCMManager.setupRegistrationToken(getApplicationContext());
    }
}
