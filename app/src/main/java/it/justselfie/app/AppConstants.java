package it.justselfie.app;

import android.os.Environment;

/**
 * Created by sky on 2015/7/6.
 */
public class AppConstants {

    public static final int REQUEST_CODE_CARRELLO = 9662;
    public static final int REQUEST_CODE_PERMISSIONS = 9888;
    public static final int REQUEST_CODE_BLUETOOTH_PERMISSION = 329;
    public static final int REQUEST_CODE_CAMERA_SELFIE = 3349;
    public static final int REQUEST_CODE_CAMERA_SCONTO = 3649;
    public static final int REQUEST_CODE_CAMERA_ACQUISTO = 3249;
    public static final int REQUEST_CODE_FILTRI = 555;
    public static final String MD5_KEY = "Aiz238d34ddfsje2_4349";
    public static final int ACTION_HOME_VIEW = 1;
    public static final int ACTION_SCONTI_VIEW = 2;
    public static final int ACTION_SELFIES_VIEW = 3;
    public static final int ACTION_MERCHANT_VIEW = 4;
    public static final int ACTION_OFFER_VIEW = 5;
    public static final int ACTION_JSCARD_VIEW = 6;
    public static final int ACTION_OFFER_PENDING_VIEW = 69;
}
