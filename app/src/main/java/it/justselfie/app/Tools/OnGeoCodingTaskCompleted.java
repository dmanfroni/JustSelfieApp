package it.justselfie.app.Tools;

/**
 * Created by Davide on 26/06/2015.
 */
public interface OnGeoCodingTaskCompleted {

    public void onGeoCodingCompleted(String via, String civ);
    public void onLocationListener(double lat, double lon);
}
