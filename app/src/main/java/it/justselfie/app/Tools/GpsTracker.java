package it.justselfie.app.Tools;

/**
 * Created by Davide on 27/10/2014.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import it.justselfie.app.Activity.JustSelfieActivity;


public class GpsTracker extends Service implements LocationListener {

    private final JustSelfieActivity mContext;

    // flag for GPS status
    public boolean isGPSEnabled = false;

    private OnGeoCodingTaskCompleted onTaskCompleted;
    // flag for GPS status
    public boolean canGetLocation = false;

    public Location location; // location
    public double latitude; // latitude
    public double longitude; // longitude

    private String via;
    private int numCiv;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20;

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 15 * 1;

    // Declaring a Location Manager
    protected LocationManager locationManager;

    private ProgressDialog progress;

    public GpsTracker(JustSelfieActivity context, OnGeoCodingTaskCompleted onTaskCompleted) {
        this.mContext = context;
        this.onTaskCompleted = onTaskCompleted;
        this.latitude = 0.0;
        this.longitude = 0.0;
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = ((LocationManager) mContext.getSystemService(LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPSEnabled) {
            this.canGetLocation = true;
        }
    }

    public Location getLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.removeUpdates(GpsTracker.this);
        }
    }

    public void startUsingGPS() {
        progress = new ProgressDialog(mContext);
        progress.setMessage("Stiamo rilevando la tua posizione");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(true);
        progress.setOnDismissListener((d)->{
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(GpsTracker.this);
        });
        progress.setOnCancelListener((d)->{
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(GpsTracker.this);
        });
        progress.show();
        getLocation();
    }


    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("Impostazioni GPS");

        // Setting Dialog Message
        alertDialog.setMessage("GPS non abilitato. Vuoi andare alle impostazioni?\nSe si prosegue senza abilitazione, non sarà possibile attivare il servizio di geolocalizzazione");

        // On pressing Settings button
        alertDialog.setPositiveButton("Impostazioni", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((Activity) mContext).startActivityForResult(intent, 1);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        stopUsingGPS();
        progress.dismiss();
        onTaskCompleted.onLocationListener(latitude, longitude);


    }

    public void logger() {
        new GeoCoding().execute();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public int getNumCiv() {
        return numCiv;
    }

    public void setNumCiv(int numCiv) {
        this.numCiv = numCiv;
    }


    private class GeoCoding extends AsyncTask<Void, Void, String> {


        public GeoCoding() {
        }

        private String readStream(InputStream in) {
            String result = "";
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                /*String url = Costanti.URL_GEOREVERSECODING;
                url = url.replace("LAT_QUI", String.valueOf(latitude));
                url = url.replace("LNG_QUI", String.valueOf(longitude));
                URL uurl = new URL(url);
                HttpURLConnection con = (HttpURLConnection) uurl.openConnection();
                result = readStream(con.getInputStream());*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject objCiv = new JSONObject(result).getJSONArray("results").getJSONObject(0).getJSONArray("address_components").getJSONObject(0);
                JSONObject objVia = new JSONObject(result).getJSONArray("results").getJSONObject(0).getJSONArray("address_components").getJSONObject(1);
                onTaskCompleted.onGeoCodingCompleted(objCiv.getString("long_name"), objVia.getString("long_name"));
            } catch (Exception ex) {

            }

        }
    }

}