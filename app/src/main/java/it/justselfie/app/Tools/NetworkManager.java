package it.justselfie.app.Tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import it.justselfie.app.AppConstants;
import it.justselfie.app.R;

/**
 * Created by Davide on 16/04/2016.
 */
public class NetworkManager {

    public static String endpoint_utente_registrazioneJS = "/utente/registrazioneJS";
    public static String endpoint_utente_autenticazioneJS = "/utente/autenticazioneJS";
    public static String endpoint_utente_autenticazioneFB = "/utente/autenticazioneFB";
    public static String endpoint_utente_scheda = "/utente/scheda";
    public static String endpoint_utente_aggiorna = "/utente/aggiorna";
    public static String endpoint_utente_registraDevice = "/utente/registraDevice";
    public static String endpoint_utente_checkEmail = "/utente/checkEmail";
    public static String endpoint_utente_collegaFB = "/utente/collegaFB";
    public static String endpoint_utente_score = "/utente/score";
    public static String endpoint_esercente_lista = "/esercente/lista";
    public static String endpoint_esercente_scheda = "/esercente/scheda";
    public static String endpoint_esercente_categorie = "/categoria/lista";
    public static String endpoint_offerta_lista = "/offerta/lista";
    public static String endpoint_offerta_scheda = "/offerta/scheda";
    public static String endpoint_wall_lista = "/wall/lista";
    public static String endpoint_wall_scheda = "/wall/scheda";
    public static String endpoint_convalida_offerta = "/convalida/offerta";
    public static String endpoint_convalida_sconto = "/convalida/sconto";
    public static String endpoint_convalida_selfie = "/convalida/selfie";
    public static String endpoint_convalida_giftcard = "/convalida/giftcard";
    public static String endpoint_convalida_checktag = "/convalida/checkTag";
    public static String endpoint_wom_registra = "/wom/registra";
    public static String endpoint_utente_codicePromo = "/utente/codicePromo";



    public static boolean isNetworkAvaliable(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static String doApiPOSTRequest(Context context, String endpoint, String parameters){
        try {
            URL url = new URL(context.getString(R.string.url_radice) + endpoint);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(10000);
            conn.addRequestProperty("Content-Type", "application/json");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(parameters);
            writer.flush();
            String line;
            String oldLine = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                oldLine += line;
            }
            writer.close();
            reader.close();
            oldLine = oldLine.trim();
            return oldLine;
        }catch (Exception ex){
            return null;
        }
    }

    public static String doApiFORMPOSTRequest(Context context, String endpoint, String parameters){
        try {
            URL url = new URL(context.getString(R.string.url_radice) + endpoint);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(10000);
            conn.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(parameters);
            writer.flush();
            String line;
            String oldLine = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                oldLine += line;
            }
            writer.close();
            reader.close();
            oldLine = oldLine.trim();
            return oldLine;
        }catch (Exception ex){
            return null;
        }
    }

    public static String doApiGETRequest(Context context, String endpoint, String parameters){
        try {
            URL url = new URL(context.getString(R.string.url_radice) + endpoint + parameters);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(2000);
            String line;
            String oldLine = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                oldLine += line;
            }
            reader.close();
            oldLine = oldLine.trim();
            return oldLine;
        }catch (Exception ex){
            return null;
        }
    }


    public static String sStringToHMACMD5(String s)
    {
        String sEncodedString = null;
        try
        {
            SecretKeySpec key = new SecretKeySpec((AppConstants.MD5_KEY).getBytes("UTF-8"), "HmacMD5");
            Mac mac = Mac.getInstance("HmacMD5");
            mac.init(key);

            byte[] bytes = mac.doFinal(s.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();

            for (int i=0; i<bytes.length; i++) {
                String hex = Integer.toHexString(0xFF &  bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            sEncodedString = hash.toString();
        }
        catch (UnsupportedEncodingException e) {}
        catch(InvalidKeyException e){}
        catch (NoSuchAlgorithmException e) {}
        return sEncodedString ;
    }
}
