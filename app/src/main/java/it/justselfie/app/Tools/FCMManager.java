package it.justselfie.app.Tools;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;

public class FCMManager {

    public static void setupRegistrationToken(final Context context) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {

                    String token = FirebaseInstanceId.getInstance().getToken();
                    Utente utente = PreferenceManager.getUtente(context);
                    String deviceId = PreferenceManager.getUID(context);
                    sendRegistrationToServer(context, token, utente.getId(), deviceId);
                    PreferenceManager.setGCMRegistrationToken(token, context);
                } catch (Exception e) {

                }

                return null;
            }
        }.execute();

    }

    public static void sendRegistrationToServer(Context context, String token, int js_user_id, String device) {
        try {
            URL url = new URL(context.getString(R.string.url_radice) + NetworkManager.endpoint_utente_registraDevice);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write("idUtente=" + js_user_id
                    + "&tipoDispositivo=" + 2
                    + "&token=" + token
                    + "&idDevice=" + device);
            writer.flush();
            conn.connect();
            conn.getInputStream();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
