package it.justselfie.app.Tools;

import android.content.Context;
import android.provider.Settings;

import com.google.gson.Gson;

import it.justselfie.app.Model.Utente;

/**
 * Created by Davide on 04/04/2016.
 */
public class PreferenceManager {

    public static final String shared_preferences_name = "shared_name";
    public static final String fb_auth_token = "fb_auth_token";
    public static final String utente = "utente";
    public static final String shared_preferences_gcm_registration_token = "shared_gcm_registration_token";
    public static final String shared_preferences_latitudine = "latitudine";
    public static final String shared_preferences_longitudine = "longitudine";
    public static final String shared_preferences_show_request_friend_dialog = "shared_show_request_friend_dialog";
    public static final String shared_preferences_show_banner_selfie = "shared_show_banner_selfie";
    public static final String shared_preferences_show_banner_condivisione = "shared_show_banner_condivisione";
    public static final String shared_preferences_badge_counter = "badge_counter";


    public static void setLatitudine(double latutidine, Context activity){
        activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .edit().putString(shared_preferences_latitudine, latutidine + "").commit();
    }


    public static double getLatitudine(Context activity){
        return Double.parseDouble(activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .getString(shared_preferences_latitudine, "0.0"));
    }

    public static void setLongitudine(double longitudine, Context activity){
        activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .edit().putString(shared_preferences_longitudine, longitudine + "").commit();
    }


    public static double getLongitudine(Context activity){
        return Double.parseDouble(activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .getString(shared_preferences_longitudine, "0.0"));
    }



    public static void setUtente(Utente ut, Context activity){
        if(ut == null){
            activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                    .edit().putString(utente, "").apply();
        }else{
            activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                    .edit().putString(utente, new Gson().toJson(ut)).apply();
        }

    }


    public static Utente getUtente(Context activity){
        String s = activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE).getString(utente, "");
        if(s == ""){
            return null;
        }else{
            return new Gson().fromJson(s, Utente.class);
        }
    }


    public static void setGCMRegistrationToken(String token, Context activity){
        activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .edit().putString(shared_preferences_gcm_registration_token, token).commit();
    }


    public static void setShowFriendsRequestDialog(boolean agree, Context activity){
        activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .edit().putBoolean(shared_preferences_show_request_friend_dialog, agree).commit();
    }

    public static boolean getShowFriendsRequestDialog(Context activity){
        return activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .getBoolean(shared_preferences_show_request_friend_dialog, true);
    }

    public static void setShowBaloonSelfie(boolean agree, Context activity){
        activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .edit().putBoolean(shared_preferences_show_banner_selfie, agree).commit();
    }

    public static boolean getShowBaloonSelfie(Context activity){
        return activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .getBoolean(shared_preferences_show_banner_selfie, true);
    }

    public static void setShowBaloonCondivisione(boolean agree, Context activity){
        activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .edit().putBoolean(shared_preferences_show_banner_condivisione, agree).commit();
    }

    public static boolean getShowBaloonCondivisione(Context activity){
        return activity.getSharedPreferences(shared_preferences_name, Context.MODE_PRIVATE)
                .getBoolean(shared_preferences_show_banner_condivisione, true);
    }


    public static String getUID(Context context) {
        String m_deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return m_deviceId;
    }

    public static void setCountNotification(int count, Context context) {
        context.getSharedPreferences(shared_preferences_badge_counter, Context.MODE_PRIVATE).edit().putInt(shared_preferences_badge_counter, count).commit();
    }

    public static int getCountNotification(Context activity) {
        return activity.getSharedPreferences(shared_preferences_badge_counter, Context.MODE_PRIVATE).getInt(shared_preferences_badge_counter, 0);
    }


}
