package it.justselfie.app.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Activity.OffertaActivity;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.ConvalidaSelfie;
import it.justselfie.app.Model.Offerta;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 05/01/2017.
 */

public class AdapterOffertaAcquistabile extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private int ultimaPosizione = -1;
    public List<Offerta> elencoOfferte;
    public AppCompatActivity activity;
    private HashMap<Integer, Integer> quantitaSelezionate;
    private int[] screenSize;
    private double width, height;
    private String categoria;
    private boolean showSnackBar;


    public AdapterOffertaAcquistabile(AppCompatActivity activity, List<Offerta> elencoOfferte, double userLat, double userLon, String colore, boolean showSnackBar) {
        this.activity = activity;
        this.elencoOfferte = elencoOfferte;
        screenSize = Tools.getScreenSizePx(activity);
        width = (screenSize[0] - Tools.dpToPx(activity, 8));
        height = (width / 16) * 9;
        this.categoria = colore;
        setHashCollection();
        this.showSnackBar = showSnackBar;
    }

    public void setShowSnackBar(boolean showSnackBar){
        this.showSnackBar = showSnackBar;
        notifyDataSetChanged();
    }

    public boolean getShowSnackBar(){
        return this.showSnackBar;
    }

    protected void setHashCollection(){
        quantitaSelezionate = new HashMap<>();
        for(int i = 0; i < elencoOfferte.size(); i++){
            quantitaSelezionate.put(i, 0);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offerta_acquistabile, parent, false);
            return new OffertaAcquistabileVH(v);
        }else {
            View view = new View(activity);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Tools.dpToPx(activity, 64)));
            return new OffertaAcquistabileBottomSpaceVH(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position < elencoOfferte.size())
            ((OffertaAcquistabileVH)holder).doLayout(elencoOfferte.get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        return position < elencoOfferte.size() ? 1 : 0;
    }


    @Override
    public int getItemCount() {
        if (elencoOfferte == null)
            return 0;
        if(showSnackBar)
            return elencoOfferte.size() + 1;
        return elencoOfferte.size();
    }



    class OffertaAcquistabileVH extends RecyclerView.ViewHolder {


        private TextView nome;
        private TextView sconto;
        private RelativeLayout scontoSfondo;
        private TextView prezzoPieno;
        private TextView quantita;
        private TextView prezzoScontato;
        private View view;
        private ImageButton piuButton, menoButton;
        private ImageView immagine;

        public OffertaAcquistabileVH(View itemView) {
            super(itemView);
            view = itemView;
            nome = (TextView) itemView.findViewById(R.id.item_offerta_acquistabile_nome);
            sconto = (TextView) itemView.findViewById(R.id.item_offerta_acquistabile_sconto);
            scontoSfondo = (RelativeLayout) itemView.findViewById(R.id.item_offerta_acquistabile_sconto_sfondo);
            prezzoPieno = (TextView) itemView.findViewById(R.id.item_offerta_acquistabile_prezzo_pieno);
            prezzoScontato = (TextView) itemView.findViewById(R.id.item_offerta_acquistabile_prezzo_scontato);
            quantita = (TextView) itemView.findViewById(R.id.item_offerta_acquistabile_quantita);
            piuButton = (ImageButton) itemView.findViewById(R.id.item_offerta_acquistabile_piu_button);
            menoButton = (ImageButton) itemView.findViewById(R.id.item_offerta_acquistabile_meno_button);
            immagine = (ImageView)itemView.findViewById(R.id.item_offerta_acquistabile_immagine);
        }

        public void doLayout(final Offerta offerta, int position) {
            DecimalFormat f = new DecimalFormat("0.00");
            SpannableString scontoSpannable = new SpannableString("-" + String.valueOf(((int) offerta.getPercentualeSconto()) + "%"));
            scontoSpannable.setSpan(new RelativeSizeSpan(2f), 0, scontoSpannable.length(), 0);
            nome.setText(offerta.getNome());
            prezzoScontato.setText(f.format(offerta.getPrezzoScontato()) + "€");
            SpannableString text = new SpannableString(String.valueOf(f.format(offerta.getPrezzoPieno()) + "€"));
            text.setSpan(new StrikethroughSpan(), 0, text.length(), 0);
            prezzoPieno.setText(text, TextView.BufferType.SPANNABLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)width, (int)height);
            immagine.setLayoutParams(params);
            Picasso.with(activity).load(activity.getString(R.string.radiceImmagini) + offerta.getImmagine())
                        .centerCrop()
                        .fit().placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagine);
            immagine.setOnClickListener((v) -> {
                Intent intent = new Intent(activity, OffertaActivity.class);
                intent.putExtra("offerta", offerta);
                activity.startActivity(intent);
            });
            quantita.setText(quantitaSelezionate.get(position) + "");
            piuButton.setOnClickListener(view -> {
                int quantitaOra = quantitaSelezionate.get(position);
                quantitaOra++;
                quantita.setText(quantitaOra + "");
                quantitaSelezionate.put(position, quantitaOra);
                AcquistoItem acquistoItem = new AcquistoItem();
                acquistoItem.setQuantita(quantitaOra);
                acquistoItem.setOfferta(offerta);
                ((EsercenteActivity)activity).aggiornaAcquisto(acquistoItem);

            });
            menoButton.setOnClickListener(view -> {
                int quantitaOra = quantitaSelezionate.get(position);
                if(quantitaOra > 0) {
                    quantitaOra--;
                    quantita.setText(quantitaOra + "");
                    quantitaSelezionate.put(position, quantitaOra);
                    AcquistoItem acquistoItem = new AcquistoItem();
                    acquistoItem.setQuantita(quantitaOra);
                    acquistoItem.setOfferta(offerta);
                    ((EsercenteActivity)activity).aggiornaAcquisto(acquistoItem);
                }
            });
            sconto.setText(scontoSpannable);
            sconto.append("\nALLA CASSA");
            GradientDrawable bgShape = (GradientDrawable)scontoSfondo.getBackground();
            bgShape.setColor(Color.parseColor(categoria));
            setAnimation(view, position);
        }

        private void setAnimation(View viewToAnimate, int position) {
            if (position > ultimaPosizione) {
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_fromdown_toup);
                viewToAnimate.startAnimation(animation);
                ultimaPosizione = position;
            }
        }


    }

    class OffertaAcquistabileBottomSpaceVH extends RecyclerView.ViewHolder {
        public OffertaAcquistabileBottomSpaceVH(View view) {
            super(view);
        }
    }


}


