package it.justselfie.app.Adapter;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Model.Esercente;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.SortFilter;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 25/05/2016.
 */
public class AdapterEsercente extends RecyclerView.Adapter<AdapterEsercente.EsercenteVH> implements Filterable {

    private int ultimaPosizione = -1;
    public List<Esercente> elencoEsercenti;
    public List<Esercente> elencoEsercentiCopia;
    public AppCompatActivity activity;
    private double userLat, userLon;
    private int[] screenSize;

    public AdapterEsercente(AppCompatActivity activity, List<Esercente> elencoEsercenti, double lat, double lon) {
        this.userLat = lat;
        this.userLon = lon;
        this.activity = activity;
        this.elencoEsercenti = elencoEsercenti;
        this.elencoEsercentiCopia = elencoEsercenti;
        screenSize = Tools.getScreenSizePx(activity);
    }

    public void setElencoEsercenti(List<Esercente> elencoEsercente) {
        if(elencoEsercente != null) {
            this.elencoEsercenti = elencoEsercente;
            this.elencoEsercentiCopia = elencoEsercente;
            notifyDataSetChanged();
        }
    }

    public void setPosizioneUtente(double userLat, double userLon) {
        this.userLat = userLat;
        this.userLon = userLon;
    }

    @Override
    public EsercenteVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_esercente_2, parent, false);
        return new EsercenteVH(v);
    }

    @Override
    public void onBindViewHolder(EsercenteVH holder, int position) {
        holder.doLayout(elencoEsercentiCopia.get(position), position);

    }

    @Override
    public int getItemCount() {
        if (elencoEsercentiCopia == null)
            return 0;
        return elencoEsercentiCopia.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                elencoEsercentiCopia = (List<Esercente>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Esercente> risultatiFiltrati;
                if (constraint.length() == 0) {
                    risultatiFiltrati = elencoEsercenti;
                } else {
                    risultatiFiltrati = getFilteredResults(new Gson().fromJson(constraint.toString(), SortFilter.class));
                }

                FilterResults results = new FilterResults();
                results.values = risultatiFiltrati;

                return results;
            }
        };
    }

    protected List<Esercente> getFilteredResults(SortFilter sortFilter) {
        List<Esercente> risultati = new ArrayList<>();
        for (Esercente esercente : elencoEsercenti) {
            if(!sortFilter.getTesto().isEmpty()){
                if(!esercente.getNome().toLowerCase().contains(sortFilter.getTesto().toLowerCase()))
                    continue;
            }
            if(sortFilter.getCategorieSelezionate().containsKey(esercente.getCategoria().getId())){
                if(sortFilter.getCategorieSelezionate().get(esercente.getCategoria().getId()))
                    risultati.add(esercente);
            }else{
                risultati.add(esercente);
            }
        }
        return risultati;
    }


    class EsercenteVH extends RecyclerView.ViewHolder {


        private TextView nome, distanza, indirizzo, sconto;
        private ImageView immagineCopertina;
        private ImageView categoria;
        private View v;
        private View container;

        public EsercenteVH(View itemView) {
            super(itemView);
            v = itemView;
            container = itemView.findViewById(R.id.item_esercente_layout);
            nome = (TextView) itemView.findViewById(R.id.item_esercente_nome);
            immagineCopertina = (ImageView) itemView.findViewById(R.id.item_esercente_immagine_copertina);
            distanza = (TextView) itemView.findViewById(R.id.item_esercente_distanza);
            categoria = (ImageView) itemView.findViewById(R.id.item_esercente_categoria);
            indirizzo = (TextView) itemView.findViewById(R.id.item_esercente_indirizzo);
            sconto = (TextView) itemView.findViewById(R.id.item_esercente_sconto);
        }

        public void doLayout(final Esercente esercente, int position) {
            v.setOnClickListener(v1 -> {
                if (activity instanceof JustSelfieActivity) {
                    Ricerca ricerca = new Ricerca();
                    ricerca.setIdEsercente(esercente.getId());
                    Intent intent = new Intent(activity, EsercenteActivity.class);
                    intent.putExtra("ricerca", ricerca);
                    intent.putExtra("imgProf", esercente.getImmagineProfilo());
                    intent.putExtra("imgHead", esercente.getImmagineCopertina());
                    intent.putExtra("nome", esercente.getNome());
                    Pair<View, String> p1 = Pair.create((View) immagineCopertina, "immagineCopertina");
                    Pair<View, String> p2 = Pair.create((View) nome, "nomeEsercente");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, p1, p2);
                    ActivityCompat.startActivity(activity, intent, options.toBundle());
                }
            });
            sconto.setText("Sconti a partire dal " + esercente.getScontoMinimo() + "%");

            nome.setText(esercente.getNome());
            indirizzo.setText(esercente.getIndirizzo());
            Picasso.with(activity).load(activity.getString(R.string.radiceImmagini) + esercente.getCategoria().getIcona())
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.no_image)
                    .into(categoria);
            if (esercente.getImmagineCopertina() != null && !esercente.getImmagineCopertina().isEmpty())
                Picasso.with(activity)
                        .load(activity.getString(R.string.radiceImmagini) + esercente.getImmagineCopertina())
                        .fit()
                        .centerCrop()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.no_image)
                        .into(immagineCopertina);
            else
                Picasso.with(activity).load(R.mipmap.no_image).into(immagineCopertina);


            if (userLat != 0.0 && userLon != 0.0) {
                double dist = Double.parseDouble(Tools.distance(userLat, userLon, esercente.getLatitudine(), esercente.getLongitudine()));
                distanza.setText( dist + " Km");
            } else {
                distanza.setVisibility(View.GONE);
            }
            setAnimation(container, position);
        }

        private void setAnimation(View viewToAnimate, int position) {
            if (position > ultimaPosizione) {
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_fromdown_toup);
                viewToAnimate.startAnimation(animation);
                ultimaPosizione = position;
            }
        }


    }


}
