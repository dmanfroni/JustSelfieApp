package it.justselfie.app.Adapter;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Activity.WallActivity;
import it.justselfie.app.Fragments.WallFragment;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 07/04/2016.
 */
public class AdapterWall extends RecyclerView.Adapter<AdapterWall.WallItemViewHolder> {


    private final DisplayImageOptions options;
    public List<Selfie> elencoSelfie;
    public AppCompatActivity activity;
    private WallFragment.WallElencoFragment wallElencoFragment;

    public AdapterWall(AppCompatActivity activity, List<Selfie> elencoSelfie, WallFragment.WallElencoFragment wallElencoFragment) {
        this.wallElencoFragment = wallElencoFragment;
        this.activity = activity;
        this.elencoSelfie = elencoSelfie;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.placeholder)
                .showImageForEmptyUri(R.mipmap.no_image)
                .showImageOnFail(R.mipmap.no_image)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true).build();
    }


    @Override
    public WallItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wall, parent, false);
        return new WallItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(WallItemViewHolder holder, int position) {
        holder.doLayout(elencoSelfie.get(position));
    }

    @Override
    public int getItemCount() {
        return elencoSelfie.size();
    }

    class WallItemViewHolder extends RecyclerView.ViewHolder {


        private final ImageView imageViewProfilePicture;
        private final ImageView imageViewSelfie;
        private View container;

        public WallItemViewHolder(View view) {
            super(view);
            this.container = view;
            imageViewSelfie = (ImageView) view.findViewById(R.id.item_wall_selfie);
            imageViewProfilePicture = (ImageView) view.findViewById(R.id.item_wall_user);
        }

        public void doLayout(final Selfie selfie) {
            container.setOnClickListener(v1 -> {

                    Ricerca ricerca = new Ricerca();
                    ricerca.setIdSelfie(selfie.getId());
                    wallElencoFragment.vediScheda(ricerca);


            });
            int[] screnSize = Tools.getScreenSizePx(activity);
            int height = screnSize[0] / 3;
            ImageSize imageSize = new ImageSize(height, height);
            ImageLoader.getInstance().displayImage(activity.getString(R.string.radiceSelfie) + selfie.getSelfie(), imageViewSelfie, options);

          /*  ImageLoader imageLoader = ImageLoader.getInstance();
           /* imageLoader.loadImage(activity.getString(R.string.radiceSelfie) + selfie.getSelfie(), imageSize, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    imageViewSelfie.setImageBitmap(loadedImage);
                }
            });
            imageLoader.displayImage(activity.getString(R.string.radiceSelfie) + selfie.getSelfie(), options, imageViewSelfie);
*/
            if (selfie.getUtente() != null && selfie.getUtente().getImmagineProfilo() != null && !selfie.getUtente().getImmagineProfilo().isEmpty()) {
                Picasso.with(activity)
                        .load(selfie.getUtente().getImmagineProfilo()).centerCrop()
                        .fit().error(R.mipmap.no_image).placeholder(R.mipmap.placeholder).into(imageViewProfilePicture);
            }
            imageViewSelfie.setLayoutParams(new RelativeLayout.LayoutParams(height, height));

        }
    }


}


