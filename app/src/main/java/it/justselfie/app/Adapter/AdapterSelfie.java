package it.justselfie.app.Adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import it.justselfie.app.Activity.ConvalidaActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Model.ConvalidaSelfie;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;

/**
 * Created by dmanfroni on 29/07/2016.
 */
public class AdapterSelfie extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Object> selfieList;
    public AppCompatActivity activity;

    private final int HEADER = 1;
    private final int SELFIE = 2;
    private final int CONVALIDA_SELFIE = 3;

    public AdapterSelfie(AppCompatActivity activity, List<Object> selfieList) {
        this.activity = activity;
        this.selfieList = selfieList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case HEADER:
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
                viewHolder = new HeaderVH(v);
                break;
            case SELFIE:
                View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selfie, parent, false);
                viewHolder = new SelfieVH(v2);
                break;
            case CONVALIDA_SELFIE:
                View v3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selfie, parent, false);
                viewHolder = new ConvalidaSelfieVH(v3);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = selfieList.get(position);
        if (object instanceof Selfie) {
            ((SelfieVH) holder).doLayout(((Selfie) object));
        } else if (object instanceof String) {
            ((HeaderVH) holder).doLayout(((String) object));
        } else if (object instanceof ConvalidaSelfie) {
            ((ConvalidaSelfieVH) holder).doLayout(((ConvalidaSelfie) object));
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object object = selfieList.get(position);
        if (object instanceof Selfie) {
            return SELFIE;
        } else if (object instanceof String) {
            return HEADER;
        } else if (object instanceof ConvalidaSelfie) {
            return CONVALIDA_SELFIE;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return selfieList.size();
    }

    class SelfieVH extends RecyclerView.ViewHolder {

        private Button convalida;
        private TextView nomeEsercente;
        private TextView selfieCoin;
        private ImageView selfieImage;
        private View v;

        public SelfieVH(View itemView) {
            super(itemView);
            v = itemView;
            convalida = (Button) v.findViewById(R.id.item_selfie_convalida);
            nomeEsercente = (TextView) v.findViewById(R.id.item_selfie_esercente_nome);
            selfieCoin = (TextView) v.findViewById(R.id.item_selfie_selfiecoin);
            selfieImage = (ImageView) v.findViewById(R.id.item_selfie_selfie);


        }

        public void doLayout(final Selfie selfie) {
            nomeEsercente.setText(selfie.getEsercente().getNome());
            double sc = selfie.getEsercente().getContratto().getSelfieCoin();
            if (selfie.getTipoCondivisione() == 2) {
                sc = ((sc / 100) * (100 - selfie.getEsercente().getContratto().getSelfieCoinRiduzioneNoLink()));

            }
            selfieCoin.setText("Riscuoti " + (int) sc + " selfie coin");
            Picasso.with(activity).load(activity.getString(R.string.radiceSelfie) + selfie.getSelfie()).resize(200, 200).into(selfieImage);
            convalida.setOnClickListener((v) -> {
                selfie.setUtente(PreferenceManager.getUtente(activity));
                ConvalidaSelfie convalidaSelfie = new ConvalidaSelfie();
                convalidaSelfie.setSelfie(selfie);
                Intent intent = new Intent(activity, ConvalidaActivity.class);
                intent.putExtra("convalidaSelfie", convalidaSelfie);
                activity.startActivity(intent);
            });


        }


    }

    class ConvalidaSelfieVH extends RecyclerView.ViewHolder {
        private Button convalida;
        private TextView nomeEsercente;
        private TextView selfieCoin;
        private ImageView selfieImage;
        private View v;

        public ConvalidaSelfieVH(View itemView) {
            super(itemView);
            v = itemView;
            convalida = (Button) v.findViewById(R.id.item_selfie_convalida);
            nomeEsercente = (TextView) v.findViewById(R.id.item_selfie_esercente_nome);
            selfieCoin = (TextView) v.findViewById(R.id.item_selfie_selfiecoin);
            selfieImage = (ImageView) v.findViewById(R.id.item_selfie_selfie);


        }

        public void doLayout(final ConvalidaSelfie selfie) {
            nomeEsercente.setText(selfie.getSelfie().getEsercente().getNome());
            double sc = selfie.getSelfie().getEsercente().getContratto().getSelfieCoin();
            if (selfie.getSelfie().getTipoCondivisione() == 2) {
                sc = ((sc / 100) * (100 - selfie.getSelfie().getEsercente().getContratto().getSelfieCoinRiduzioneNoLink()));

            }
            selfieCoin.setText("Convalida e riscuoti " + (int) sc + " selfie coin");
            Picasso.with(activity).load(activity.getString(R.string.radiceSelfie) + selfie.getSelfie()).resize(200, 200).into(selfieImage);
            convalida.setOnClickListener((v) -> {
                Intent intent = new Intent(activity, ConvalidaActivity.class);
                intent.putExtra("convalidaSelfie", selfie);
                activity.startActivity(intent);
            });
            Picasso.with(activity).load(new File(selfie.getSelfie().getSelfie())).into(selfieImage);
            selfieImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }
    }

    class HeaderVH extends RecyclerView.ViewHolder {

        private TextView header;
        private View v;

        public HeaderVH(View itemView) {
            super(itemView);
            v = itemView;
            header = (TextView) itemView.findViewById(R.id.item_header_text);
        }

        public void doLayout(String string) {
            header.setText(string);
        }


    }
}

