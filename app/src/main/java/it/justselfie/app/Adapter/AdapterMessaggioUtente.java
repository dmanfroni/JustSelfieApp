package it.justselfie.app.Adapter;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import it.justselfie.app.Model.MessaggioUtente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 30/10/2016.
 */

public class AdapterMessaggioUtente extends RecyclerView.Adapter<AdapterMessaggioUtente.MsgUtenteVH> {


    public List<MessaggioUtente> messaggiUtente;
    public AppCompatActivity activity;

    public AdapterMessaggioUtente(AppCompatActivity activity, List<MessaggioUtente> messaggiUtente) {
        this.activity = activity;
        this.messaggiUtente = messaggiUtente;

    }

    @Override
    public MsgUtenteVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_messaggio_utente, parent, false);
        return new MsgUtenteVH(v);
    }

    @Override
    public void onBindViewHolder(MsgUtenteVH holder, int position) {
        if((messaggiUtente == null || messaggiUtente.size() == 0) &&position == 0) holder.doErrore();
        else holder.doLayout(messaggiUtente.get(position));
    }

    @Override
    public int getItemCount() {
        if(messaggiUtente == null || messaggiUtente.size() == 0){
            return 1;
        }
        return messaggiUtente.size();
    }

    class MsgUtenteVH extends RecyclerView.ViewHolder {

        private TextView messaggi;

        private ImageView imageView;
        public MsgUtenteVH(View itemView) {
            super(itemView);
            messaggi = (TextView) itemView.findViewById(R.id.item_messaggio_utente_testo);
            imageView = (ImageView) itemView.findViewById(R.id.item_messaggio_tipo);
        }

        public void doLayout(MessaggioUtente messaggioUtente) {
           messaggi.setText(Tools.getDataTimeDisplay(messaggioUtente.getDataMessaggio()) + " - " + messaggioUtente.getMessaggio());
            if(messaggioUtente.getLivello() == 0){
                imageView.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.transazione_negativa));
            }else{
                imageView.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.transazione_positiva));
            }
        }

        public void doErrore() {
            messaggi.setText("Non hai nessun messaggio");

        }
    }
}



