package it.justselfie.app.Adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import it.justselfie.app.Activity.ConvalidaActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Model.ConvalidaSconto;
import it.justselfie.app.Model.Sconto;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 06/07/2016.
 */
public class AdapterSconto extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Object> elencoSconti;
    public AppCompatActivity activity;
    private final int HEADER = 1;
    private final int SCONTO = 2;
    private final int CONVALIDA_SCONTO = 3;


    public AdapterSconto(AppCompatActivity activity, List<Object> elencoSconti) {
        this.activity = activity;
        this.elencoSconti = elencoSconti;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case HEADER:
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
                viewHolder = new HeaderVH(v);
                break;
            case SCONTO:
                View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sconto, parent, false);
                viewHolder = new ScontoVH(v2);
                break;
            case CONVALIDA_SCONTO:
                View v3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sconto, parent, false);
                viewHolder = new ConvalidaScontoVH(v3);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = elencoSconti.get(position);
        if (object instanceof Sconto){
            ((ScontoVH)holder).doLayout(((Sconto) object));
        }else if(object instanceof String){
            ((HeaderVH)holder).doLayout(((String)object));
        }else if(object instanceof ConvalidaSconto){
            ((ConvalidaScontoVH)holder).doLayout(((ConvalidaSconto) object));
        }
    }

    @Override
    public int getItemViewType(int position){
        Object object = elencoSconti.get(position);
        if (object instanceof Sconto){
            return SCONTO;
        }else if(object instanceof String){
            return HEADER;
        }else {
            return  CONVALIDA_SCONTO;
        }
    }

    @Override
    public int getItemCount() {
        return elencoSconti.size();
    }

    class ScontoVH extends RecyclerView.ViewHolder {

        private Button riscuoti;
        private TextView validitaSconto;
        private TextView nomeEsercenteSconto;


        private View v;

        public ScontoVH(View itemView) {
            super(itemView);
            v = itemView;
            riscuoti = (Button) itemView.findViewById(R.id.item_sconto_riscuoti_button);
            nomeEsercenteSconto = (TextView) itemView.findViewById(R.id.item_sconto_esercente);
            validitaSconto = (TextView) itemView.findViewById(R.id.item_sconto_validita);
        }

        public void doLayout(final Sconto sconto) {
            riscuoti.setOnClickListener(view -> {
               if(activity instanceof JustSelfieActivity){
                   ((JustSelfieActivity)activity).startCameraSconto(sconto);
               }
            });
            validitaSconto.append(" " + Tools.getDataTimeDisplay(sconto.getDataScadenza()));
            nomeEsercenteSconto.setText(sconto.getSconto() + "% di sconto presso " + sconto.getEsercente().getNome());

        }


    }

    class ConvalidaScontoVH extends RecyclerView.ViewHolder {

        private Button riscuoti;
        private TextView validitaSconto;
        private TextView nomeEsercenteSconto;
        private ImageView immagineSelfie;


        private View v;

        public ConvalidaScontoVH(View itemView) {
            super(itemView);
            v = itemView;
            riscuoti = (Button) itemView.findViewById(R.id.item_sconto_riscuoti_button);
            nomeEsercenteSconto = (TextView) itemView.findViewById(R.id.item_sconto_esercente);
            validitaSconto = (TextView) itemView.findViewById(R.id.item_sconto_validita);
            immagineSelfie = (ImageView) itemView.findViewById(R.id.item_sconto_immagine);
        }

        public void doLayout(final ConvalidaSconto convalidaSconto) {
            riscuoti.setOnClickListener(view -> {
                Intent intent = new Intent(activity, ConvalidaActivity.class);
                intent.putExtra("convalidaSconto", convalidaSconto);
                activity.startActivity(intent);
            });
            riscuoti.setText("CONVALIDA");
            validitaSconto.append(Tools.getDataTimeDisplay(convalidaSconto.getSconto().getDataScadenza()));
            nomeEsercenteSconto.setText(convalidaSconto.getSconto().getSconto() + "% di sconto presso " + convalidaSconto.getSconto().getEsercente().getNome());

            Picasso.with(activity).load(new File(convalidaSconto.getSelfie().getSelfie())).into(immagineSelfie);
            immagineSelfie.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }


    }

    class HeaderVH extends RecyclerView.ViewHolder {

        private TextView header;
        private View v;

        public HeaderVH(View itemView) {
            super(itemView);
            v = itemView;
            header = (TextView) itemView.findViewById(R.id.item_header_text);
        }

        public void doLayout(String string) {
            header.setText(string);
        }


    }

}

