package it.justselfie.app.Adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.justselfie.app.Activity.CarrelloActivity;
import it.justselfie.app.Activity.ConvalidaActivity;
import it.justselfie.app.Database.DatabaseHelper;
import it.justselfie.app.Database.QueryBuilder;
import it.justselfie.app.Model.Acquisto;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.ConvalidaOfferta;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;

/**
 * Created by Davide on 13/11/2016.
 */

public class AdapterConvalidaOfferta extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public List<ConvalidaOfferta> acquisti;
    public AppCompatActivity activity;

    public AdapterConvalidaOfferta(AppCompatActivity activity, List<ConvalidaOfferta> acquisti) {
        this.activity = activity;
        this.acquisti = acquisti;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConvalidaOffertaVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_convalida_offerta, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((ConvalidaOffertaVH) holder).doLayout(acquisti.get(position));
    }

    @Override
    public int getItemCount() {
        return acquisti.size();
    }


    class ConvalidaOffertaVH extends RecyclerView.ViewHolder {


        private final ImageView immagine;
        private final TextView ricapitoloAcquisto;
        private final TextView ricapitoloTag;
        private final TextView importoTotale;
        private final Button convalidaButton;
        private final Button eliminaButton;

        public ConvalidaOffertaVH(View view) {
            super(view);
            immagine = (ImageView) view.findViewById(R.id.item_convalida_offerta_immagine);
            ricapitoloAcquisto = (TextView) view.findViewById(R.id.item_convalida_offerta_ricapitolo_acquisto);
            importoTotale = (TextView) view.findViewById(R.id.item_convalida_offerta_importo);
            ricapitoloTag = (TextView) view.findViewById(R.id.item_convalida_offerta_ricapitolo_tag);
            convalidaButton = (Button) view.findViewById(R.id.item_convalida_button);
            eliminaButton = (Button) view.findViewById(R.id.item_convalida_elimina_button);
        }

        public void doLayout(final ConvalidaOfferta convalidaOfferta) {
            if (!convalidaOfferta.getSelfie().getSelfie().isEmpty())
                Picasso.with(activity).load(new File(convalidaOfferta.getSelfie().getSelfie())).into(immagine);
            String recapOfferte = "Hai acquisto";
            int quantitaTotale = 0;
            double importo = 0.0;
            for(int i = 0; i < convalidaOfferta.getAcquisto().getOfferteAcquistate().size(); i++){
                quantitaTotale = quantitaTotale + convalidaOfferta.getAcquisto().getOfferteAcquistate().get(i).getQuantita();
                importo = importo + ( convalidaOfferta.getAcquisto().getOfferteAcquistate().get(i).getOfferta().getPrezzoScontato() * quantitaTotale);
            }
            recapOfferte += " " + quantitaTotale + " offerte presso " + convalidaOfferta.getSelfie().getEsercente().getNome();
            ricapitoloAcquisto.setText(recapOfferte);
            String recapTag = "";
            List<Utente> tags = new ArrayList<>();
            tags.addAll(convalidaOfferta.getSelfie().getAmiciTaggatiFB());
            tags.addAll(convalidaOfferta.getUtentiTagOfferta());

            if(tags.size() == 0){
                recapTag = "Non hai taggato nessun amico";
            }
            if(tags.size() >= 1){
                recapTag = "Con " + tags.get(0).getNome();
            }
            if(tags.size() > 1){
                recapTag += " ed altre " + (tags.size()-1) + " persone.";
            }
            ricapitoloTag.setText(recapTag);
            DecimalFormat f = new DecimalFormat("0.00");
            importoTotale.setText("EUR " + f.format(importo));

            convalidaButton.setOnClickListener((v) -> {
                Intent intent = new Intent(activity, ConvalidaActivity.class);
                intent.putExtra("convalidaOfferta", convalidaOfferta);
                activity.startActivity(intent);
            });
            eliminaButton.setOnClickListener(view -> {
                new AlertDialog.Builder(activity)
                        .setTitle("Attenzione")
                        .setMessage("Sei sicuro di voler rimuovere l'acquisto?")
                        .setPositiveButton("Ok", (dialogInterface, i) -> {
                            acquisti.remove(convalidaOfferta);
                            DatabaseHelper db = new DatabaseHelper(activity);
                            db.delete(QueryBuilder.tabellaAcquisti, convalidaOfferta.getId());
                            db.close();
                            notifyDataSetChanged();
                        })
                        .setNegativeButton("Annulla", (dialogInterface, i) -> dialogInterface.dismiss()).create().show();

            });
        }


    }


}


