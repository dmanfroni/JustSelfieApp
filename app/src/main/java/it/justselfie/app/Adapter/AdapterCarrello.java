package it.justselfie.app.Adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.justselfie.app.Activity.CarrelloActivity;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.R;

/**
 * Created by Davide on 09/11/2016.
 */

public class AdapterCarrello extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<Object> elementi;
    public AppCompatActivity activity;
    private HashMap<Integer, Integer> quantitaSelezionate;
    private final int OMAGGIO = 1;
    private final int PRODOTTO = 2;

    public AdapterCarrello(AppCompatActivity activity, List<AcquistoItem> acquisti) {
        this.activity = activity;
        this.elementi = new ArrayList<>();
        this.elementi.addAll(acquisti);
        setHashCollection();
    }

    protected void setHashCollection() {
        quantitaSelezionate = new HashMap<>();
        for (int i = 0; i < elementi.size(); i++) {
            if(elementi.get(i) instanceof AcquistoItem)
                quantitaSelezionate.put(i, ((AcquistoItem)elementi.get(i)).getQuantita());
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        /*switch (viewType){
            case OMAGGIO:
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carrello_omaggio, parent, false);
                viewHolder = new OmaggioVH(v);
                break;
            case PRODOTTO:

                break;

        }*/
        View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carrello, parent, false);
        viewHolder = new CarreloVH(v2);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = this.elementi.get(position);
        if(object instanceof AcquistoItem){
            ((CarreloVH)holder).doLayout(((AcquistoItem) object), position);
        }
    }

    @Override
    public int getItemCount() {
        return this.elementi.size();
    }

    @Override
    public int getItemViewType(int position){
        Object object = this.elementi.get(position);
        if (object instanceof AcquistoItem){
            return PRODOTTO;
        }else if(object instanceof Integer){
            return OMAGGIO;
        }else {
            return  PRODOTTO;
        }
    }


    class CarreloVH extends RecyclerView.ViewHolder {


        private final ImageView immagine;
        private final TextView nome;
        private TextView quantita;
        private TextView importo;
        private ImageButton piuButton;
        private ImageButton menoButton;
        private ImageButton rimuovi;

        public CarreloVH(View view) {
            super(view);
            immagine = (ImageView) view.findViewById(R.id.item_carrello_immagine);
            nome = (TextView) view.findViewById(R.id.item_carrello_nome_offerta);
            quantita = (TextView) view.findViewById(R.id.item_carrello_quantita);
            importo = (TextView) view.findViewById(R.id.item_carrello_importo_offerta);
            piuButton = (ImageButton) view.findViewById(R.id.item_carrello_piu_button);
            menoButton = (ImageButton) view.findViewById(R.id.item_carrello_meno_button);
            rimuovi = (ImageButton) view.findViewById(R.id.item_carrello_elimina_button);
        }

        public void doLayout(final AcquistoItem acquistoItem, int position) {
            if (acquistoItem.getOfferta().getImmagine() != null && !acquistoItem.getOfferta().getImmagine().isEmpty())
                Picasso.with(activity).load(activity.getString(R.string.radiceImmagini) + acquistoItem.getOfferta().getImmagine()).into(immagine);
            nome.setText(acquistoItem.getOfferta().getNome());
            int quantitaParziale = quantitaSelezionate.get(position);
            DecimalFormat f = new DecimalFormat("0.00");
            piuButton.setOnClickListener(view -> {
                int quantitaOra = quantitaSelezionate.get(position);
                quantitaOra++;
                quantita.setText(quantitaOra + "");
                quantitaSelezionate.put(position, quantitaOra);
                acquistoItem.setQuantita(quantitaOra);
                ((CarrelloActivity) activity).aggiornaAcquisto(acquistoItem);
                importo.setText(f.format(acquistoItem.getOfferta().getPrezzoScontato() * quantitaOra) + "€");
            });
            quantita.setText(quantitaParziale + "");
            importo.setText(f.format(acquistoItem.getOfferta().getPrezzoScontato() * quantitaParziale) + "€");
            menoButton.setOnClickListener(view -> {
                int quantitaOra = quantitaSelezionate.get(position);
                if (quantitaOra > 1) {
                    quantitaOra--;
                    quantita.setText(quantitaOra + "");
                    quantitaSelezionate.put(position, quantitaOra);
                    acquistoItem.setQuantita(quantitaOra);
                    ((CarrelloActivity) activity).aggiornaAcquisto(acquistoItem);
                    importo.setText(f.format(acquistoItem.getOfferta().getPrezzoScontato() * quantitaOra) + "€");
                }
            });
            rimuovi.setOnClickListener(view -> {
                ((CarrelloActivity) activity).rimuoviAcquisto(acquistoItem);
            });
        }


    }




}



