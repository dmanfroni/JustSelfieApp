package it.justselfie.app.Adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Activity.WallActivity;
import it.justselfie.app.Model.Categoria;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 06/01/2017.
 */

public class AdapterCategoriaSelezionabili  extends RecyclerView.Adapter<AdapterCategoriaSelezionabili.CategoriaVH> {


    public List<Categoria> elencoCategorie;
    public AppCompatActivity activity;
    private HashMap<Integer, Boolean> categorieSelezionate;

    public AdapterCategoriaSelezionabili(AppCompatActivity activity, List<Categoria> elencoCategorie, HashMap<Integer, Boolean> categorieSelezionate) {
        this.activity = activity;
        this.elencoCategorie = elencoCategorie;
        this.categorieSelezionate = categorieSelezionate;
        if(this.categorieSelezionate.size() == 0)
            setHashCollection();

    }

    protected void setHashCollection(){
        categorieSelezionate = new HashMap<>();
        for(int i = 0; i < elencoCategorie.size(); i++){
            categorieSelezionate.put(elencoCategorie.get(i).getId(), true);
        }
    }

    public void setElencoCategorie(List<Categoria> elencoCategorie){
        this.elencoCategorie = elencoCategorie;
        if(categorieSelezionate.size() == 0){
            setHashCollection();
        }
        notifyDataSetChanged();
    }

    public HashMap<Integer, Boolean> getCategorieSelezionate(){
        return categorieSelezionate;
    }


    @Override
    public AdapterCategoriaSelezionabili.CategoriaVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categoria_selezionabile, parent, false);
        return new AdapterCategoriaSelezionabili.CategoriaVH(v);
    }

    @Override
    public void onBindViewHolder(AdapterCategoriaSelezionabili.CategoriaVH holder, int position) {
        holder.doLayout(elencoCategorie.get(position));
    }

    @Override
    public int getItemCount() {
        return elencoCategorie.size();
    }

    class CategoriaVH extends RecyclerView.ViewHolder {

        private ImageView iconaCategoria;
        private TextView nomeCategoria;
        private CheckBox selezionataCategoria;
        private View container;

        public CategoriaVH(View view) {
            super(view);
            this.container = view;
            iconaCategoria = (ImageView) view.findViewById(R.id.item_categoria_selezionabile_icona);
            nomeCategoria = (TextView) view.findViewById(R.id.item_categoria_selezionabile_nome);
            selezionataCategoria = (CheckBox) view.findViewById(R.id.item_categoria_selezionabile_checkbox);
        }

        public void doLayout(final Categoria categoria) {
            Picasso.with(activity).load(activity.getString(R.string.radiceImmagini) + categoria.getIcona())
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.no_image)
                    .into(iconaCategoria);
            nomeCategoria.setText(categoria.getNome());
            selezionataCategoria.setChecked(categorieSelezionate.get(categoria.getId()));
            selezionataCategoria.setOnCheckedChangeListener((compoundButton, b) -> {
                categorieSelezionate.put(categoria.getId(), b);

            });

        }


    }


}



