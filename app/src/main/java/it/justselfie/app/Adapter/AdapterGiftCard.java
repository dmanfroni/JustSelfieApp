package it.justselfie.app.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.squareup.picasso.Picasso;

import java.util.List;

import it.justselfie.app.Activity.ConvalidaActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Model.ConvalidaGiftCard;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 20/11/2016.
 */

public class AdapterGiftCard  extends RecyclerView.Adapter<AdapterGiftCard.GiftCardVH>{


    public AppCompatActivity activity;
    private int selfieCoin;

    public AdapterGiftCard(AppCompatActivity activity, int selfieCoin){
        this.activity = activity;
        this.selfieCoin = selfieCoin;
    }


    @Override
    public GiftCardVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gift_card, parent, false);
        return new GiftCardVH(v);
    }

    @Override
    public void onBindViewHolder(GiftCardVH holder, int position) {
        holder.doLayout(position);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class GiftCardVH extends RecyclerView.ViewHolder{


        private final RoundCornerProgressBar roundCornerProgressBar;
        private ImageView iconaGiftCard;
        private TextView tipoGiftCard;
        private Button riscuoti;
        private TextView quantitaSelfieCoin;

        public GiftCardVH(View view) {
            super(view);
            roundCornerProgressBar = (RoundCornerProgressBar) view.findViewById(R.id.item_gift_card_progressbar);
            iconaGiftCard = (ImageView) view.findViewById(R.id.item_gift_card_icona);
            tipoGiftCard = (TextView) view.findViewById(R.id.item_gift_card_tipobonus_text);
            quantitaSelfieCoin = (TextView) view.findViewById(R.id.item_gift_card_selfiecoin_text);
            riscuoti = (Button) view.findViewById(R.id.item_gift_card_riscuoti);
        }

        public void doLayout(int position){
            int valoreEuro = 0;
            int valoreSelfieCoin = 0;
            switch (position){
                case 0:
                    valoreEuro = 5;
                    valoreSelfieCoin = 500;

                    iconaGiftCard.setImageResource(R.mipmap.giftcard_5);
                    roundCornerProgressBar.setProgressColor(Color.parseColor("#F39200"));

                    break;
                case 1:
                    valoreEuro = 10;
                    valoreSelfieCoin = 1000;
                    iconaGiftCard.setImageResource(R.mipmap.giftcard_10);
                    roundCornerProgressBar.setProgressColor(Color.parseColor("#5BC2DB"));

                    break;
                case 2:
                    valoreEuro = 20;
                    valoreSelfieCoin = 2000;
                    iconaGiftCard.setImageResource(R.mipmap.giftcard_20);
                    roundCornerProgressBar.setProgressColor(Color.parseColor("#2D9E39"));

                    break;
            }

            tipoGiftCard.setText("Bonus di " + valoreEuro + "€");
            quantitaSelfieCoin.setText(selfieCoin + "/" + valoreSelfieCoin);
            roundCornerProgressBar.setMax(valoreSelfieCoin);
            if(selfieCoin >= valoreSelfieCoin)
                roundCornerProgressBar.setProgress(valoreSelfieCoin);
            else
                roundCornerProgressBar.setProgress(selfieCoin);


            int finalValoreSelfieCoin = valoreSelfieCoin;
            int finalValoreEuro = valoreEuro;
            riscuoti.setOnClickListener(view -> {
                if(selfieCoin < finalValoreSelfieCoin){
                    Toast.makeText(activity, "Non hai abbastanza selfie coin :( " , Toast.LENGTH_LONG).show();
                }else{
                    ConvalidaGiftCard convalidaGiftCard = new ConvalidaGiftCard();
                    convalidaGiftCard.setValoreEuro(finalValoreEuro);
                    convalidaGiftCard.setValoreSelfieCoin(finalValoreSelfieCoin);
                    Intent intent = new Intent(activity, ConvalidaActivity.class);
                    intent.putExtra("convalidaGiftCard", convalidaGiftCard);
                    activity.startActivity(intent);
                }
            });
        }
    }



}



