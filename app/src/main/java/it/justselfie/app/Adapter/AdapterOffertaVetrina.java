package it.justselfie.app.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Model.Offerta;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.SortFilter;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 27/12/2016.
 */

public class AdapterOffertaVetrina extends RecyclerView.Adapter<AdapterOffertaVetrina.OffertaVH> implements Filterable {

    public List<Offerta> elencoOfferte;
    public List<Offerta> elencoOfferteCopia;
    public AppCompatActivity activity;
    private double userLat, userLon;
    private int[] screenSize;
    private double width, height;

    public AdapterOffertaVetrina(AppCompatActivity activity, List<Offerta> elencoOfferte, double userLat, double userLon) {
        this.activity = activity;
        this.elencoOfferte = elencoOfferte;
        this.elencoOfferteCopia = elencoOfferte;
        this.userLat = userLat;
        this.userLon = userLon;
        screenSize = Tools.getScreenSizePx(activity);
        width = (screenSize[0] - Tools.dpToPx(activity, 8));
        height = (width / 16) * 9;
    }



    public void setElencoOfferte(List<Offerta> elencoOfferte, SortFilter sortFilter) {
        if(elencoOfferte != null) {
            this.elencoOfferte = elencoOfferte;
            this.elencoOfferteCopia = elencoOfferte;
            notifyDataSetChanged();
            getFilter().filter(new Gson().toJson(sortFilter));
        }
    }

    public void setPosizioneUtente(double userLat, double userLon) {
        this.userLat = userLat;
        this.userLon = userLon;
        notifyDataSetChanged();
    }

    @Override
    public AdapterOffertaVetrina.OffertaVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offerta_vetrina, parent, false);
        return new AdapterOffertaVetrina.OffertaVH(v);
    }

    @Override
    public void onBindViewHolder(AdapterOffertaVetrina.OffertaVH holder, int position) {
        holder.doLayout(elencoOfferteCopia.get(position));
    }

    @Override
    public int getItemCount() {
        if (elencoOfferteCopia == null)
            return 0;
        return elencoOfferteCopia.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                elencoOfferteCopia = (List<Offerta>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Offerta> risultatiFiltrati;
                if (constraint.length() == 0) {
                    risultatiFiltrati = elencoOfferte;
                } else {

                    risultatiFiltrati = getFilteredResults(new Gson().fromJson(constraint.toString(), SortFilter.class));
                }

                FilterResults results = new FilterResults();
                results.values = risultatiFiltrati;

                return results;
            }
        };
    }

    protected List<Offerta> getFilteredResults(SortFilter sortFilter) {
        List<Offerta> risultati = new ArrayList<>();

        for (Offerta offerta : elencoOfferte) {
            if(!sortFilter.getTesto().isEmpty()){
                if(!offerta.getNome().toLowerCase().contains(sortFilter.getTesto().toLowerCase()))
                    continue;
            }
            if(sortFilter.isFiltroPrezzoMassimo() && offerta.getPrezzoScontato() > sortFilter.getPrezzoMassimo()){
                continue;
            }
            if(sortFilter.isFiltroPrezzoMinimo() && offerta.getPrezzoScontato() < sortFilter.getPrezzoMinimo()){
                continue;
            }
            if(sortFilter.getCategorieSelezionate().containsKey(offerta.getEsercente().getCategoria().getId())){
                if(!sortFilter.getCategorieSelezionate().get(offerta.getEsercente().getCategoria().getId()))
                    continue;
            }
            risultati.add(offerta);
        }
        return risultati;
    }


    class OffertaVH extends RecyclerView.ViewHolder {


        private TextView nome;
        private TextView sconto;
        private RelativeLayout scontoLayout;
        private TextView prezzoPieno;
        private TextView prezzoScontato;
        private TextView esercente;
        private TextView distanza;
        private ImageView categoria;
        private ImageView immagine;
        private View view;

        public OffertaVH(View itemView) {
            super(itemView);
            view = itemView;
            nome = (TextView) itemView.findViewById(R.id.item_offerta_nome);
            sconto = (TextView) itemView.findViewById(R.id.item_offerta_sconto);
            scontoLayout = (RelativeLayout) itemView.findViewById(R.id.item_offerta_sconto_sfondo);
            prezzoPieno = (TextView) itemView.findViewById(R.id.item_offerta_prezzo_pieno);
            prezzoScontato = (TextView) itemView.findViewById(R.id.item_offerta_prezzo_scontato);
            esercente = (TextView) itemView.findViewById(R.id.item_offerta_esercente);
            distanza = (TextView) itemView.findViewById(R.id.item_offerta_distanza);
            categoria = (ImageView) itemView.findViewById(R.id.item_offerta_categoria);
            immagine = (ImageView) itemView.findViewById(R.id.item_offerta_immagine);
        }

        public void doLayout(final Offerta offerta) {
            DecimalFormat f = new DecimalFormat("0.00");
            SpannableString prezzoPienoSpannable = new SpannableString(String.valueOf(f.format(offerta.getPrezzoPieno()) + "€"));
            prezzoPienoSpannable.setSpan(new StrikethroughSpan(), 0, prezzoPienoSpannable.length(), 0);
            SpannableString scontoSpannable = new SpannableString("-" + String.valueOf(((int) offerta.getPercentualeSconto()) + "%"));
            scontoSpannable.setSpan(new RelativeSizeSpan(2f), 0, scontoSpannable.length(), 0);


            nome.setText(StringEscapeUtils.unescapeHtml4(offerta.getNome()));
            prezzoScontato.setText(f.format(offerta.getPrezzoScontato()) + "€");
            prezzoPieno.setText(prezzoPienoSpannable);
            esercente.setText(offerta.getEsercente().getNome());
            Picasso.with(activity).load(activity.getString(R.string.radiceImmagini) + offerta.getEsercente().getCategoria().getIcona())
                    .placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(categoria);

            immagine.setLayoutParams(new RelativeLayout.LayoutParams((int)width, (int)height));

            Picasso.with(activity).load(activity.getString(R.string.radiceImmagini) + offerta.getImmagine())
                    .centerCrop().fit().placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagine);

            sconto.setText(scontoSpannable);
            sconto.append("\nALLA CASSA");

            view.setOnClickListener(v1 -> {
                if (activity instanceof JustSelfieActivity) {
                    Ricerca ricerca = new Ricerca();
                    ricerca.setIdEsercente(offerta.getEsercente().getId());
                    Intent intent = new Intent(activity, EsercenteActivity.class);
                    intent.putExtra("ricerca", ricerca);
                    activity.startActivity(intent);
                }
            });

            if (userLat != 0.0 && userLon != 0.0) {
                double dist = Double.parseDouble(Tools.distance(userLat, userLon, offerta.getEsercente().getLatitudine(), offerta.getEsercente().getLongitudine()));
                distanza.setText( dist + " Km");
            } else {
                distanza.setVisibility(View.GONE);
            }

            GradientDrawable bgShape = (GradientDrawable)scontoLayout.getBackground();
            bgShape.setColor(Color.parseColor(offerta.getEsercente().getCategoria().getColore()));



        }

    }


}

