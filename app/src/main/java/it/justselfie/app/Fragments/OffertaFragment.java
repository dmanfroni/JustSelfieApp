package it.justselfie.app.Fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.DecimalFormat;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Activity.CarrelloActivity;
import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.Offerta;
import it.justselfie.app.R;

/**
 * Created by Davide on 09/11/2016.
 */

public class OffertaFragment extends DialogFragment {

    @InjectView(R.id.fragment_offerta_nome)
    TextView nomeOfferta;

    @InjectView(R.id.fragment_offerta_prezzo_pieno)
    TextView prezzoPieno;

    @InjectView(R.id.fragment_offerta_prezzo_scontato)
    TextView prezzoScontato;

    @InjectView(R.id.fragment_offerta_descrizione)
    WebView descrizioneOfferta;

    @InjectView(R.id.fragment_offerta_immagine)
    ImageView immagineOfferta;


    private int quantita;

    private Offerta offerta;
    private AppCompatActivity appCompatActivity;


    public static OffertaFragment newInstance(Offerta offerta, int quantity) {
        OffertaFragment offertaFragment = new OffertaFragment();
        Bundle b = new Bundle();
        b.putSerializable("offerta", offerta);
        b.putSerializable("quantita", quantity);
        offertaFragment.setArguments(b);
        return offertaFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_offerta, container, false);
        ButterKnife.inject(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        quantita = getArguments().containsKey("quantita") ? getArguments().getInt("quantita") : 1;
        offerta = (Offerta) getArguments().getSerializable("offerta");
        appCompatActivity = (AppCompatActivity) getActivity();
        if(offerta.getImmagine() != null && !offerta.getImmagine().isEmpty()){
            Picasso.with(appCompatActivity)
                    .load(appCompatActivity.getString(R.string.radiceImmagini) + offerta.getImmagine())
                    .centerCrop()
                    .fit()
                    .placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagineOfferta);
        }
        nomeOfferta.setText(offerta.getNome());
        descrizioneOfferta.getSettings().setAllowFileAccess(true);
        descrizioneOfferta.getSettings().setPluginState(WebSettings.PluginState.ON);
        descrizioneOfferta.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        descrizioneOfferta.getSettings().setJavaScriptEnabled(true);
        descrizioneOfferta.getSettings().setLoadWithOverviewMode(true);
        descrizioneOfferta.getSettings().setUseWideViewPort(true);
        descrizioneOfferta.getSettings().setDefaultFontSize(25);
        String descrizione =  StringEscapeUtils.unescapeHtml4(offerta.getDescrizione());
        if(descrizione != null) {
            String content =
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                            "<html><head>" +
                            "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />" +
                            "<meta name=\"viewport\" content=\"target-densityDpi=device-dpi, width=device-width\"/>" +
                            "<style>@font-face {font-family: \'NeutraTextLight\';src: url(\'file:///android_asset/fonts/NeutraTextLight.otf\');}"+
                            "html, body {font-size: 20px; font-family: \'NeutraTextLight\';color: #3D3D3B;width:100%;height: auto;margin: 0px;padding: 4px;}</style></head><body style=\"margin: 0; padding: 0\">";

            // String content = "<html><body>";
            content += descrizione + "</body></html>";
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP){
                descrizioneOfferta.loadData(content, "text/html; charset=utf-8", "UTF-8");
            }else{
                descrizioneOfferta.loadDataWithBaseURL("file:///android_asset/", content, "text/html; charset=utf-8", "UTF-8", null);
            }


        }else
            descrizioneOfferta.setVisibility(View.GONE);
        //descrizioneOfferta.setText(offerta.getDescrizione());
        /*menoButton.setOnClickListener((v) ->{
            if(quantita == 1)
                return;
            quantita --;
            updateQuantity();
        });
        piuButton.setOnClickListener((v) ->{
            quantita ++;
            updateQuantity();
        });
        annulla.setOnClickListener((v) ->  dismiss() );
        conferma.setOnClickListener((v) -> {
            AcquistoItem acquistoItem = new AcquistoItem();
            acquistoItem.setQuantita(quantita);
            acquistoItem.setOfferta(offerta);
            if(appCompatActivity instanceof EsercenteActivity){
                ((EsercenteActivity) appCompatActivity).aggiornaAcquisto(acquistoItem);
            }
            if(appCompatActivity instanceof CarrelloActivity){
                ((CarrelloActivity) appCompatActivity).aggiornaAcquisto(acquistoItem);
            }

            dismiss();
        });*/
        updateQuantity();

    }

    private void updateQuantity(){
       // quantitaScelta.setText(quantita + "");
        DecimalFormat f = new DecimalFormat("0.00");

        double prPieno = offerta.getPrezzoPieno() * quantita;
        double prScontato = offerta.getPrezzoScontato() * quantita;

        SpannableString text = new SpannableString(String.valueOf(f.format(prPieno) + "€"));
        text.setSpan(new StrikethroughSpan(), 0, text.length(), 0);
        prezzoPieno.setText(text, TextView.BufferType.SPANNABLE);
        prezzoScontato.setText(String.valueOf(f.format(prScontato) + "€"));
    }


    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        super.onResume();
    }

}
