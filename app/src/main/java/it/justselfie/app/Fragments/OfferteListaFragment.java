package it.justselfie.app.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Activity.AnalyticsActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Adapter.AdapterEsercente;
import it.justselfie.app.Adapter.AdapterOffertaVetrina;
import it.justselfie.app.Interface.SortFilterable;
import it.justselfie.app.Model.Offerta;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.SortFilter;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.OnGeoCodingTaskCompleted;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 27/12/2016.
 */

public class OfferteListaFragment extends Fragment implements SortFilterable, OnGeoCodingTaskCompleted {

    @InjectView(R.id.fragment_child_recyclerview)
    RecyclerView recyclerView;
    @InjectView(R.id.fragment_child_swiperefresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.fragment_child_empty_container)
    View emptyContainer;
    @InjectView(R.id.fragment_child_empty_message)
    TextView emptyMessage;

    private SortFilter sortFilter;
    private List<Offerta> elencoOfferte;
    private JustSelfieActivity justSelfieActivity;
    private double userLat, userLon;

    public static OfferteListaFragment newInstance(SortFilter sortFilter) {
        OfferteListaFragment offerteListaFragment = new OfferteListaFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("filtri", sortFilter);
        offerteListaFragment.setArguments(bundle);
        return offerteListaFragment;
    }

    public static OfferteListaFragment newInstance() {
        return new OfferteListaFragment();
    }

    public OfferteListaFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
        super.onCreateView(inflater, root, bundle);
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_child_elenco_base, null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        ((AnalyticsActivity) getActivity()).getTracker().setScreenName("Vetrina");
        ((AnalyticsActivity) getActivity()).getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        justSelfieActivity = (JustSelfieActivity) getActivity();
        userLat = PreferenceManager.getLatitudine(justSelfieActivity);
        userLon = PreferenceManager.getLongitudine(justSelfieActivity);
        sortFilter = (getArguments() != null && getArguments().containsKey("filtri"))
                ? (SortFilter) getArguments().getSerializable("filtri")
                : new SortFilter();
        emptyContainer.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new AdapterOffertaVetrina(justSelfieActivity, new ArrayList<>(), userLat, userLon));
        swipeRefreshLayout.setRefreshing(true);
        new OfferteListaFragment.ElencoOfferteDownloader().execute();
    }

    @Override
    public void applySortFilter(SortFilter sortFilter) {
        this.sortFilter = sortFilter;
        ordina();
        ((AdapterOffertaVetrina) recyclerView.getAdapter()).setElencoOfferte(elencoOfferte, sortFilter);
    }

    @Override
    public SortFilter getSortFilter() {
        return sortFilter;
    }

    @Override
    public void onGeoCodingCompleted(String via, String civ) {

    }

    @Override
    public void onLocationListener(double lat, double lon) {
        this.userLat = lat;
        this.userLon = lon;
        PreferenceManager.setLatitudine(lat, justSelfieActivity);
        PreferenceManager.setLongitudine(lon, justSelfieActivity);
        sortFilter.setOrdinamento(SortFilter.ORDINAMENTO_DISTANZA);
        ordina();
        ((AdapterOffertaVetrina) recyclerView.getAdapter()).setPosizioneUtente(lat, lon);
        ((AdapterOffertaVetrina) recyclerView.getAdapter()).setElencoOfferte(elencoOfferte, sortFilter);
    }

    private void ordina() {
        if(elencoOfferte != null) {
            if (sortFilter.getOrdinamento() == SortFilter.ORDINAMENTO_DEFAULT) {
                if(elencoOfferte != null) Collections.shuffle(elencoOfferte);
            } else {
                Collections.sort(elencoOfferte, merchantComparator);
            }
        }
    }

    private class ElencoOfferteDownloader extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            try {
                Ricerca ricerca = new Ricerca();
                ricerca.setIdUtente(PreferenceManager.getUtente(justSelfieActivity).getId());
                String s = new Gson().toJson(ricerca);
                return new JSONObject(NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_offerta_lista, s));
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            try {
                if (object.getBoolean("success")) {
                    showView(object.getJSONObject("response"));
                } else {
                    showError(object.getString("response"));
                }
            } catch (Exception ex) {
                showError(ex.getMessage());
            }

        }
    }

    void showError(String message) {
        Toast.makeText(justSelfieActivity, message, Toast.LENGTH_LONG).show();
    }

    void showView(JSONObject object) {
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);
        try {
            JSONArray array = object.getJSONArray("elencoOfferte");
            elencoOfferte = new Gson().fromJson(array.toString(), new TypeToken<List<Offerta>>() {
            }.getType());
            if (sortFilter.getOrdinamento() == SortFilter.ORDINAMENTO_DEFAULT) {
                Collections.shuffle(elencoOfferte);
            } else {
                Collections.sort(elencoOfferte, merchantComparator);
            }
            ((AdapterOffertaVetrina) recyclerView.getAdapter()).setElencoOfferte(elencoOfferte, sortFilter);
            emptyContainer.setVisibility(elencoOfferte.size() > 0 ? View.GONE : View.VISIBLE);
        } catch (Exception ex) {
            emptyContainer.setVisibility(View.VISIBLE);
            showError(ex.getMessage());
        }

    }


    public Comparator<Offerta> merchantComparator = new Comparator<Offerta>() {

        public int compare(Offerta offertaA, Offerta offertaB) {
            switch (sortFilter.getOrdinamento()) {
                case SortFilter.ORDINAMENTO_DEFAULT:
                    int random = (int) (Math.random() * 10);
                    return random;
                case SortFilter.ORDINAMENTO_DISTANZA:
                    /*double distanceUserToA = Math.sqrt(
                            ((offertaA.getEsercente().getLatitudine() - userLat) * (offertaA.getEsercente().getLatitudine() - userLat)) +
                            ((offertaA.getEsercente().getLongitudine() - userLon) * (offertaA.getEsercente().getLongitudine() - userLon)));
                    double distanceUserToB = Math.sqrt(
                            ((offertaB.getEsercente().getLatitudine() - userLat) * (offertaB.getEsercente().getLatitudine() - userLat)) +
                            ((offertaB.getEsercente().getLongitudine() - userLon) * (offertaB.getEsercente().getLongitudine() - userLon)));
                    int value = (int) ((distanceUserToA - distanceUserToB) * 100000);
                    return value;*/
                    double distanceUserToA = Tools.distanceDouble(offertaA.getEsercente().getLatitudine(), offertaA.getEsercente().getLongitudine(), userLat, userLon);
                    double distanceUserToB = Tools.distanceDouble(offertaB.getEsercente().getLatitudine(), offertaB.getEsercente().getLongitudine(), userLat, userLon);
                    int value = (int) ((distanceUserToA * 10000) - (distanceUserToB * 10000));
                    return value;
                case SortFilter.ORDINAMENTO_PREZZO:
                    return (int) (offertaA.getPrezzoScontato() - offertaB.getPrezzoScontato());
            }

           /* */
            return 1;

        }

    };
}


