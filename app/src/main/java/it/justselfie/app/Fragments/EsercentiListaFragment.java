package it.justselfie.app.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Activity.AnalyticsActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Adapter.AdapterEsercente;
import it.justselfie.app.Adapter.AdapterOffertaVetrina;
import it.justselfie.app.Interface.SortFilterable;
import it.justselfie.app.Model.Esercente;
import it.justselfie.app.Model.Offerta;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.SortFilter;
import it.justselfie.app.R;
import it.justselfie.app.Tools.GpsTracker;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.OnGeoCodingTaskCompleted;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 30/10/2016.
 */

public class EsercentiListaFragment extends Fragment implements SortFilterable, OnGeoCodingTaskCompleted {


    private Ricerca ricerca;
    private double userLat, userLon;

    private GpsTracker gpsTracker;


    @InjectView(R.id.fragment_child_recyclerview)
    RecyclerView recyclerView;
    @InjectView(R.id.fragment_child_swiperefresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.fragment_child_empty_container)
    View emptyContainer;
    @InjectView(R.id.fragment_child_empty_message)
    TextView emptyMessage;


    private SortFilter sortFilter;
    private List<Esercente> elencoEsercenti;
    private JustSelfieActivity justSelfieActivity;


    public static EsercentiListaFragment newInstance(SortFilter sortFilter) {
        EsercentiListaFragment esercentiListaFragment = new EsercentiListaFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("filtri", sortFilter);
        esercentiListaFragment.setArguments(bundle);
        return esercentiListaFragment;
    }

    public static EsercentiListaFragment newInstance(){
        return new EsercentiListaFragment();
    }

    public EsercentiListaFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
        super.onCreateView(inflater, root, bundle);
        View view = inflater.inflate(R.layout.fragment_child_elenco_base, null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        justSelfieActivity = (JustSelfieActivity) getActivity();
        userLat = PreferenceManager.getLatitudine(justSelfieActivity);
        userLon = PreferenceManager.getLongitudine(justSelfieActivity);
        sortFilter = (getArguments() != null && getArguments().containsKey("filtri"))
                ? (SortFilter) getArguments().getSerializable("filtri")
                : new SortFilter();
        emptyContainer.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new AdapterEsercente(justSelfieActivity, new ArrayList<>(), userLat, userLon));
        swipeRefreshLayout.setRefreshing(true);
        new ElencoEsercenteDownloader().execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        ((AnalyticsActivity) getActivity()).getTracker().setScreenName("Js Point");
        ((AnalyticsActivity) getActivity()).getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }



    @Override
    public void applySortFilter(SortFilter sortFilter) {
        this.sortFilter = sortFilter;
        ordina();
        ((AdapterEsercente)recyclerView.getAdapter()).setElencoEsercenti(elencoEsercenti);
    }

    @Override
    public SortFilter getSortFilter() {
        return sortFilter;
    }

    @Override
    public void onGeoCodingCompleted(String via, String civ) {

    }

    @Override
    public void onLocationListener(double lat, double lon) {
        if(justSelfieActivity != null) {
            this.userLat = lat;
            this.userLon = lon;
            PreferenceManager.setLatitudine(lat, justSelfieActivity);
            PreferenceManager.setLongitudine(lon, justSelfieActivity);
            sortFilter.setOrdinamento(SortFilter.ORDINAMENTO_DISTANZA);
            ((AdapterEsercente) recyclerView.getAdapter()).setPosizioneUtente(lat, lon);
            if(elencoEsercenti != null) {
                ordina();
                ((AdapterEsercente) recyclerView.getAdapter()).setElencoEsercenti(elencoEsercenti);
            }
        }
    }

    private void ordina(){
        if(sortFilter.getOrdinamento() == SortFilter.ORDINAMENTO_DEFAULT){
            if(elencoEsercenti != null) Collections.shuffle(elencoEsercenti);
        }else {
            Collections.sort(elencoEsercenti, merchantComparator);
        }
    }

    private class ElencoEsercenteDownloader extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            Ricerca ricerca = (getArguments() != null &&  getArguments().containsKey("ricerca")) ? (Ricerca) getArguments().getSerializable("ricerca") : new Ricerca();
            try {
                String s = new Gson().toJson(ricerca);
                return new JSONObject(NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_esercente_lista, s));
            } catch (Exception ex) {}
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            try {
                if (object.getBoolean("success")) {
                    showView(object.getJSONObject("response"));
                } else {
                    showError(object.getString("response"));
                }
            } catch (Exception ex) {
                showError(ex.getMessage());
            }

        }
    }

    void showError(String message) {
        Toast.makeText(justSelfieActivity, message, Toast.LENGTH_LONG).show();
    }

    void showView(JSONObject object) {
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);
        try {
            JSONArray array = object.getJSONArray("elencoEsercenti");
            elencoEsercenti = new Gson().fromJson(array.toString(), new TypeToken<List<Esercente>>() {}.getType());
            ordina();
            ((AdapterEsercente) recyclerView.getAdapter()).setElencoEsercenti(elencoEsercenti);
            emptyContainer.setVisibility(elencoEsercenti.size() > 0 ? View.GONE : View.VISIBLE);
        } catch (Exception ex) {
            emptyContainer.setVisibility(View.VISIBLE);
            showError(ex.getMessage());
        }

    }

    public void performSearch(String ricerca) {
        ((AdapterEsercente) recyclerView.getAdapter()).getFilter().filter(ricerca);
    }

    public Comparator<Esercente> merchantComparator = new Comparator<Esercente>() {

        public int compare(Esercente esercenteA, Esercente esercenteB) {
            switch (sortFilter.getOrdinamento()) {
                case SortFilter.ORDINAMENTO_DEFAULT:
                    int random = (int) (Math.random() * 10);
                    return random;
                case SortFilter.ORDINAMENTO_DISTANZA:
                    double distanceUserToA = Tools.distanceDouble(esercenteA.getLatitudine(), esercenteA.getLongitudine(), userLat, userLon);
                    double distanceUserToB = Tools.distanceDouble(esercenteB.getLatitudine(), esercenteB.getLongitudine(), userLat, userLon);
                    int value = (int) ((distanceUserToA * 10000) - (distanceUserToB * 10000));
                    return value;
                case SortFilter.ORDINAMENTO_PREZZO:
                    double distanceUserToAA = Tools.distanceDouble(esercenteA.getLatitudine(), esercenteA.getLongitudine(), userLat, userLon);
                    double distanceUserToBB = Tools.distanceDouble(esercenteB.getLatitudine(), esercenteB.getLongitudine(), userLat, userLon);
                    int valueA = (int) ((distanceUserToAA * 10000) - (distanceUserToBB * 10000));
                    return valueA;

            }

            return 1;

        }

    };

}


