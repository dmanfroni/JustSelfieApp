package it.justselfie.app.Fragments;


import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Activity.AnalyticsActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Adapter.AdapterConvalidaOfferta;
import it.justselfie.app.Adapter.AdapterGiftCard;
import it.justselfie.app.Adapter.AdapterSconto;
import it.justselfie.app.Adapter.AdapterSelfie;
import it.justselfie.app.CustomViews.SimpleDividerItemDecoration;
import it.justselfie.app.Database.DatabaseHelper;
import it.justselfie.app.Database.QueryBuilder;
import it.justselfie.app.Model.ConvalidaOfferta;
import it.justselfie.app.Model.ConvalidaSconto;
import it.justselfie.app.Model.ConvalidaSelfie;
import it.justselfie.app.Model.Esercente;
import it.justselfie.app.Model.MessaggioUtente;
import it.justselfie.app.Model.Sconto;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.Tools.Tools;


/**
 * Created by Davide on 21/03/2016.
 */
public class ScoreFragment extends Fragment {

    @InjectView(R.id.fragment_score_tablayout)
    TabLayout tabLayout;

    @InjectView(R.id.fragment_score_viewpager)
    ViewPager viewPager;

    @InjectView(R.id.fragment_score_progressbar)
    ProgressBar progressBar;

    private JustSelfieActivity justSelfieActivity;

    private Utente utente;
    private ArrayList<Sconto> elencoSconti;
    private ArrayList<Selfie> elencoSelfie;

    public static ScoreFragment newInstance(){
        return new ScoreFragment();
    }

    public static ScoreFragment newInstance(int viewDaAprire){
        // 1 offerte, 2 sconti, 3 selfie, 4 giftcard
        ScoreFragment scoreFragment = new ScoreFragment();
        Bundle b = new Bundle();
        b.putInt("view", viewDaAprire);
        scoreFragment.setArguments(b);
        return scoreFragment;
    }

    public ScoreFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup root, Bundle savedInstance){
        super.onCreateView(layoutInflater, root, savedInstance);
        View view = layoutInflater.inflate(R.layout.fragment_score, null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        justSelfieActivity = (JustSelfieActivity) getActivity();
        viewPager.setVisibility(View.GONE);
        utente = PreferenceManager.getUtente(justSelfieActivity);
        new ScoreDownloader().execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        ((AnalyticsActivity) getActivity()).getTracker().setScreenName("Premi");
        ((AnalyticsActivity) getActivity()).getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }


    private class ScoreDownloader extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            return NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_utente_score, new Gson().toJson(utente));
        }

        @Override
        protected void onPostExecute(String response){
            try{
                progressBar.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject(response);
                if(jsonObject.getBoolean("success")){
                    elencoSelfie = new Gson().fromJson(jsonObject.getJSONObject("response").getJSONArray("selfie").toString(), new TypeToken<List<Selfie>>(){}.getType());
                    elencoSconti = new Gson().fromJson(jsonObject.getJSONObject("response").getJSONArray("sconti").toString(), new TypeToken<List<Sconto>>(){}.getType());
                    int selfieCoin = jsonObject.getJSONObject("response").getInt("selfieCoin");
                    utente.setSelfieCoin(selfieCoin);
                    PreferenceManager.setUtente(utente, justSelfieActivity);

                }
            }catch (Exception ex){

            }

            tabLayout.addTab(tabLayout.newTab());
            tabLayout.addTab(tabLayout.newTab());
            tabLayout.addTab(tabLayout.newTab());
            tabLayout.addTab(tabLayout.newTab());
            viewPager.setAdapter(new PageAdapter(getChildFragmentManager()));
            viewPager.setOffscreenPageLimit(3);
            tabLayout.setupWithViewPager(viewPager);
            Tools.changeFontInViewGroup(tabLayout, "fonts/NeutraTextDemi.otf");
            if(getArguments() != null && getArguments().containsKey("view")){
                int viewDaAprire = getArguments().getInt("view");
                viewPager.setCurrentItem(viewDaAprire, false);
            }

        }
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {

            super(fm);
        }

        @Override
        public String getPageTitle(int position){
            switch (position) {

                case 1:
                    return "OFFERTE";
                case 2:
                    return "SCONTI";
                case 3:
                    return "SELFIE COIN";
                case 0:
                    return "JS CARD";
                default:
                    return "";
            }

        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case 1:
                    return OfferteFragment.newInstance();
                case 2:
                    return ScontiFragment.newInstance(elencoSconti);
                case 3:
                    return SelfieFragment.newInstance(elencoSelfie);
                case 0:
                    return GiftCardFragment.newInstance();
                default:
                    return GiftCardFragment.newInstance();
            }


        }
    }


    public static class OfferteFragment extends Fragment {

        @InjectView(R.id.fragment_child_recyclerview)
        RecyclerView recyclerView;

        @InjectView(R.id.fragment_child_swiperefresh_layout)
        SwipeRefreshLayout swipeRefresh;

        @InjectView(R.id.fragment_child_empty_container)
        View emptyContainer;
        @InjectView(R.id.fragment_child_empty_message)
        TextView emptyMessage;

        private JustSelfieActivity justSelfieActivity;

        public static OfferteFragment newInstance() {
            OfferteFragment offerteFragment = new OfferteFragment();
            return offerteFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            View v = inflater.inflate(R.layout.fragment_child_elenco_base, null);
            ButterKnife.inject(this, v);
            justSelfieActivity = (JustSelfieActivity) getActivity();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            List<ConvalidaOfferta> offerteDaConvalidare = new ArrayList<>();
            String query = "SELECT * FROM " + QueryBuilder.tabellaAcquisti + " WHERE 1=1";
            DatabaseHelper db = new DatabaseHelper(justSelfieActivity);
            Cursor c = db.rawQuery(query);
            while(c.moveToNext()){
                int id  = c.getInt(c.getColumnIndex("_id"));
                String json = c.getString(c.getColumnIndex("json"));
                ConvalidaOfferta convalidaOfferta = new Gson().fromJson(json, ConvalidaOfferta.class);
                convalidaOfferta.setId(id);
                offerteDaConvalidare.add(convalidaOfferta);
            }
            swipeRefresh.setEnabled(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(justSelfieActivity, LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(justSelfieActivity));
            recyclerView.setAdapter(new AdapterConvalidaOfferta(justSelfieActivity, offerteDaConvalidare));
            emptyContainer.setVisibility(offerteDaConvalidare.size() > 0 ? View.GONE : View.VISIBLE);
        }

    }

    public static class ScontiFragment extends Fragment {

        private JustSelfieActivity justSelfieActivity;

        @InjectView(R.id.fragment_child_recyclerview)
        RecyclerView recyclerView;

        @InjectView(R.id.fragment_child_swiperefresh_layout)
        SwipeRefreshLayout swipeRefreshLayout;

        @InjectView(R.id.fragment_child_empty_container)
        View emptyContainer;
        @InjectView(R.id.fragment_child_empty_message)
        TextView emptyMessage;

        public static ScontiFragment newInstance(ArrayList<Sconto> sconti) {
            ScontiFragment scontiFragment = new ScontiFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("sconti", sconti);
            scontiFragment.setArguments(bundle);
            return scontiFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            View v = inflater.inflate(R.layout.fragment_child_elenco_base, null);
            ButterKnife.inject(this, v);
            justSelfieActivity = (JustSelfieActivity) getActivity();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            List<Sconto> elencoScontiServer = new ArrayList<>();
            List<ConvalidaSconto> elencoConvalidaSconti = new ArrayList<>();
            List<Object> elencoOggetti = new ArrayList<>();
            if(getArguments() != null && getArguments().containsKey("sconti")) {
                ArrayList<Sconto> sconti = (ArrayList<Sconto>) getArguments().getSerializable("sconti");
                if(sconti != null && sconti.size() > 0) {
                    elencoScontiServer.addAll(sconti);
                }
            }

            String query = "SELECT * FROM " + QueryBuilder.tabellaSconti + " WHERE 1=1";
            DatabaseHelper db = new DatabaseHelper(justSelfieActivity);
            Cursor c = db.rawQuery(query);
            while(c.moveToNext()){
                int id  = c.getInt(c.getColumnIndex("_id"));
                String json = c.getString(c.getColumnIndex("json"));
                ConvalidaSconto convalidaSconto = new Gson().fromJson(json, ConvalidaSconto.class);
                convalidaSconto.setId(id);
                elencoConvalidaSconti.add(convalidaSconto);
            }

            for(ConvalidaSconto convalidaSconto : elencoConvalidaSconti){
                for(int i = 0; i < elencoScontiServer.size(); i++){
                    if(convalidaSconto.getSconto().getEsercente().getId() == elencoScontiServer.get(i).getEsercente().getId()){
                        elencoScontiServer.remove(i);
                    }
                }
            }

            if(elencoConvalidaSconti.size() > 0){
                elencoOggetti.add("PRONTI DA CONVALIDARE");
                elencoOggetti.addAll(elencoConvalidaSconti);
            }
            if(elencoScontiServer.size() > 0){
                elencoOggetti.add("DA RISCUOTERE");
                elencoOggetti.addAll(elencoScontiServer);
            }

            swipeRefreshLayout.setEnabled(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(justSelfieActivity, LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(justSelfieActivity));
            recyclerView.setAdapter(new AdapterSconto(justSelfieActivity, elencoOggetti));
            emptyContainer.setVisibility(elencoOggetti.size() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    public static class SelfieFragment extends Fragment {

        private JustSelfieActivity justSelfieActivity;
        @InjectView(R.id.fragment_child_recyclerview)
        RecyclerView recyclerView;
        @InjectView(R.id.fragment_child_swiperefresh_layout)
        SwipeRefreshLayout swipeRefreshLayout;
        @InjectView(R.id.fragment_child_empty_container)
        View emptyContainer;
        @InjectView(R.id.fragment_child_empty_message)
        TextView emptyMessage;

        public static SelfieFragment newInstance(ArrayList<Selfie> selfie) {
            SelfieFragment selfieFragment = new SelfieFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("selfie", selfie);
            selfieFragment.setArguments(bundle);
            return selfieFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            View v = inflater.inflate(R.layout.fragment_child_elenco_base, null);
            ButterKnife.inject(this, v);
            justSelfieActivity = (JustSelfieActivity) getActivity();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            List<Object> elencoOggetti = new ArrayList<>();
            if(getArguments() != null && getArguments().containsKey("selfie")) {
                ArrayList<Selfie> selfie = (ArrayList<Selfie>) getArguments().getSerializable("selfie");
                if(selfie != null && selfie.size() > 0) {
                    elencoOggetti.add("IN CUI SEI TAGGATO");
                    elencoOggetti.addAll(selfie);

                }
            }
            String query = "SELECT * FROM " + QueryBuilder.tabellaSelfie + " WHERE 1=1";
            DatabaseHelper db = new DatabaseHelper(justSelfieActivity);
            Cursor c = db.rawQuery(query);
            ArrayList<ConvalidaSelfie> selfieDaConvalidare = new ArrayList<>();
            while(c.moveToNext()){
                int id  = c.getInt(c.getColumnIndex("_id"));
                String json = c.getString(c.getColumnIndex("json"));
                ConvalidaSelfie convalidaSelfie = new Gson().fromJson(json, ConvalidaSelfie.class);
                convalidaSelfie.setId(id);
                selfieDaConvalidare.add(convalidaSelfie);
            }
            if(selfieDaConvalidare.size() > 0){
                elencoOggetti.add("PRONTI DA CONVALIDARE");
                elencoOggetti.addAll(selfieDaConvalidare);
            }
            swipeRefreshLayout.setEnabled(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(justSelfieActivity, LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(justSelfieActivity));
            recyclerView.setAdapter(new AdapterSelfie(justSelfieActivity, elencoOggetti));
            emptyContainer.setVisibility(elencoOggetti.size() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    public static class GiftCardFragment extends Fragment {
        private JustSelfieActivity justSelfieActivity;
        @InjectView(R.id.fragment_child_recyclerview)
        RecyclerView recyclerView;
        @InjectView(R.id.fragment_child_swiperefresh_layout)
        SwipeRefreshLayout swipeRefreshLayout;
        @InjectView(R.id.fragment_child_empty_container)
        View emptyContainer;
        @InjectView(R.id.fragment_child_empty_message)
        TextView emptyMessage;

        public static GiftCardFragment newInstance() {
            GiftCardFragment giftCardFragment = new GiftCardFragment();
            return giftCardFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            View v = inflater.inflate(R.layout.fragment_child_elenco_base, null);
            ButterKnife.inject(this, v);
            justSelfieActivity = (JustSelfieActivity) getActivity();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            swipeRefreshLayout.setEnabled(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(justSelfieActivity, LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(justSelfieActivity));
            recyclerView.setAdapter(new AdapterGiftCard(justSelfieActivity, PreferenceManager.getUtente(justSelfieActivity).getSelfieCoin()));
            emptyContainer.setVisibility(View.GONE );
        }


    }

}
