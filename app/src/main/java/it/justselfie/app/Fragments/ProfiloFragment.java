package it.justselfie.app.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import it.justselfie.app.Activity.AnalyticsActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Adapter.AdapterMessaggioUtente;
import it.justselfie.app.CustomViews.PieView;
import it.justselfie.app.Model.MessaggioUtente;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 25/10/2016.
 */

public class ProfiloFragment extends Fragment {

    @InjectView(R.id.fragment_profilo_tablayout)
    TabLayout tabLayout;

    @InjectView(R.id.fragment_profilo_viewpager)
    ViewPager viewPager;

    @InjectView(R.id.fragment_profilo_progressbar)
    ProgressBar progressBar;

    private Utente utente;

    private ArrayList<MessaggioUtente> elencoAffidabilitaMsg;
    private ArrayList<MessaggioUtente> elencoSelfieCoinMsg;
    private JustSelfieActivity justSelfieActivity;

    private SelfieCoinFragment selfieCoinFragment;
    private AffidabilitaFragment affidabilitaFragment;
    private DatiProfiloFragment datiProfiloFragment;

    public static ProfiloFragment newInstance() {
        return new ProfiloFragment();
    }

    public ProfiloFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup root, Bundle savedInstance) {
        super.onCreateView(layoutInflater, root, savedInstance);
        View view = layoutInflater.inflate(R.layout.fragment_profilo, null);
        ButterKnife.inject(this, view);
        selfieCoinFragment = SelfieCoinFragment.newInstance();
        datiProfiloFragment = DatiProfiloFragment.newInstance();
        affidabilitaFragment = AffidabilitaFragment.newInstance();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        justSelfieActivity = (JustSelfieActivity) getActivity();
        utente = PreferenceManager.getUtente(justSelfieActivity);
        tabLayout.addTab(tabLayout.newTab().setText("Selfie coin"));
        tabLayout.addTab(tabLayout.newTab().setText("Dati"));
        tabLayout.addTab(tabLayout.newTab().setText("Affidabilita"));
        PageAdapter pageAdapter = new PageAdapter(getChildFragmentManager());
        viewPager.setAdapter(pageAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1, false);
        Tools.changeFontInViewGroup(tabLayout, "fonts/NeutraTextDemi.otf");
        viewPager.setVisibility(View.GONE);
        new SchedaProfiloDownloader().execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        ((AnalyticsActivity) getActivity()).getTracker().setScreenName("Profilo");
        ((AnalyticsActivity) getActivity()).getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }


    void showError(String message) {
        Toast.makeText(justSelfieActivity, message, Toast.LENGTH_LONG).show();
    }

    void showView(JSONObject object) {
        try {
            utente = new Gson().fromJson(object.getJSONObject("utente").toString(), Utente.class);
            PreferenceManager.setUtente(utente, justSelfieActivity);
            elencoAffidabilitaMsg = new Gson().fromJson(object.getJSONArray("messaggiAffidabilita").toString(), new TypeToken<List<MessaggioUtente>>() {
            }.getType());
            elencoSelfieCoinMsg = new Gson().fromJson(object.getJSONArray("messaggiSelfieCoin").toString(), new TypeToken<List<MessaggioUtente>>() {
            }.getType());
            selfieCoinFragment.setDati(elencoSelfieCoinMsg, utente.getSelfieCoin());
            affidabilitaFragment.setDati(elencoAffidabilitaMsg, utente.getAffidabilita());
            datiProfiloFragment.setDati(utente);

        } catch (Exception ex) {
            showError(ex.getMessage());
        }
    }


    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public String getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Selfie Coin";
                case 1:
                    return "Dati";
                case 2:
                    return "Affidabilita";
                default:
                    return "Dati";
            }

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return selfieCoinFragment;
                case 1:
                    return datiProfiloFragment;
                case 2:
                    return affidabilitaFragment;
                default:
                    return selfieCoinFragment;
            }


        }
    }

    private class SchedaProfiloDownloader extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {

            try {
                String s = new Gson().toJson(utente);
                return new JSONObject(NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_utente_scheda, s));
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            viewPager.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            try {
                if (object.getBoolean("success")) {
                    showView(object.getJSONObject("response"));
                } else {
                    showError(object.getString("response"));
                }
            } catch (Exception ex) {
                showError(ex.getMessage());
            }

        }
    }


    public static class SelfieCoinFragment extends Fragment {

        @InjectView(R.id.fragment_profilo_selfiecoin_icona)
        ImageView iconaImageView;

        @InjectView(R.id.fragment_profilo_selfiecoin_messaggi)
        RecyclerView messaggiRecyclerView;

        @InjectView(R.id.fragment_profilo_selfiecoin_numero)
        TextView selfieCoinTextView;

        @InjectView(R.id.fragment_profilo_selfiecoin_usa)
        Button imageButton;

        private JustSelfieActivity justSelfieActivity;

        public static SelfieCoinFragment newInstance() {
            SelfieCoinFragment selfieCoinFragment = new SelfieCoinFragment();
            return selfieCoinFragment;
        }

        public static SelfieCoinFragment newInstance(ArrayList<MessaggioUtente> messaggi, int selfieCoin) {
            SelfieCoinFragment selfieCoinFragment = new SelfieCoinFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("messaggi", messaggi);
            bundle.putInt("selfieCoin", selfieCoin);
            selfieCoinFragment.setArguments(bundle);
            return selfieCoinFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            setRetainInstance(true);
            View v = inflater.inflate(R.layout.fragment_profilo_selfiecoin, null);
            ButterKnife.inject(this, v);
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            justSelfieActivity = (JustSelfieActivity) getActivity();
        }

        public void setDati(ArrayList<MessaggioUtente> messaggi, int selfieCoin) {
            selfieCoinTextView.setText(selfieCoin + "");
            messaggiRecyclerView.setLayoutManager(new LinearLayoutManager(justSelfieActivity, LinearLayoutManager.VERTICAL, false));
            messaggiRecyclerView.setAdapter(new AdapterMessaggioUtente(justSelfieActivity, messaggi));
            imageButton.setOnClickListener(view -> justSelfieActivity.vediPremi(0));
        }
    }

    public static class DatiProfiloFragment extends Fragment {

        @InjectView(R.id.fragment_profilo_scheda_immagine_profilo)
        CircleImageView immagineProfilo;

        @InjectView(R.id.fragment_profilo_scheda_button)
        Button salvaButton;

        @InjectView(R.id.fragment_profilo_scheda_email)
        TextView emailText;

        @InjectView(R.id.fragment_profilo_scheda_nome)
        TextView nomeText;

        @InjectView(R.id.fragment_profilo_scheda_switch_facebook)
        Switch switchFacebook;

        @InjectView(R.id.fragment_profilo_scheda_switch_instagram)
        Switch switchInstagram;

        private ProgressDialog progressDialog;

        private Utente utente;
        private JustSelfieActivity justSelfieActivity;




        public static DatiProfiloFragment newInstance() {
            DatiProfiloFragment datiProfiloFragment = new DatiProfiloFragment();
            return datiProfiloFragment;
        }

        public static DatiProfiloFragment newInstance(Utente utente) {
            DatiProfiloFragment datiProfiloFragment = new DatiProfiloFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("utente", utente);
            return datiProfiloFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            setRetainInstance(true);
            View v = inflater.inflate(R.layout.fragment_profilo_scheda, null);
            justSelfieActivity = (JustSelfieActivity) getActivity();
            ButterKnife.inject(this, v);
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            salvaButton.setOnClickListener((v) -> {
                if (nomeText.getText().toString().isEmpty()) {
                    nomeText.setError("Inserire il nome");
                    nomeText.requestFocus();
                    return;
                }
                if (emailText.getText().toString().isEmpty()) {
                    emailText.setError("Inserire l'email");
                    emailText.requestFocus();
                    return;
                }
                Utente temp = utente;
                String email = emailText.getText().toString().trim();
                String nome = nomeText.getText().toString().trim();
                temp.setEmail(email);
                temp.setNome(nome);
                progressDialog = new ProgressDialog(justSelfieActivity);
                progressDialog.setMessage("Aggiornamento profilo");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... voids) {
                        String jsonUp = new Gson().toJson(temp);
                        return NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_utente_aggiorna, jsonUp);
                    }

                    @Override
                    public void onPostExecute(String res) {
                        progressDialog.dismiss();
                        try {
                            JSONObject result = new JSONObject(res);
                            if (result.getBoolean("success")) {
                                if (result.getJSONObject("response").getInt("auth") == 1) {
                                    utente = new Gson().fromJson(result.getJSONObject("response").getJSONObject("utente").toString(), Utente.class);
                                    PreferenceManager.setUtente(utente, justSelfieActivity);
                                    Toast.makeText(justSelfieActivity, "Aggiornamento effettuato con successo", Toast.LENGTH_LONG).show();
                                } else {
                                    emailText.setError("Email gia esistente");
                                    emailText.requestFocus();
                                    return;
                                }
                            } else {
                                throw new Exception(result.getString("response"));
                            }
                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Toast.makeText(justSelfieActivity, ex.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }.execute();
            });
        }


        public void setDati(Utente usr) {
            this.utente = usr;
            emailText.setText(utente.getEmail());
            nomeText.setText(utente.getNome());
            switchFacebook.setChecked((utente.getUserIdFB() > 0));
            switchInstagram.setChecked(utente.getUserIdINS() > 0);

            switchFacebook.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b && utente.getUserIdFB() == 0) doFacebookAccess();
                else switchFacebook.setChecked(true);
            });
            if (utente.getImmagineProfilo() != null && !utente.getImmagineProfilo().isEmpty()) {
                Picasso.with(justSelfieActivity).load(utente.getImmagineProfilo()).into(immagineProfilo);
            }

        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            justSelfieActivity.getFacebookReadCallbackManager().onActivityResult(requestCode, resultCode, data);
        }

        public void doFacebookAccess() {

            LoginManager.getInstance().registerCallback(justSelfieActivity.getFacebookReadCallbackManager(), new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    if (loginResult.getRecentlyDeniedPermissions().size() > 0) {
                        Toast.makeText(justSelfieActivity, "Ci dispiace :( Devi accettare tutti i permessi per offrirti una migliore esperienza su JustSelfie", Toast.LENGTH_LONG).show();
                    } else {
                        progressDialog = new ProgressDialog(justSelfieActivity);
                        progressDialog.setMessage("Accesso in corso");
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();
                        readFacebookData();
                    }
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    error.printStackTrace();

                }
            });
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends", "email", "public_profile"));
        }

        public void readFacebookData() {
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), (object, response) -> {
                try {
                    String id = object.getString("id");
                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected String doInBackground(Void... voids) {
                            return NetworkManager.doApiFORMPOSTRequest(justSelfieActivity, NetworkManager.endpoint_utente_checkEmail, "userIdFB=" + id);
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            try {
                                if (result != null && !result.isEmpty()) {
                                    JSONObject jsonObject = new JSONObject(result);
                                    if (jsonObject.getBoolean("success")) {
                                        String email = jsonObject.getJSONObject("response").getString("email");
                                        if (email.isEmpty() || utente.getEmail().equalsIgnoreCase(email)) {
                                            utente.setSesso(object.getString("gender"));
                                            utente.setUserIdFB(Long.parseLong(object.getString("id")));
                                            utente.setImmagineProfilo("https://graph.facebook.com/" + utente.getUserIdFB() + "/picture?type=large");
                                            readFacebookFriends();
                                        } else {
                                            switchFacebook.setChecked(false);
                                            progressDialog.dismiss();
                                            Toast.makeText(justSelfieActivity, "Account facebook associato ad un altro utente JustSelfie.", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        throw new Exception("");
                                    }
                                } else {
                                    throw new Exception("");
                                }
                            } catch (Exception e) {
                                switchFacebook.setChecked(false);
                                progressDialog.dismiss();
                                Toast.makeText(justSelfieActivity, "Errore durante controllo email", Toast.LENGTH_LONG).show();
                            }

                        }
                    }.execute();

                } catch (Exception ex) {
                    switchFacebook.setChecked(false);
                    progressDialog.dismiss();
                    Toast.makeText(justSelfieActivity, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "gender, name, email");
            request.setParameters(parameters);
            request.executeAsync();
        }

        public void readFacebookFriends() {
            new GraphRequest().newMyFriendsRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONArrayCallback() {
                @Override
                public void onCompleted(JSONArray array, GraphResponse response) {
                    try {
                        if (array != null) {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject friendObject = array.getJSONObject(i);
                                Utente amico = new Utente();
                                amico.setNome(friendObject.getString("name"));
                                amico.setUserIdFB(Long.parseLong(friendObject.getString("id")));
                                utente.getAmiciFB().add(amico);
                            }
                            collegaFB();
                        }

                    } catch (Exception ex) {
                        progressDialog.dismiss();
                        ex.printStackTrace();
                    }
                }
            }).executeAsync();
        }

        public void collegaFB() {
            new AsyncTask<Void, Void, JSONObject>() {
                @Override
                protected JSONObject doInBackground(Void... params) {
                    try {
                        String response = NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_utente_collegaFB, new Gson().toJson(utente));
                        return new JSONObject(response);
                    } catch (Exception ex) {
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(JSONObject result) {

                    progressDialog.dismiss();
                    try {
                        if (result.getBoolean("success")) {
                            JSONObject response = result.getJSONObject("response");

                            utente = new Gson().fromJson(response.getJSONObject("utente").toString(), Utente.class);
                            PreferenceManager.setUtente(utente, justSelfieActivity);
                            if (utente.getImmagineProfilo() != null && !utente.getImmagineProfilo().isEmpty()) {
                                Picasso.with(justSelfieActivity).load(utente.getImmagineProfilo()).into(immagineProfilo);
                            }
                            switchFacebook.setEnabled((utente.getUserIdFB() == 0));


                        } else {
                            throw new Exception(result.getString("response"));
                        }
                    } catch (Exception ex) {
                        switchFacebook.setChecked(false);
                        progressDialog.dismiss();
                        ErroreFragment erroreFragment = ErroreFragment.newInstance(ex.getMessage());
                        erroreFragment.show(getChildFragmentManager(), "errore");
                    }
                }
            }.execute();
        }
    }

    public static class AffidabilitaFragment extends Fragment {

        @InjectView(R.id.fragment_profilo_affidabilita_messaggi)
        RecyclerView messaggiRecyclerView;

        @InjectView(R.id.fragment_profilo_affidabilita_pieview)
        PieView pieView;

        private JustSelfieActivity justSelfieActivity;


        public static AffidabilitaFragment newInstance() {
            AffidabilitaFragment selfieCoinFragment = new AffidabilitaFragment();
            return selfieCoinFragment;
        }

        public static AffidabilitaFragment newInstance(ArrayList<MessaggioUtente> messaggi, double affidabilita) {
            AffidabilitaFragment affidabilitaFragment = new AffidabilitaFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("messaggi", messaggi);
            bundle.putDouble("affidabilita", affidabilita);
            affidabilitaFragment.setArguments(bundle);
            return affidabilitaFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            setRetainInstance(true);
            View v = inflater.inflate(R.layout.fragment_profilo_affidabilita, null);
            ButterKnife.inject(this, v);
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            justSelfieActivity = (JustSelfieActivity) getActivity();
            justSelfieActivity.supportInvalidateOptionsMenu();
            Tools.changeFontInViewGroup(pieView.getBaseLayout(), "fonts/HouseMvmt-Custom.otf");


        }


        @Override
        public void onResume() {
            super.onResume();
        }

        public void setDati(ArrayList<MessaggioUtente> messaggi, double affidabilita) {
            pieView.setPercentage((float) affidabilita);
            messaggiRecyclerView.setLayoutManager(new LinearLayoutManager(justSelfieActivity, LinearLayoutManager.VERTICAL, false));
            messaggiRecyclerView.setAdapter(new AdapterMessaggioUtente(justSelfieActivity, messaggi));
        }
    }

}
