package it.justselfie.app.Fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import it.justselfie.app.Activity.AnalyticsActivity;
import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Adapter.AdapterWall;
import it.justselfie.app.CustomViews.NonSwipeableViewPager;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.GpsTracker;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 02/04/2016.
 */
public class WallFragment extends Fragment {


    private Ricerca ricerca;
    private double userLat, userLon;

    private GpsTracker gpsTracker;

    @InjectView(R.id.fragment_wall_viewpager)
    NonSwipeableViewPager viewPager;

    @InjectView(R.id.fragment_wall_progressbar)
    ProgressBar progressBar;

    private AppCompatActivity justSelfieActivity;

    private WallElencoFragment wallElencoFragment;
    private WallSchedaFragment wallSchedaFragment;




    public static WallFragment newInstance(ArrayList<Selfie> selfie){
        WallFragment wallFragment = new WallFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("selfie", selfie);
        wallFragment.setArguments(bundle);
        return wallFragment;
    }

    public static WallFragment newInstance(Ricerca ricerca) {
        WallFragment wallFragment = new WallFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("ricerca", ricerca);
        wallFragment.setArguments(bundle);
        return wallFragment;
    }

    public static WallFragment newInstance(){
        return new WallFragment();
    }

    public WallFragment() {

    }

    public boolean goBack(){
        if(viewPager.getCurrentItem() == 0)
            return false;
        else
            viewPager.setCurrentItem(0);
        return true;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
        super.onCreateView(inflater, root, bundle);
        View view = inflater.inflate(R.layout.fragment_wall, null);
        ButterKnife.inject(this, view);
        justSelfieActivity = (AppCompatActivity) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if(getArguments() != null && getArguments().containsKey("ricerca")){
            Ricerca ricerca = (Ricerca) getArguments().getSerializable("ricerca");
            wallElencoFragment = WallElencoFragment.newInstance(ricerca);
            wallSchedaFragment = WallSchedaFragment.newInstance();
            viewPager.setAdapter(new PageAdapter(getChildFragmentManager()));
            if(ricerca.getIdSelfie() > 0){
                vediScheda(ricerca);
            }
        }else if(getArguments() != null && getArguments().containsKey("selfie")){
            ArrayList<Selfie> elencoSelfie = (ArrayList<Selfie>) getArguments().getSerializable("selfie");
            wallElencoFragment = WallElencoFragment.newInstance(elencoSelfie);
            wallSchedaFragment = WallSchedaFragment.newInstance();
            viewPager.setAdapter(new PageAdapter(getChildFragmentManager()));

        }else{
            wallElencoFragment = WallElencoFragment.newInstance();
            wallSchedaFragment = WallSchedaFragment.newInstance();
            viewPager.setAdapter(new PageAdapter(getChildFragmentManager()));
        }
    }


    public void vediScheda(Ricerca ricerca) {
        viewPager.setCurrentItem(1, true);
        wallSchedaFragment.vedi(ricerca);
    }

    public boolean tornaIndietro() {
        if (viewPager.getCurrentItem() == 1) {
            viewPager.setCurrentItem(0, true);
            return true;
        }
        return false;
    }


    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {

            super(fm);
        }


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return wallElencoFragment;
                case 1:
                    return wallSchedaFragment;
                default:
                    return wallElencoFragment;
            }
        }
    }


    public static class WallElencoFragment extends Fragment {

        @InjectView(R.id.fragment_child_recyclerview)
        RecyclerView recyclerView;
        @InjectView(R.id.fragment_child_swiperefresh_layout)
        SwipeRefreshLayout swipeRefreshLayout;
        @InjectView(R.id.fragment_child_empty_container)
        View emptyContainer;
        @InjectView(R.id.fragment_child_empty_message)
        TextView emptyMessage;

        private List<Selfie> elencoSelfie;
        private AppCompatActivity justSelfieActivity;

        private Ricerca ricerca;

        public static WallElencoFragment newInstance(Ricerca ricerca) {
            WallElencoFragment wallElencoFragment = new WallElencoFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("ricerca", ricerca);
            wallElencoFragment.setArguments(bundle);
            return wallElencoFragment;
        }

        public static WallElencoFragment newInstance(ArrayList<Selfie> selfie ) {
            WallElencoFragment wallElencoFragment = new WallElencoFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("selfie", selfie);
            wallElencoFragment.setArguments(bundle);
            return wallElencoFragment;
        }

        public static WallElencoFragment newInstance(){
            return new WallElencoFragment();
        }

        public WallElencoFragment() {

        }

        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
            super.onCreateView(inflater, root, bundle);
            View view = inflater.inflate(R.layout.fragment_child_elenco_base, null);
            ButterKnife.inject(this, view);
            return view;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            justSelfieActivity = (AppCompatActivity) getActivity();
            recyclerView.setLayoutManager(new GridLayoutManager(justSelfieActivity, 3, LinearLayoutManager.VERTICAL, false));


            if(getArguments() != null && getArguments().containsKey("selfie")){
                elencoSelfie = (ArrayList<Selfie>) getArguments().getSerializable("selfie");
                swipeRefreshLayout.setEnabled(false);
                showView();
                return;
            }else{
                ricerca = new Ricerca();
                swipeRefreshLayout.setOnRefreshListener(() -> {refresh();});
                refresh();
            }
        }

        @Override
        public void onResume(){
            super.onResume();
            ((AnalyticsActivity) getActivity()).getTracker().setScreenName("Wall Elenco");
            ((AnalyticsActivity) getActivity()).getTracker().send(new HitBuilders.ScreenViewBuilder().build());
        }


        private void refresh() {
            swipeRefreshLayout.setRefreshing(true);
            emptyContainer.setVisibility(View.GONE);
            recyclerView.setAdapter(new AdapterWall(justSelfieActivity, new ArrayList<>(), this));
            new WallListaDownloader().execute();

        }

        private void showView(){
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setEnabled(false);
            recyclerView.setAdapter(new AdapterWall(justSelfieActivity, elencoSelfie, this));

            emptyContainer.setVisibility(elencoSelfie.size() > 0 ? View.GONE : View.VISIBLE);
        }

        private void showError(String errore){
            emptyContainer.setVisibility(View.VISIBLE);
            Toast.makeText(justSelfieActivity, errore, Toast.LENGTH_LONG).show();
        }

        public void vediScheda(Ricerca ricerca){
            ((WallFragment)getParentFragment()).vediScheda(ricerca);
        }

        private class WallListaDownloader extends AsyncTask<Void, Void, String>{

            @Override
            protected String doInBackground(Void... voids) {
                String searchString = new Gson().toJson(ricerca);
                return NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_wall_lista, searchString);
            }

            @Override
            protected void onPostExecute(String response){
                try {
                    if (response != null && !response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        if(jsonObject.getBoolean("success")){
                            elencoSelfie = new Gson().fromJson(jsonObject.getJSONObject("response").getJSONArray("selfies").toString(), new TypeToken<ArrayList<Selfie>>() {
                            }.getType());
                            showView();
                        }else{
                            showError(jsonObject.getString("response"));
                        }

                    }
                }catch (Exception ex){
                    showError(ex.getMessage());
                }
            }
        }
    }


    public static class WallSchedaFragment extends Fragment {

        @InjectView(R.id.fragment_wall_scheda_esercente_button)
        FloatingActionButton vediEsercente;

        private AppCompatActivity justSelfieActivity;

        private Ricerca ricerca;

        private Selfie selfie;

        @InjectView(R.id.fragment_wall_scheda_data)
        TextView data;

        @InjectView(R.id.fragment_wall_scheda_utente_nome)
        TextView nomeUtente;

        @InjectView(R.id.fragment_wall_scheda_esercente_nome)
        TextView nomeEsercente;

        @InjectView(R.id.fragment_wall_scheda_esercente_indirizzo)
        TextView indirizzoEsercente;

        @InjectView(R.id.fragment_wall_scheda_selfie)
        ImageView selfieImageView;

        @InjectView(R.id.fragment_wall_scheda_utente_immagine_profilo)
        ImageView immagineProfilo;

        @InjectView(R.id.fragment_wall_scheda_tag_icona)
        ImageView iconaTag;

        @InjectView(R.id.fragment_wall_scheda_tags_layout)
        LinearLayout tagsLayout;

        public static WallSchedaFragment newInstance() {
            return new WallSchedaFragment();
        }

        public WallSchedaFragment() {

        }

        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
            super.onCreateView(inflater, root, bundle);
            View view = inflater.inflate(R.layout.fragment_wall_scheda, null);
            ButterKnife.inject(this, view);
            return view;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            justSelfieActivity = (AppCompatActivity) getActivity();
        }

        @Override
        public void onResume(){
            super.onResume();
            ((AnalyticsActivity) getActivity()).getTracker().setScreenName("Wall Dettaglio");
            ((AnalyticsActivity) getActivity()).getTracker().send(new HitBuilders.ScreenViewBuilder().build());
        }

        public void vedi(Ricerca ricerca){
            this.ricerca = ricerca;
            new WallSchedaDownloader().execute();
        }

        private void showView(){
            ImageLoader imageLoader = ImageLoader.getInstance();
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.mipmap.placeholder) // resource or drawable
                    .showImageForEmptyUri(R.mipmap.no_image) // resource or drawable
                    .showImageOnFail(R.mipmap.no_image) // resource or drawable
                    .resetViewBeforeLoading(false)  // default
                    .cacheInMemory(true) // default
                    .cacheOnDisk(true) // default

                    .considerExifParams(true).build();
            imageLoader.displayImage(justSelfieActivity.getString(R.string.radiceSelfie) + selfie.getSelfie(), selfieImageView, options);
            /*Picasso.with(justSelfieActivity)
                    .load(justSelfieActivity.getString(R.string.radiceSelfie) + selfie.getSelfie())
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.no_image)
                    .centerInside()
                    .fit()
                    .rotate(60)
                    .into(selfieImageView);*/
            nomeEsercente.setText(selfie.getEsercente().getNome());
            data.setText(Tools.getDataTimeDisplay(selfie.getRegistratoIl()));
            Picasso.with(justSelfieActivity).load(selfie.getUtente().getImmagineProfilo()).into(immagineProfilo);
            nomeUtente.setText(selfie.getUtente().getNome());
            indirizzoEsercente.setText(selfie.getEsercente().getIndirizzo());
            vediEsercente.setOnClickListener(view1 -> {
                Ricerca ricerca = new Ricerca();
                ricerca.setIdEsercente(selfie.getEsercente().getId());
                Intent intent = new Intent(justSelfieActivity, EsercenteActivity.class);
                intent.putExtra("ricerca", ricerca);
                justSelfieActivity.startActivity(intent);
            });
            if(selfie.getAmiciTaggatiFB() != null && selfie.getAmiciTaggatiFB().size() > 0) {
                tagsLayout.removeAllViews();
                for (Utente amicoTag : selfie.getAmiciTaggatiFB()) {
                    View v = justSelfieActivity.getLayoutInflater().inflate(R.layout.item_amico_taggato, null);
                    v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    TextView nomeAmico = (TextView) v.findViewById(R.id.textView1);
                    nomeAmico.setText(amicoTag.getNome());
                    nomeAmico.setTextSize(20);
                    CircleImageView circleImageView = (CircleImageView) v.findViewById(R.id.imageView1);
                    Picasso.with(justSelfieActivity)
                            .load(amicoTag.getImmagineProfilo())
                            .placeholder(R.mipmap.placeholder)
                            .error(R.mipmap.no_image)
                            .centerCrop()
                            .fit()
                            .into(circleImageView);
                    tagsLayout.addView(v);
                }
                tagsLayout.setVisibility(View.VISIBLE);
                iconaTag.setVisibility(View.VISIBLE);
            }else{
                tagsLayout.setVisibility(View.GONE);
                iconaTag.setVisibility(View.GONE);
            }

        }

        private void showError(String errore){
            Toast.makeText(justSelfieActivity, errore, Toast.LENGTH_LONG).show();
        }

        private class WallSchedaDownloader extends AsyncTask<Void, Void, String>{

            @Override
            protected String doInBackground(Void... voids) {
                String searchString = new Gson().toJson(ricerca);
                return NetworkManager.doApiPOSTRequest(justSelfieActivity, NetworkManager.endpoint_wall_scheda, searchString);
            }

            @Override
            protected void onPostExecute(String response){
                try {
                    if (response != null && !response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        if(jsonObject.getBoolean("success")){
                            selfie = new Gson().fromJson(jsonObject.getJSONObject("response").getJSONObject("selfie").toString(), Selfie.class);
                            showView();
                        }else{
                            showError(jsonObject.getString("response"));
                        }

                    }
                }catch (Exception ex){
                    showError(ex.getMessage());
                }
            }
        }


    }


}
