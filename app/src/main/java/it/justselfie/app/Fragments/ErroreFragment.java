package it.justselfie.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.R;

/**
 * Created by Davide on 11/11/2016.
 */

public class ErroreFragment extends DialogFragment {

    @InjectView(R.id.dialog_errore_messaggio)
    TextView messaggio;

    @InjectView(R.id.dialog_errore_button)
    Button button;

    private View.OnClickListener listener;


    public static ErroreFragment newInstance(String errore) {
        ErroreFragment offertaFragment = new ErroreFragment();
        Bundle b = new Bundle();
        b.putString("errore", errore);
        offertaFragment.setArguments(b);
        return offertaFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_errore, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        messaggio.setText(getArguments().getString("errore"));
        button.setOnClickListener(this.listener == null  ? view1 -> dismiss() : this.listener);

    }

    public void setButtonListener(View.OnClickListener listener){
        this.listener = listener;
    }



    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        getDialog().setCancelable(true);
        super.onResume();
    }

}

