package it.justselfie.app.Fragments;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.icu.text.IDNA;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.apache.commons.lang3.StringEscapeUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.R;
import it.justselfie.app.Tools.Tools;

import static it.justselfie.app.Tools.Tools.getResizedBitmap;

/**
 * Created by Davide on 05/01/2017.
 */

public class InfoContattiFragment extends Fragment implements OnMapReadyCallback {

    @InjectView(R.id.fragment_info_contatti_descrizione)
    ExpandableTextView descrizione;

    @InjectView(R.id.fragment_info_contatti_indirizzo)
    TextView indirizzo;

    @InjectView(R.id.fragment_info_contatti_email)
    TextView email;

    @InjectView(R.id.fragment_info_contatti_telefono)
    TextView telefono;

    private GoogleMap googleMap;

    private AppCompatActivity appCompatActivity;
    private SupportMapFragment mapFragment;

    public static InfoContattiFragment newInstance(String nome, String descrizione, String indirizzo, String email, String telefono, double lat, double lon){
        InfoContattiFragment infoContattiFragment = new InfoContattiFragment();
        Bundle savedInstance = new Bundle();
        savedInstance.putString("nome", nome);
        savedInstance.putString("descrizione", descrizione);
        savedInstance.putString("indirizzo", indirizzo);
        savedInstance.putString("email", email);
        savedInstance.putString("telefono", telefono);
        savedInstance.putDouble("latitudine", lat);
        savedInstance.putDouble("longitudine", lon);
        infoContattiFragment.setArguments(savedInstance);
        return infoContattiFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstance){
        super.onCreateView(inflater, root, savedInstance);
        View view = inflater.inflate(R.layout.fragment_info_contatti, null);
        ButterKnife.inject(this, view);
        appCompatActivity = (AppCompatActivity) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        String desc = StringEscapeUtils.unescapeHtml4(getArguments().getString("descrizione"));

        descrizione.setText(Html.fromHtml(desc));
        indirizzo.setText(StringEscapeUtils.unescapeHtml4(getArguments().getString("indirizzo")));
        email.setText(getArguments().getString("email"));
        telefono.setText(getArguments().getString("telefono"));
        telefono.setOnClickListener(view1 -> {
            if(!telefono.getText().toString().isEmpty()){
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", telefono.getText().toString(), null));
                startActivity(intent);
            }
        });
        email.setOnClickListener(view12 -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:?subject=");
            intent.setData(data);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email.getText().toString()});
            startActivity(Intent.createChooser(intent, "Invia email"));
        });

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_info_contatti_mappa);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (googleMap != null) {
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(getArguments().getDouble("latitudine"), getArguments().getDouble("longitudine")))
                    .title(getArguments().getString("nome"))
                    .icon(BitmapDescriptorFactory.fromBitmap(getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.icona_locazione_jspoint), Tools.dpToPx(appCompatActivity, 48), Tools.dpToPx(appCompatActivity, 48)))));
            CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(getArguments().getDouble("latitudine"), getArguments().getDouble("longitudine")), 16);
            googleMap.animateCamera(cu);
        }
    }

}
