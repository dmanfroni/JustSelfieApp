package it.justselfie.app.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Activity.EsercenteActivity;
import it.justselfie.app.Activity.JustSelfieActivity;
import it.justselfie.app.Adapter.AdapterOffertaAcquistabile;
import it.justselfie.app.Adapter.AdapterOffertaVetrina;
import it.justselfie.app.AppConstants;
import it.justselfie.app.Database.DatabaseHelper;
import it.justselfie.app.Database.QueryBuilder;
import it.justselfie.app.Model.Campagna;
import it.justselfie.app.Model.ConvalidaOfferta;
import it.justselfie.app.Model.Sconto;
import it.justselfie.app.R;

/**
 * Created by Davide on 05/01/2017.
 */

public class PromoFragment extends Fragment {

    @InjectView(R.id.fragment_promo_offerte_layout)
    View offerteLayout;

    @InjectView(R.id.fragment_promo_offerte_disponibili_recyclerview)
    RecyclerView offerteDisponibiliRecyclerView;

    @InjectView(R.id.fragment_promo_offerta_esistente_layout)
    View offertaEsistenteLayout;

    @InjectView(R.id.fragment_promo_offerta_esistente_testo)
    TextView offertaEsistenteTesto;

    @InjectView(R.id.fragment_promo_offerta_esistente_visualizza_button)
    Button offertaEsistenteButton;

    @InjectView(R.id.fragment_promo_sconto_layout)
    View scontoLayout;

    @InjectView(R.id.fragment_promo_sconto_testo)
    TextView scontoTesto;

    @InjectView(R.id.fragment_promo_sconto_button)
    Button scontoButton;

    @InjectView(R.id.fragment_promo_selfiecoin_layout)
    View selfieCoinLayout;

    @InjectView(R.id.fragment_promo_selfiecoin_testo)
    TextView selfieCoinTesto;

    @InjectView(R.id.fragment_promo_selfiecoin_button)
    Button selfieCoinButton;

    private AppCompatActivity appCompatActivity;

    public PromoFragment(){

    }

    public static PromoFragment newInstance(Campagna campagna, String colore){
        PromoFragment promoFragment = new PromoFragment();
        Bundle savedInstance = new Bundle();
        savedInstance.putSerializable("campagna", campagna);
        savedInstance.putString("colore", colore);
        promoFragment.setArguments(savedInstance);
        return promoFragment;
    }

    public static PromoFragment newInstance(Sconto sconto){
        PromoFragment promoFragment = new PromoFragment();
        Bundle savedInstance = new Bundle();
        savedInstance.putSerializable("sconto", sconto);
        promoFragment.setArguments(savedInstance);
        return promoFragment;
    }

    public static PromoFragment newInstance(int selfieCoin){
        PromoFragment promoFragment = new PromoFragment();
        Bundle savedInstance = new Bundle();
        savedInstance.putInt("selfieCoin", selfieCoin);
        promoFragment.setArguments(savedInstance);
        return promoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstance){
        super.onCreateView(inflater, root, savedInstance);
        View view = inflater.inflate(R.layout.fragment_promo, null);
        ButterKnife.inject(this, view);
        appCompatActivity = (AppCompatActivity) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if(getArguments().containsKey("campagna")){
            offerteLayout.setVisibility(View.VISIBLE);
            scontoLayout.setVisibility(View.GONE);
            selfieCoinLayout.setVisibility(View.GONE);
            Campagna campagna = (Campagna) getArguments().getSerializable("campagna");
            String colore = getArguments().getString("colore");
            if(esisteCampagnaInSospeso(campagna.getId())){
                offerteDisponibiliRecyclerView.setVisibility(View.GONE);
                offertaEsistenteButton.setOnClickListener(view1 -> {
                    Intent intent = new Intent(appCompatActivity, JustSelfieActivity.class);
                    intent.putExtra("view", AppConstants.ACTION_OFFER_PENDING_VIEW);
                    intent.putExtra("id", 0);
                    startActivity(intent);
                });
            }else{
                offertaEsistenteTesto.setVisibility(View.GONE);
                offertaEsistenteButton.setVisibility(View.GONE);
                offerteDisponibiliRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                offerteDisponibiliRecyclerView.setAdapter(new AdapterOffertaAcquistabile(appCompatActivity, campagna.getOfferte(), 0.0, 0.0, colore, false));
                offerteDisponibiliRecyclerView.setVisibility(View.VISIBLE);
                offertaEsistenteButton.setOnClickListener(view14 -> {
                    Intent intent = new Intent(appCompatActivity, JustSelfieActivity.class);
                    intent.putExtra("view", AppConstants.ACTION_OFFER_PENDING_VIEW);
                    startActivity(intent);
                });
            }

        }else if(getArguments().containsKey("sconto")){
            Sconto sconto = (Sconto) getArguments().getSerializable("sconto");
            offerteLayout.setVisibility(View.GONE);
            scontoLayout.setVisibility(View.VISIBLE);
            scontoButton.setOnClickListener(view12 -> ((EsercenteActivity)appCompatActivity).riscuotiSconto());
            scontoTesto.setText(scontoTesto.getText().toString().replace("#SCONTO", sconto.getSconto() + ""));
            selfieCoinLayout.setVisibility(View.GONE);
        }else if(getArguments().containsKey("selfieCoin")){
            int selfiCoin = getArguments().getInt("selfieCoin");
            selfieCoinButton.setOnClickListener(view13 -> ((EsercenteActivity)appCompatActivity).riscuotiSelfieCoin());
            offerteLayout.setVisibility(View.GONE);
            scontoLayout.setVisibility(View.GONE);
            selfieCoinLayout.setVisibility(View.VISIBLE);
            selfieCoinTesto.setText(selfieCoinTesto.getText().toString().replace("#SELFIECOIN", selfiCoin + ""));
        }

    }

    private boolean esisteCampagnaInSospeso(int idCampagna){
        boolean found = false;
        String query = "SELECT * FROM " + QueryBuilder.tabellaAcquisti + " WHERE 1=1";
        DatabaseHelper db = new DatabaseHelper(appCompatActivity);
        Cursor c = db.rawQuery(query);
        while (c.moveToNext()) {
            String json = c.getString(c.getColumnIndex("json"));
            ConvalidaOfferta convalidaOfferta = new Gson().fromJson(json, ConvalidaOfferta.class);
            if (convalidaOfferta.getAcquisto().getCampagna().getId() == idCampagna)
                found = true;
        }
        return found;
    }

    public void setShowSnackBar(boolean showSnackBar) {
        if(showSnackBar != isSnackBarShow())
            if(offerteDisponibiliRecyclerView != null && offerteDisponibiliRecyclerView.getAdapter() != null)
            ((AdapterOffertaAcquistabile) offerteDisponibiliRecyclerView.getAdapter()).setShowSnackBar(showSnackBar);
    }

    public boolean isSnackBarShow(){
        if(offerteDisponibiliRecyclerView != null && offerteDisponibiliRecyclerView.getAdapter() != null)
        return ((AdapterOffertaAcquistabile)offerteDisponibiliRecyclerView.getAdapter()).getShowSnackBar();
        else return false;
    }
}
