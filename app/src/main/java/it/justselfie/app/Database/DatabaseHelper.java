/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Davide
 */
public class DatabaseHelper {
    private static final int DATABASE_VERSION = 2;
    private String DATABASE_NAME;
    private Context context;
    private DatabaseOpenHelper db_helper;
    private SQLiteDatabase database;

    public DatabaseHelper(Context context) {
        DATABASE_NAME = "JustSelfieApp";
        db_helper = new DatabaseOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public void openWritableMode() throws SQLException {
        database = db_helper.getWritableDatabase();
    }

    public void openReadableMode() throws SQLException {
        database = db_helper.getReadableDatabase();
    }

    public Cursor rawQuery(String query) {
        openReadableMode();
        Cursor c = database.rawQuery(query, null);
        return c;
    }

    public void close() {
        if (database != null) {
            database.close();
        }
    }

    public long insert(String tbl_name, ContentValues content) {
        openWritableMode();
        return database.insert(tbl_name, null, content);
    }

    public void delete(String tbl_name, int id){
        openWritableMode();
        database.delete(tbl_name, "_id=?", new String[]{id+""});
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {

        public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String query = QueryBuilder.createTableAcquisti();
            db.execSQL(query);
            query = QueryBuilder.createTableSconti();
            db.execSQL(query);
            query = QueryBuilder.createTableSelfie();
            db.execSQL(query);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
