package it.justselfie.app.Database;

/**
 * Created by Davide on 17/02/14.
 */
public class QueryBuilder {

    public static String tabellaAcquisti= "acquisti";

    public static String tabellaSconti= "sconti";

    public static String tabellaSelfie= "selfie";


    public static String createTableAcquisti() {
        String query = "CREATE TABLE acquisti ( " +
                "_id integer primary key autoincrement, " +
                "json text )";
        return query;

    }

    public static String createTableSconti() {
        String query = "CREATE TABLE sconti ( " +
                "_id integer primary key autoincrement, " +
                "json text )";
        return query;

    }

    public static String createTableSelfie() {
        String query = "CREATE TABLE selfie ( " +
                "_id integer primary key autoincrement, " +
                "json text )";
        return query;

    }


}
