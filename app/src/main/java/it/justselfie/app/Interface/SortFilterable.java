package it.justselfie.app.Interface;

import it.justselfie.app.Model.SortFilter;

/**
 * Created by Davide on 04/01/2017.
 */

public interface SortFilterable {
    public void applySortFilter(SortFilter sortFilter);
    public SortFilter getSortFilter();
}
