/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Contratto implements Serializable{

    private int id;
    private String nome;

    /*Offerte benvenuto*/
    private int offertaScontoMinimo;
    private int offertaSelfieCoin;

    /*Sconto su scontrino*/
    private int scontoScontrinoMinimo;
    private int scontoScontrinoRiduzioneNoLink;
    private int scontroScontrinoValidita;

    /*Selfie coin*/
    private int selfieCoin;
    private int selfieCoinRiduzioneNoLink;

    /*World of mouth*/
    private int womValidita;
    private int womSelfieCoinUtenteA;
    private int womSelfieCoinUtenteB;

    private double provvigioneSelfie;
    private double provvigioneOfferta;
    private double provvigioneWom;

    private double provvigioneSelfiePromo;
    private double provvigioneOffertaPromo;
    private int numeroSelfiePromo;
    private int numeroOffertePromo;




    public Contratto(){
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the offertaScontoMinimo
     */
    public int getOffertaScontoMinimo() {
        return offertaScontoMinimo;
    }

    /**
     * @param offertaScontoMinimo the offertaScontoMinimo to set
     */
    public void setOffertaScontoMinimo(int offertaScontoMinimo) {
        this.offertaScontoMinimo = offertaScontoMinimo;
    }

    /**
     * @return the offertaSelfieCoin
     */
    public int getOffertaSelfieCoin() {
        return offertaSelfieCoin;
    }

    /**
     * @param offertaSelfieCoin the offertaSelfieCoin to set
     */
    public void setOffertaSelfieCoin(int offertaSelfieCoin) {
        this.offertaSelfieCoin = offertaSelfieCoin;
    }

    /**
     * @return the scontoScontrinoMinimo
     */
    public int getScontoScontrinoMinimo() {
        return scontoScontrinoMinimo;
    }

    /**
     * @param scontoScontrinoMinimo the scontoScontrinoMinimo to set
     */
    public void setScontoScontrinoMinimo(int scontoScontrinoMinimo) {
        this.scontoScontrinoMinimo = scontoScontrinoMinimo;
    }

    /**
     * @return the scontoScontrinoRiduzioneNoLink
     */
    public int getScontoScontrinoRiduzioneNoLink() {
        return scontoScontrinoRiduzioneNoLink;
    }

    /**
     * @param scontoScontrinoRiduzioneNoLink the scontoScontrinoRiduzioneNoLink to set
     */
    public void setScontoScontrinoRiduzioneNoLink(int scontoScontrinoRiduzioneNoLink) {
        this.scontoScontrinoRiduzioneNoLink = scontoScontrinoRiduzioneNoLink;
    }

    /**
     * @return the scontroScontrinoValidita
     */
    public int getScontroScontrinoValidita() {
        return scontroScontrinoValidita;
    }

    /**
     * @param scontroScontrinoValidita the scontroScontrinoValidita to set
     */
    public void setScontroScontrinoValidita(int scontroScontrinoValidita) {
        this.scontroScontrinoValidita = scontroScontrinoValidita;
    }

    /**
     * @return the selfieCoin
     */
    public int getSelfieCoin() {
        return selfieCoin;
    }

    /**
     * @param selfieCoin the selfieCoin to set
     */
    public void setSelfieCoin(int selfieCoin) {
        this.selfieCoin = selfieCoin;
    }

    /**
     * @return the selfieCoinRiduzioneNoLink
     */
    public int getSelfieCoinRiduzioneNoLink() {
        return selfieCoinRiduzioneNoLink;
    }

    /**
     * @param selfieCoinRiduzioneNoLink the selfieCoinRiduzioneNoLink to set
     */
    public void setSelfieCoinRiduzioneNoLink(int selfieCoinRiduzioneNoLink) {
        this.selfieCoinRiduzioneNoLink = selfieCoinRiduzioneNoLink;
    }

    /**
     * @return the womValidita
     */
    public int getWomValidita() {
        return womValidita;
    }

    /**
     * @param womValidita the womValidita to set
     */
    public void setWomValidita(int womValidita) {
        this.womValidita = womValidita;
    }

    /**
     * @return the womSelfieCoinUtenteA
     */
    public int getWomSelfieCoinUtenteA() {
        return womSelfieCoinUtenteA;
    }

    /**
     * @param womSelfieCoinUtenteA the womSelfieCoinUtenteA to set
     */
    public void setWomSelfieCoinUtenteA(int womSelfieCoinUtenteA) {
        this.womSelfieCoinUtenteA = womSelfieCoinUtenteA;
    }

    /**
     * @return the womSelfieCoinUtenteB
     */
    public int getWomSelfieCoinUtenteB() {
        return womSelfieCoinUtenteB;
    }

    /**
     * @param womSelfieCoinUtenteB the womSelfieCoinUtenteB to set
     */
    public void setWomSelfieCoinUtenteB(int womSelfieCoinUtenteB) {
        this.womSelfieCoinUtenteB = womSelfieCoinUtenteB;
    }

    /**
     * @return the provvigioneSelfie
     */
    public double getProvvigioneSelfie() {
        return provvigioneSelfie;
    }

    /**
     * @param provvigioneSelfie the provvigioneSelfie to set
     */
    public void setProvvigioneSelfie(double provvigioneSelfie) {
        this.provvigioneSelfie = provvigioneSelfie;
    }

    /**
     * @return the provvigioneOfferta
     */
    public double getProvvigioneOfferta() {
        return provvigioneOfferta;
    }

    /**
     * @param provvigioneOfferta the provvigioneOfferta to set
     */
    public void setProvvigioneOfferta(double provvigioneOfferta) {
        this.provvigioneOfferta = provvigioneOfferta;
    }

    /**
     * @return the provvigioneWom
     */
    public double getProvvigioneWom() {
        return provvigioneWom;
    }

    /**
     * @param provvigioneWom the provvigioneWom to set
     */
    public void setProvvigioneWom(double provvigioneWom) {
        this.provvigioneWom = provvigioneWom;
    }

    /**
     * @return the provvigioneSelfiePromo
     */
    public double getProvvigioneSelfiePromo() {
        return provvigioneSelfiePromo;
    }

    /**
     * @param provvigioneSelfiePromo the provvigioneSelfiePromo to set
     */
    public void setProvvigioneSelfiePromo(double provvigioneSelfiePromo) {
        this.provvigioneSelfiePromo = provvigioneSelfiePromo;
    }

    /**
     * @return the provvigioneOffertaPromo
     */
    public double getProvvigioneOffertaPromo() {
        return provvigioneOffertaPromo;
    }

    /**
     * @param provvigioneOffertaPromo the provvigioneOffertaPromo to set
     */
    public void setProvvigioneOffertaPromo(double provvigioneOffertaPromo) {
        this.provvigioneOffertaPromo = provvigioneOffertaPromo;
    }

    /**
     * @return the numeroSelfiePromo
     */
    public int getNumeroSelfiePromo() {
        return numeroSelfiePromo;
    }

    /**
     * @param numeroSelfiePromo the numeroSelfiePromo to set
     */
    public void setNumeroSelfiePromo(int numeroSelfiePromo) {
        this.numeroSelfiePromo = numeroSelfiePromo;
    }

    /**
     * @return the numeroOffertePromo
     */
    public int getNumeroOffertePromo() {
        return numeroOffertePromo;
    }

    /**
     * @param numeroOffertePromo the numeroOffertePromo to set
     */
    public void setNumeroOffertePromo(int numeroOffertePromo) {
        this.numeroOffertePromo = numeroOffertePromo;
    }



}
