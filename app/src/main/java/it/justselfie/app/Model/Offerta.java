/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class Offerta implements Serializable{
    private int id;
    private String nome;
    private String descrizione;
    private String immagine;
    private double prezzoPieno;
    private double prezzoScontato;
    private double sconto;
    private Esercente esercente;


    public Offerta(){
        this.nome = "PIZZA E BIBITA PER 2";
        this.descrizione = "";
        this.immagine = "";

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getImmagine() {
        return immagine;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public double getPrezzoPieno() {
        return prezzoPieno;
    }

    public void setPrezzoPieno(double prezzoPieno) {
        this.prezzoPieno = prezzoPieno;
    }

    public double getPrezzoScontato() {
        return prezzoScontato;
    }

    public void setPrezzoScontato(double prezzoScontato) {
        this.prezzoScontato = prezzoScontato;
    }

    public double getPercentualeSconto() {
        return sconto;
    }

    public void setPercentualeSconto(double percentualeSconto) {
        this.sconto = percentualeSconto;
    }

    public Esercente getEsercente() {
        return esercente;
    }

    public void setEsercente(Esercente esercente) {
        this.esercente = esercente;
    }
}
