/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Esercente implements Serializable{
    
    
    private int id;
    private String nome;
    private String email;
    private String telefono;
    private String indirizzo;
    private double latitudine;
    private double longitudine;
    private int scontoMinimo;
    private Categoria categoria;
    private Contratto contratto;
    
    private String immagineProfilo;
    private String immagineCopertina;
    private String descrizione;
   
    private String idFacebook;
    private String idInstagram;

    private String nomeSocieta;
    private String indirizzoSocieta;
    private String partitaIvaSocieta;
    private String codiceFiscaleSocieta;
    
    private int statoBackend;
    private int statoApp;
    
    private String beaconUUID;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the indirizzo
     */
    public String getIndirizzo() {
        return indirizzo;
    }

    /**
     * @param indirizzo the indirizzo to set
     */
    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    /**
     * @return the latitudine
     */
    public double getLatitudine() {
        return latitudine;
    }

    /**
     * @param latitudine the latitudine to set
     */
    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    /**
     * @return the longitudine
     */
    public double getLongitudine() {
        return longitudine;
    }


    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the regole
     */
    public Contratto getContratto() {
        return contratto;
    }

    /**
     * @param contratto the regole to set
     */
    public void setContratto(Contratto contratto) {
        this.contratto = contratto;
    }

    /**
     * @return the idFacebook
     */
    public String getIdFacebook() {
        return idFacebook;
    }

    /**
     * @param idFacebook the idFacebook to set
     */
    public void setIdFacebook(String idFacebook) {
        this.idFacebook = idFacebook;
    }

    /**
     * @return the idInstagram
     */
    public String getIdInstagram() {
        return idInstagram;
    }

    /**
     * @param idInstagram the idInstagram to set
     */
    public void setIdInstagram(String idInstagram) {
        this.idInstagram = idInstagram;
    }



    /**
     * @return the nomeSocieta
     */
    public String getNomeSocieta() {
        return nomeSocieta;
    }

    /**
     * @param nomeSocieta the nomeSocieta to set
     */
    public void setNomeSocieta(String nomeSocieta) {
        this.nomeSocieta = nomeSocieta;
    }

    /**
     * @return the indirizzoSocieta
     */
    public String getIndirizzoSocieta() {
        return indirizzoSocieta;
    }

    /**
     * @param indirizzoSocieta the indirizzoSocieta to set
     */
    public void setIndirizzoSocieta(String indirizzoSocieta) {
        this.indirizzoSocieta = indirizzoSocieta;
    }

    /**
     * @return the partitaIvaSocieta
     */
    public String getPartitaIvaSocieta() {
        return partitaIvaSocieta;
    }

    /**
     * @param partitaIvaSocieta the partitaIvaSocieta to set
     */
    public void setPartitaIvaSocieta(String partitaIvaSocieta) {
        this.partitaIvaSocieta = partitaIvaSocieta;
    }

    /**
     * @return the codiceFiscaleSocieta
     */
    public String getCodiceFiscaleSocieta() {
        return codiceFiscaleSocieta;
    }

    /**
     * @param codiceFiscaleSocieta the codiceFiscaleSocieta to set
     */
    public void setCodiceFiscaleSocieta(String codiceFiscaleSocieta) {
        this.codiceFiscaleSocieta = codiceFiscaleSocieta;
    }

    
    /**
     * @return the immagineProfilo
     */
    public String getImmagineProfilo() {
        return immagineProfilo;
    }

    /**
     * @param immagineProfilo the immagineProfilo to set
     */
    public void setImmagineProfilo(String immagineProfilo) {
        this.immagineProfilo = immagineProfilo;
    }

    /**
     * @return the immagineCopertina
     */
    public String getImmagineCopertina() {
        return immagineCopertina;
    }

    /**
     * @param immagineCopertina the immagineCopertina to set
     */
    public void setImmagineCopertina(String immagineCopertina) {
        this.immagineCopertina = immagineCopertina;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @param descrizione the descrizione to set
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    /**
     * @return the statoBackend
     */
    public int getStatoBackend() {
        return statoBackend;
    }

    /**
     * @param statoBackend the statoBackend to set
     */
    public void setStatoBackend(int statoBackend) {
        this.statoBackend = statoBackend;
    }

    /**
     * @return the statoApp
     */
    public int getStatoApp() {
        return statoApp;
    }

    /**
     * @param statoApp the statoApp to set
     */
    public void setStatoApp(int statoApp) {
        this.statoApp = statoApp;
    }


    public String getBeaconUUID() {
        return beaconUUID;
    }

    public void setBeaconUUID(String beaconUUID) {
        this.beaconUUID = beaconUUID;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    public int getScontoMinimo() {
        return scontoMinimo;
    }

    public void setScontoMinimo(int scontoMinimo) {
        this.scontoMinimo = scontoMinimo;
    }
}
