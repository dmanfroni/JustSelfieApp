/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class ConvalidaGiftCard implements Serializable{
    private int idUtente;
    private String beaconUUID;
    private int valoreEuro;
    private int valoreSelfieCoin;

    public ConvalidaGiftCard(){

    }

    /**
     * @return the idUtente
     */
    public int getIdUtente() {
        return idUtente;
    }

    /**
     * @param idUtente the idUtente to set
     */
    public void setIdUtente(int idUtente) {
        this.idUtente = idUtente;
    }

    /**
     * @return the beaconUUID
     */
    public String getBeaconUUID() {
        return beaconUUID;
    }

    /**
     * @param beaconUUID the beaconUUID to set
     */
    public void setBeaconUUID(String beaconUUID) {
        this.beaconUUID = beaconUUID;
    }


    public int getValoreEuro() {
        return valoreEuro;
    }

    public void setValoreEuro(int valoreEuro) {
        this.valoreEuro = valoreEuro;
    }

    public int getValoreSelfieCoin() {
        return valoreSelfieCoin;
    }

    public void setValoreSelfieCoin(int valoreSelfieCoin) {
        this.valoreSelfieCoin = valoreSelfieCoin;
    }
}
