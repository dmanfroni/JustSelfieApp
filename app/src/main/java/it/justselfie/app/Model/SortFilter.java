package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Davide on 04/01/2017.
 */

public class SortFilter implements Serializable {

    public static final int ORDINAMENTO_DEFAULT = -1;
    public static final int ORDINAMENTO_DISTANZA = 0;
    public static final int ORDINAMENTO_PREZZO = 1;
    private int ordinamento;
    private boolean filtroPrezzoMinimo;
    private int prezzoMinimo;
    private boolean filtroPrezzoMassimo;
    private int prezzoMassimo;
    private String testo;
    private HashMap<Integer, Boolean> categorieSelezionate;

    public SortFilter(){
        this.ordinamento = ORDINAMENTO_DEFAULT;
        filtroPrezzoMinimo = false;
        filtroPrezzoMassimo = false;
        prezzoMassimo = 0;
        prezzoMassimo = 0;
        categorieSelezionate = new HashMap<>();
        testo = "";
    }

    public int getOrdinamento() {
        return ordinamento;
    }

    public void setOrdinamento(int ordinamento) {
        this.ordinamento = ordinamento;
    }

    public boolean isFiltroPrezzoMinimo() {
        return filtroPrezzoMinimo;
    }

    public void setFiltroPrezzoMinimo(boolean filtroPrezzoMinimo) {
        this.filtroPrezzoMinimo = filtroPrezzoMinimo;
    }

    public int getPrezzoMinimo() {
        return prezzoMinimo;
    }

    public void setPrezzoMinimo(int prezzoMinimo) {
        this.prezzoMinimo = prezzoMinimo;
    }

    public boolean isFiltroPrezzoMassimo() {
        return filtroPrezzoMassimo;
    }

    public void setFiltroPrezzoMassimo(boolean filtroPrezzoMassimo) {
        this.filtroPrezzoMassimo = filtroPrezzoMassimo;
    }

    public int getPrezzoMassimo() {
        return prezzoMassimo;
    }

    public void setPrezzoMassimo(int prezzoMassimo) {
        this.prezzoMassimo = prezzoMassimo;
    }

    public HashMap<Integer, Boolean> getCategorieSelezionate() {
        return categorieSelezionate;
    }

    public void setCategorieSelezionate(HashMap<Integer, Boolean> categorieSelezionate) {
        this.categorieSelezionate = categorieSelezionate;
    }

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }
}
