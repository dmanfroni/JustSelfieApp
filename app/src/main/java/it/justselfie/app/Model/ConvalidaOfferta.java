/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class ConvalidaOfferta implements Serializable{

    private int id;
    private Acquisto acquisto;
    private Selfie selfie;
    private String immagineBase64;
    private List<Utente> utentiTagOfferta;
    
    public ConvalidaOfferta(){
        
    }




    /**
     * @return the acquisto
     */
    public Acquisto getAcquisto() {
        return acquisto;
    }

    /**
     * @param acquisto the acquisto to set
     */
    public void setAcquisto(Acquisto acquisto) {
        this.acquisto = acquisto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Selfie getSelfie() {
        return selfie;
    }

    public void setSelfie(Selfie selfie) {
        this.selfie = selfie;
    }

    public String getImmagineBase64() {
        return immagineBase64;
    }

    public void setImmagineBase64(String immagineBase64) {
        this.immagineBase64 = immagineBase64;
    }

    public List<Utente> getUtentiTagOfferta() {
        return utentiTagOfferta;
    }

    public void setUtentiTagOfferta(List<Utente> utentiTagOfferta) {
        this.utentiTagOfferta = utentiTagOfferta;
    }
}
