package it.justselfie.app.Model;

import java.io.Serializable;

/**
 * Created by Davide on 29/10/2016.
 */

public class Ricerca implements Serializable{

    private String ricercaTesto;
    private int idCategoria;
    private int idEsercente;
    private int idUtente;

    private int idSelfie;
    public Ricerca(){
        this.ricercaTesto = "";
    }

    /**
     * @return the ricercaTesto
     */
    public String getRicercaTesto() {
        return ricercaTesto;
    }

    /**
     * @param ricercaTesto the ricercaTesto to set
     */
    public void setRicercaTesto(String ricercaTesto) {
        this.ricercaTesto = ricercaTesto;
    }

    /**
     * @return the idCategoria
     */
    public int getIdCategoria() {
        return idCategoria;
    }

    /**
     * @param idCategoria the idCategoria to set
     */
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }


    /**
     * @return the idUtente
     */
    public int getIdUtente() {
        return idUtente;
    }

    /**
     * @param idUtente the idUtente to set
     */
    public void setIdUtente(int idUtente) {
        this.idUtente = idUtente;
    }

    public int getIdEsercente() {
        return idEsercente;
    }

    public void setIdEsercente(int idEsercente) {
        this.idEsercente = idEsercente;
    }

    public int getIdSelfie() {
        return idSelfie;
    }

    public void setIdSelfie(int idSelfie) {
        this.idSelfie = idSelfie;
    }
}
