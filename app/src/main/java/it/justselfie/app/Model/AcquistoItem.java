/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class AcquistoItem implements Serializable{
    private Offerta offerta;
    private int quantita;
    
    public AcquistoItem(){
        this.offerta = new Offerta();
        this.quantita = 0;
    }

    /**
     * @return the offerta
     */
    public Offerta getOfferta() {
        return offerta;
    }

    /**
     * @param offerta the offerta to set
     */
    public void setOfferta(Offerta offerta) {
        this.offerta = offerta;
    }

    /**
     * @return the quantita
     */
    public int getQuantita() {
        return quantita;
    }

    /**
     * @param quantita the quantita to set
     */
    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }
}
