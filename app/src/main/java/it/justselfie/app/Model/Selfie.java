/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Selfie implements Serializable {

    private int id;
    private String selfie;
    private String testo;
    private int tipoCondivisione;
    private Utente utente;
    private List<Utente> amiciTaggatiFB;
    private Esercente esercente;
    private String registratoIl;

    public Selfie(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }





    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public List<Utente> getAmiciTaggatiFB() {
        return amiciTaggatiFB;
    }

    public void setAmiciTaggatiFB(List<Utente> amiciTaggatiFB) {
        this.amiciTaggatiFB = amiciTaggatiFB;
    }

    public Esercente getEsercente() {
        return esercente;
    }

    public void setEsercente(Esercente esercente) {
        this.esercente = esercente;
    }

    public int getTipoCondivisione() {
        return tipoCondivisione;
    }

    public void setTipoCondivisione(int tipoCondivisione) {
        this.tipoCondivisione = tipoCondivisione;
    }

    public String getRegistratoIl() {
        return registratoIl;
    }

    public void setRegistratoIl(String registratoIl) {
        this.registratoIl = registratoIl;
    }
}
