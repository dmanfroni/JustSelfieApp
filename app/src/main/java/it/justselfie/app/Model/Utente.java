/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Utente implements Serializable {

    private int id;
    private String nome;
    private String email;
    private String password;
    private String sesso;
    private String immagineProfilo;
    private long userIdFB;
    private long userIdINS;
    private int selfieCoin;
    private List<Utente> amiciFB;
    private List<Utente> amiciINS;
    private String telefono;
    private double affidabilita;
    
    public Utente(){
        this.id = 0;
        this.nome = "";
        this.email = "";
        this.sesso = "";
        this.password = "";
        this.immagineProfilo = "";
        this.userIdFB = 0;
        this.userIdINS = 0;
        this.selfieCoin = 0;
        this.amiciFB = new ArrayList<>();
        this.amiciINS = new ArrayList<>();
     
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the sesso
     */
    public String getSesso() {
        return sesso;
    }

    /**
     * @param sesso the gender to set
     */
    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    /**
     * @return the immagineProfilo
     */
    public String getImmagineProfilo() {
        return immagineProfilo;
    }

    /**
     * @param immagineProfilo the immagineProfilo to set
     */
    public void setImmagineProfilo(String immagineProfilo) {
        this.immagineProfilo = immagineProfilo;
    }

    /**
     * @return the userIdFB
     */
    public long getUserIdFB() {
        return userIdFB;
    }

    /**
     * @param userIdFB the userIdFB to set
     */
    public void setUserIdFB(long userIdFB) {
        this.userIdFB = userIdFB;
    }

    /**
     * @return the userIdINS
     */
    public long getUserIdINS() {
        return userIdINS;
    }

    /**
     * @param userIdINS the userIdINS to set
     */
    public void setUserIdINS(long userIdINS) {
        this.userIdINS = userIdINS;
    }

    /**
     * @return the selfieCoin
     */
    public int getSelfieCoin() {
        return selfieCoin;
    }

    /**
     * @param selfieCoin the selfieCoin to set
     */
    public void setSelfieCoin(int selfieCoin) {
        this.selfieCoin = selfieCoin;
    }

    /**
     * @return the amiciFB
     */
    public List<Utente> getAmiciFB() {
        return amiciFB;
    }

    /**
     * @param amiciFB the amiciFB to set
     */
    public void setAmiciFB(List<Utente> amiciFB) {
        this.amiciFB = amiciFB;
    }

    /**
     * @return the amiciINS
     */
    public List<Utente> getAmiciINS() {
        return amiciINS;
    }

    /**
     * @param amiciINS the amiciINS to set
     */
    public void setAmiciINS(List<Utente> amiciINS) {
        this.amiciINS = amiciINS;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the affidabilita
     */
    public double getAffidabilita() {
        return affidabilita;
    }

    /**
     * @param affidabilita the affidabilita to set
     */
    public void setAffidabilita(double affidabilita) {
        this.affidabilita = affidabilita;
    }

}
