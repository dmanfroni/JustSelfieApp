/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class Sconto implements Serializable{

    private int id;
    private int sconto;
    private String dataCreazione;
    private String dataScadenza;
    private String dataConvalida;
    private int convalidato;
    private Esercente esercente;
    private Utente utente;


    public Sconto(){

    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the sconto
     */
    public int getSconto() {
        return sconto;
    }

    /**
     * @param sconto the sconto to set
     */
    public void setSconto(int sconto) {
        this.sconto = sconto;
    }

    /**
     * @return the dataCreazione
     */
    public String getDataCreazione() {
        return dataCreazione;
    }

    /**
     * @param dataCreazione the dataCreazione to set
     */
    public void setDataCreazione(String dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    /**
     * @return the dataScadenza
     */
    public String getDataScadenza() {
        return dataScadenza;
    }

    /**
     * @param dataScadenza the dataScadenza to set
     */
    public void setDataScadenza(String dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    /**
     * @return the dataConvalida
     */
    public String getDataConvalida() {
        return dataConvalida;
    }

    /**
     * @param dataConvalida the dataConvalida to set
     */
    public void setDataConvalida(String dataConvalida) {
        this.dataConvalida = dataConvalida;
    }

    /**
     * @return the convalidato
     */
    public int getConvalidato() {
        return convalidato;
    }

    /**
     * @param convalidato the convalidato to set
     */
    public void setConvalidato(int convalidato) {
        this.convalidato = convalidato;
    }

    /**
     * @return the esercente
     */
    public Esercente getEsercente() {
        return esercente;
    }

    /**
     * @param esercente the esercente to set
     */
    public void setEsercente(Esercente esercente) {
        this.esercente = esercente;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }
}
