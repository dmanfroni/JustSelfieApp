/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;

/**
 *
 * @author Davide
 */
public class ConvalidaSelfie implements Serializable {

    private Selfie selfie;
    private String immagineBase64;
    private int id;

    public ConvalidaSelfie(){}


    public Selfie getSelfie() {
        return selfie;
    }

    public void setSelfie(Selfie selfie) {
        this.selfie = selfie;
    }

    public String getImmagineBase64() {
        return immagineBase64;
    }

    public void setImmagineBase64(String immagineBase64) {
        this.immagineBase64 = immagineBase64;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
