package it.justselfie.app.Model;

import java.io.Serializable;

/**
 * Created by Davide on 29/10/2016.
 */

public class MessaggioUtente implements Serializable {

    private int id;
    private String messaggio;
    private int livello;
    private String dataMessaggio;


    public MessaggioUtente(){

    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the messaggio
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * @param messaggio the messaggio to set
     */
    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    /**
     * @return the livello
     */
    public int getLivello() {
        return livello;
    }

    /**
     * @param livello the livello to set
     */
    public void setLivello(int livello) {
        this.livello = livello;
    }

    public String getDataMessaggio() {
        return dataMessaggio;
    }

    public void setDataMessaggio(String dataMessaggio) {
        this.dataMessaggio = dataMessaggio;
    }
}
