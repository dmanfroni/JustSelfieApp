package it.justselfie.app.Model;

import java.io.Serializable;

/**
 * Created by Davide on 13/11/2016.
 */

public class CondivisioneItem implements Serializable {

    private int tipo;
    private String messaggio;
    private int abilitato;

    public CondivisioneItem(){

    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getAbilitato() {
        return abilitato;
    }

    public void setAbilitato(int abilitato) {
        this.abilitato = abilitato;
    }
}
