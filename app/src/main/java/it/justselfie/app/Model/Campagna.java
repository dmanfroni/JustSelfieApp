/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.justselfie.app.Model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Davide
 */
public class Campagna implements Serializable{
    private int id;
    private String nome;
    private List<Offerta> offerte;
    private int sconto;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the offerte
     */
    public List<Offerta> getOfferte() {
        return offerte;
    }

    /**
     * @param offerte the offerte to set
     */
    public void setOfferte(List<Offerta> offerte) {
        this.offerte = offerte;
    }

    /**
     * @return the sconto
     */
    public int getSconto() {
        return sconto;
    }

    /**
     * @param sconto the sconto to set
     */
    public void setSconto(int sconto) {
        this.sconto = sconto;
    }
    
}
