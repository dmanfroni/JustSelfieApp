package it.justselfie.app.Activity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Database.DatabaseHelper;
import it.justselfie.app.Database.QueryBuilder;
import it.justselfie.app.Fragments.ErroreFragment;
import it.justselfie.app.Model.Acquisto;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.CondivisioneItem;
import it.justselfie.app.Model.ConvalidaOfferta;
import it.justselfie.app.Model.ConvalidaSconto;
import it.justselfie.app.Model.ConvalidaSelfie;
import it.justselfie.app.Model.Sconto;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.CustomViews.chipsedittext.ChipsAdapter;
import it.justselfie.app.CustomViews.chipsedittext.ChipsItem;
import it.justselfie.app.CustomViews.chipsedittext.ChipsMultiAutoCompleteTextview;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 09/11/2016.
 */

public class CondivisioneActivity extends AnalyticsActivity {

    private Acquisto acquisto;
    private Sconto sconto;
    private Selfie selfie;
    private String percorsoImmagine;

    public static final int CONDIZIONE_ACQUISTO = 1;
    public static final int CONDIZIONE_SCONTO = 2;
    public static final int CONDIZIONE_SELFIE = 3;

    private int condizioneCondivisione;

    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;

    @InjectView(R.id.activity_condivisione_claim_tag_offerta)
    TextView claimTagOfferta;

    @InjectView(R.id.activity_condivisione_claim_tag_selfie_coin)
    TextView claimTagSelfieCoin;

    @InjectView(R.id.activity_condivisione_icon_tag_offerta)
    ImageView iconTagOfferta;

    @InjectView(R.id.activity_condivisione_divisore_tag_offerta)
    View divisoreTagOfferta;

    @InjectView(R.id.activity_condivisione_editext_tag_offerta)
    ChipsMultiAutoCompleteTextview chipsTagOfferta;

    @InjectView(R.id.activity_condivisione_editext_tag_selfiecoin)
    ChipsMultiAutoCompleteTextview chipsTagSelfieCoin;


    @InjectView(R.id.activity_condivisione_foto)
    ImageView foto;

    @InjectView(R.id.activity_condivisione_localizzazione)
    TextView localizzazione;

    @InjectView(R.id.activity_condivisione_descrizione)
    TextView descrizione;

    @InjectView(R.id.activity_condivisione_claim_information)
    TextView info;

    @InjectView(R.id.activity_condivisione_spinner)
    Spinner condivisioni;

    @InjectView(R.id.activity_condivisione_convalida_dopo)
    Button convalidaDopo;

    @InjectView(R.id.activity_condivisione_convalida_subito)
    Button convalidaSubito;

    @InjectView(R.id.activity_condivisione_baloon)
    View baloon;


    private List<CondivisioneItem> elencoCondivisioni;

    private List<Utente> utentiTaggatiOfferta;
    private List<Utente> utentiTaggatiSelfieCoin;


    private ProgressDialog progressDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return true;

    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Condivisione");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }


    private View.OnClickListener convalidaAcquistoListener = view -> {

        if (condivisioni.getSelectedItemPosition() == 2) {
            new AlertDialog.Builder(CondivisioneActivity.this).setMessage("Funzione non ancora disponibile!").setTitle("Attenzione")
                    .setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss()).create().show();
            return;
        }
        final int pendingConvalidaOra = view.getId() == R.id.activity_condivisione_convalida_subito ? 1 : 0;

        fillUtentiTaggatiOfferta();
        fillUtentiTaggatiSelfieCoin();
        if (checkTagOfferteLatoClient()) {
            progressDialog = new ProgressDialog(CondivisioneActivity.this);
            progressDialog.setMessage("Verifico la disponibilita di offerte per gli utenti taggati");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... voids) {
                    HashMap<String, Object> input = new HashMap<String, Object>();
                    input.put("idCampagna", acquisto.getCampagna().getId());
                    input.put("utentiTaggati", utentiTaggatiOfferta);
                    String s = new Gson().toJson(input);
                    return NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_convalida_checktag, s);
                }

                @Override
                protected void onPostExecute(String result) {
                    try {
                        progressDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getBoolean("success")) {
                            List<Utente> amiciRigettati = new Gson().fromJson(jsonObject.getJSONObject("response").getJSONArray("amiciRifiutati").toString(), new TypeToken<List<Utente>>() {
                            }.getType());
                            if (amiciRigettati.size() > 0) {
                                String s = "I seguenti utenti non possono acquistare un offerta di benvenuto perche gia consumate precedentemente:";
                                for (Utente utente : amiciRigettati) {
                                    s += "\n- " + utente.getNome();
                                }
                                ErroreFragment erroreFragment = ErroreFragment.newInstance(s);
                                erroreFragment.show(getSupportFragmentManager(), "errore");
                            } else {

                                fillSelfie();
                                ConvalidaOfferta co = salvaAcquisto();
                                Intent intent = null;
                                if (pendingConvalidaOra == 1) {
                                    intent = new Intent(CondivisioneActivity.this, ConvalidaActivity.class);
                                    intent.putExtra("convalidaOfferta", co);
                                } else {
                                    intent = new Intent(CondivisioneActivity.this, JustSelfieActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("view", 1);
                                }
                                startActivity(intent);
                            }
                        } else {
                            new Exception(jsonObject.getString("response"));
                        }
                    } catch (Exception ex) {
                        Toast.makeText(CondivisioneActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }.execute();
        }
    };

    private View.OnClickListener convalidaScontoListener = view -> {
        if (condivisioni.getSelectedItemPosition() == 2) {
            new AlertDialog.Builder(CondivisioneActivity.this).setMessage("Funzione non ancora disponibile!").setTitle("Attenzione")
                    .setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss()).create().show();
            return;
        }
        final int pendingConvalidaOra = view.getId() == R.id.activity_condivisione_convalida_subito ? 1 : 0;
        fillUtentiTaggatiSelfieCoin();
        if (!controllaDoppioniSelfieCoin()) {
            fillSelfie();
            ConvalidaSconto co = salvaSconto();

            Intent intent = null;
            if (pendingConvalidaOra == 1) {
                intent = new Intent(CondivisioneActivity.this, ConvalidaActivity.class);
                intent.putExtra("convalidaSconto", co);
            } else {
                intent = new Intent(CondivisioneActivity.this, JustSelfieActivity.class);
            }
            startActivity(intent);


        } else {
            ErroreFragment erroreFragment = ErroreFragment.newInstance("Non puoi inserire due volte lo stesso utente :( ");
            erroreFragment.setCancelable(true);
            erroreFragment.show(getSupportFragmentManager(), "errore");
        }
    };

    private View.OnClickListener convalidaSelfieListener = view -> {
        if (condivisioni.getSelectedItemPosition() == 2) {
            new AlertDialog.Builder(CondivisioneActivity.this).setMessage("Funzione non ancora disponibile!").setTitle("Attenzione")
                    .setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss()).create().show();
            return;
        }
        final int pendingConvalidaOra = view.getId() == R.id.activity_condivisione_convalida_subito ? 1 : 0;

        fillUtentiTaggatiSelfieCoin();
        if (!controllaDoppioniSelfieCoin()) {

            fillSelfie();
            ConvalidaSelfie co = salvaSelfie();
            Intent intent = null;
            if (pendingConvalidaOra == 1) {
                intent = new Intent(CondivisioneActivity.this, ConvalidaActivity.class);
                intent.putExtra("convalidaSelfie", co);
            } else {
                intent = new Intent(CondivisioneActivity.this, JustSelfieActivity.class);
            }
            startActivity(intent);
        } else {
            ErroreFragment erroreFragment = ErroreFragment.newInstance("Non puoi inserire due volte lo stesso utente :( ");
            erroreFragment.setCancelable(true);
            erroreFragment.show(getSupportFragmentManager(), "errore");
        }
    };

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_condivisione);
        ButterKnife.inject(this);
        setupToolbar();
        percorsoImmagine = getIntent().getExtras().getString("foto");
        selfie = new Selfie();
        utentiTaggatiOfferta = new ArrayList<>();
        utentiTaggatiSelfieCoin = new ArrayList<>();
        if (PreferenceManager.getShowBaloonCondivisione(this)) {
            baloon.setVisibility(View.VISIBLE);
            baloon.setOnClickListener(view -> {
                baloon.setVisibility(View.GONE);
                PreferenceManager.setShowBaloonCondivisione(false, CondivisioneActivity.this);
            });
        }

        initElements();
        selfie.setUtente(PreferenceManager.getUtente(this));
        if (selfie.getUtente().getUserIdFB() == 0) {
            ErroreFragment erroreFragment = ErroreFragment.newInstance("Devi effettuare prima l'accesso su Facebook per poter proseguire");
            erroreFragment.show(getSupportFragmentManager(), "errore");
        } else {
            setAdapterAmici();
            Picasso.with(this).load(new File(percorsoImmagine)).into(foto);
            localizzazione.setText(selfie.getEsercente().getNome());
            condivisioni.setAdapter(new CondivisioniAdapter(this, elencoCondivisioni));
            condivisioni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 2) {
                        new AlertDialog.Builder(CondivisioneActivity.this)
                                .setMessage("Funzione non ancora disponibile!")
                                .setTitle("Attenzione")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                        condivisioni.performClick();
                        return;
                    }
                    info.setText(i == 0 ? R.string.message_share_justselfie : R.string.message_share_facebook);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            switch (condizioneCondivisione) {
                case CONDIZIONE_ACQUISTO:
                    convalidaDopo.setOnClickListener(convalidaAcquistoListener);
                    convalidaSubito.setOnClickListener(convalidaAcquistoListener);
                    break;
                case CONDIZIONE_SCONTO:
                    convalidaDopo.setOnClickListener(convalidaScontoListener);
                    convalidaSubito.setOnClickListener(convalidaScontoListener);
                    break;
                case CONDIZIONE_SELFIE:
                    convalidaDopo.setOnClickListener(convalidaSelfieListener);
                    convalidaSubito.setOnClickListener(convalidaSelfieListener);
                    break;
            }
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setAdapterAmici() {
        ArrayList<ChipsItem> arrCountry = new ArrayList<>();
        for (Utente amico : selfie.getUtente().getAmiciFB()) {
            arrCountry.add(new ChipsItem(amico.getNome(), "https://graph.facebook.com/" + amico.getUserIdFB() + "/picture?type=large"));
        }

        chipsTagOfferta.setAdapter(new ChipsAdapter(this, arrCountry));
        chipsTagOfferta.setThreshold(0);
        chipsTagSelfieCoin.setAdapter(new ChipsAdapter(this, arrCountry));
        chipsTagSelfieCoin.setThreshold(0);
    }


    private void initElements() {
        if (getIntent().getExtras().containsKey("oggetto")) {
            Serializable ogg = getIntent().getExtras().getSerializable("oggetto");
            if (ogg instanceof Acquisto) {
                acquisto = (Acquisto) ogg;
                selfie.setEsercente(acquisto.getEsercente());
                condizioneCondivisione = CONDIZIONE_ACQUISTO;
                loadCondivisioniSconto();
            } else if (ogg instanceof Sconto) {
                sconto = (Sconto) ogg;
                selfie.setEsercente(sconto.getEsercente());
                condizioneCondivisione = CONDIZIONE_SCONTO;
                loadCondivisioniSelfieCoin();
                divisoreTagOfferta.setVisibility(View.GONE);
                chipsTagOfferta.setVisibility(View.GONE);
                claimTagOfferta.setVisibility(View.GONE);
                iconTagOfferta.setVisibility(View.GONE);
                claimTagSelfieCoin.setText("Tagga gli amici che sono con te");
            } else if (ogg instanceof Selfie) {
                selfie = (Selfie) ogg;
                condizioneCondivisione = CONDIZIONE_SELFIE;
                loadCondivisioniSelfieCoin();
                divisoreTagOfferta.setVisibility(View.GONE);
                chipsTagOfferta.setVisibility(View.GONE);
                claimTagOfferta.setVisibility(View.GONE);
                iconTagOfferta.setVisibility(View.GONE);
            }
        }
    }

    private void fillUtentiTaggatiOfferta() {
        utentiTaggatiOfferta.clear();
        if (!chipsTagOfferta.getText().toString().isEmpty()) {
            String utentiTagText = chipsTagOfferta.getText().toString().trim();
            String[] utentiTaggatiArrat = utentiTagText.split(",");
            for (String nome : utentiTaggatiArrat) {
                for (Utente amico : selfie.getUtente().getAmiciFB()) {
                    if (nome.trim().equalsIgnoreCase(amico.getNome()))
                        utentiTaggatiOfferta.add(amico);
                }
            }
        }
    }

    private void fillUtentiTaggatiSelfieCoin() {
        utentiTaggatiSelfieCoin.clear();
        if (!chipsTagSelfieCoin.getText().toString().isEmpty()) {
            String utentiTagText = chipsTagSelfieCoin.getText().toString().trim();
            String[] utentiTaggatiArrat = utentiTagText.split(",");
            for (String nome : utentiTaggatiArrat) {
                for (Utente amico : selfie.getUtente().getAmiciFB()) {
                    if (nome.trim().equalsIgnoreCase(amico.getNome()))
                        utentiTaggatiSelfieCoin.add(amico);
                }
            }
        }
    }

    private boolean checkTagOfferteLatoClient() {
        int quantitaTot = 0;
        for (AcquistoItem acquistoItem : acquisto.getOfferteAcquistate())
            quantitaTot = quantitaTot + acquistoItem.getQuantita();


        if ((quantitaTot - 1) != utentiTaggatiOfferta.size()) {
            ErroreFragment erroreFragment = ErroreFragment.newInstance("Il numero di amici che usano le offerte deve essere pari al numero delle offerte acquistate.");
            erroreFragment.setCancelable(true);
            erroreFragment.show(getSupportFragmentManager(), "errore");
            return false;
        }
        boolean duplicato = false;
        for (Utente tagOff : utentiTaggatiOfferta) {
            for (Utente tagSC : utentiTaggatiSelfieCoin) {
                if (tagSC.getNome().equalsIgnoreCase(tagOff.getNome())) {
                    duplicato = true;
                    break;
                }
            }
            if (duplicato)
                break;
        }

        if (!duplicato) {
            duplicato = controllaDoppioniOfferte() || controllaDoppioniSelfieCoin();
        }
        if (duplicato) {
            ErroreFragment erroreFragment = ErroreFragment.newInstance("Non puoi inserire due volte lo stesso utente :( ");
            erroreFragment.setCancelable(true);
            erroreFragment.show(getSupportFragmentManager(), "errore");
            return false;
        }
        return true;
    }

    public boolean controllaDoppioniSelfieCoin() {
        boolean duplicato = false;
        for (int i = 0; i < utentiTaggatiSelfieCoin.size(); i++) {
            for (int k = i + 1; k < utentiTaggatiSelfieCoin.size(); k++) {
                if (utentiTaggatiSelfieCoin.get(i).getNome().equalsIgnoreCase(utentiTaggatiSelfieCoin.get(k).getNome())) {
                    duplicato = true;
                    break;
                }
            }
            if (duplicato)
                break;
        }
        return duplicato;
    }

    public boolean controllaDoppioniOfferte() {
        boolean duplicato = false;
        for (int i = 0; i < utentiTaggatiOfferta.size(); i++) {
            for (int k = i + 1; k < utentiTaggatiOfferta.size(); k++) {
                if (utentiTaggatiOfferta.get(i).getNome().equalsIgnoreCase(utentiTaggatiOfferta.get(k).getNome())) {
                    duplicato = true;
                    break;
                }
            }
            if (duplicato)
                break;
        }
        return duplicato;
    }

    private void fillSelfie() {
        selfie.setAmiciTaggatiFB(utentiTaggatiSelfieCoin);
        selfie.setTesto(descrizione.getText().toString());
        selfie.setTipoCondivisione(condivisioni.getSelectedItemPosition() + 1);
        selfie.setSelfie(percorsoImmagine);
    }

    private ConvalidaOfferta salvaAcquisto() {
        boolean found = false;
        ConvalidaOfferta convalidaOfferta = new ConvalidaOfferta();
        convalidaOfferta.setAcquisto(acquisto);
        convalidaOfferta.setSelfie(selfie);
        convalidaOfferta.setUtentiTagOfferta(utentiTaggatiOfferta);
        String query = "SELECT * FROM " + QueryBuilder.tabellaAcquisti + " WHERE 1=1";
        DatabaseHelper db = new DatabaseHelper(this);
        Cursor c = db.rawQuery(query);
        while (c.moveToNext()) {
            String json = c.getString(c.getColumnIndex("json"));
            ConvalidaOfferta convSaved = new Gson().fromJson(json, ConvalidaOfferta.class);
            if (convSaved.getAcquisto().getCampagna().getId() == acquisto.getCampagna().getId())
                found = true;
        }
        if (!found) {
            String json = new Gson().toJson(convalidaOfferta);
            ContentValues contentValues = new ContentValues();
            contentValues.put("json", json);
            long id = db.insert(QueryBuilder.tabellaAcquisti, contentValues);

            convalidaOfferta.setId((int) id);
        }
        db.close();


        return convalidaOfferta;

    }

    private ConvalidaSconto salvaSconto() {
        ConvalidaSconto convalidaSelfie = new ConvalidaSconto();
        convalidaSelfie.setSelfie(selfie);
        convalidaSelfie.setSconto(sconto);
        String json = new Gson().toJson(convalidaSelfie);
        ContentValues contentValues = new ContentValues();
        contentValues.put("json", json);
        DatabaseHelper db = new DatabaseHelper(this);
        db.insert(QueryBuilder.tabellaSconti, contentValues);
        db.close();
        return convalidaSelfie;

    }

    private ConvalidaSelfie salvaSelfie() {
        ConvalidaSelfie convalidaSelfie = new ConvalidaSelfie();
        convalidaSelfie.setSelfie(selfie);
        String json = new Gson().toJson(convalidaSelfie);
        ContentValues contentValues = new ContentValues();
        contentValues.put("json", json);
        DatabaseHelper db = new DatabaseHelper(this);
        db.insert(QueryBuilder.tabellaSelfie, contentValues);
        db.close();
        return convalidaSelfie;

    }


    private void loadCondivisioniSconto() {
        elencoCondivisioni = new ArrayList<>();

        double sc = acquisto.getCampagna().getSconto();
        double scRid = (sc / 100) * (100 - acquisto.getEsercente().getContratto().getScontoScontrinoRiduzioneNoLink());

        CondivisioneItem condivisioneJS = new CondivisioneItem();
        condivisioneJS.setTipo(1);
        condivisioneJS.setMessaggio("Gudagna " + (int) sc + " % di sconto per il tuo prossimo acquisto");

        CondivisioneItem condivisioneFB = new CondivisioneItem();
        condivisioneFB.setTipo(2);
        condivisioneFB.setMessaggio("Gudagna " + (int) scRid + " % di sconto per il tuo prossimo acquisto");

        /*CondivisioneItem condivisioneINS = new CondivisioneItem();
        condivisioneINS.setTipo(3);
        condivisioneINS.setMessaggio("Gudagna " + scRid + " % di sconto per il tuo prossimo acquisto");
*/

        if (acquisto.getEsercente().getIdFacebook() != null && !acquisto.getEsercente().getIdFacebook().isEmpty()) {
            condivisioneFB.setAbilitato(1);
            condivisioneJS.setAbilitato(1);
        } else {
            condivisioneFB.setAbilitato(0);
            condivisioneJS.setAbilitato(0);
        }
/*
        if(acquisto.getEsercente().getIdInstagram() != null && !acquisto.getEsercente().getIdInstagram().isEmpty()) {
            condivisioneINS.setAbilitato(1);
        }else{
            condivisioneINS.setAbilitato(0);
        }
*/
        CondivisioneItem condivisioneINS = new CondivisioneItem();
        condivisioneINS.setTipo(3);
        condivisioneINS.setMessaggio("In arrivo");


        elencoCondivisioni.add(condivisioneJS);
        elencoCondivisioni.add(condivisioneFB);
        elencoCondivisioni.add(condivisioneINS);


    }

    private void loadCondivisioniSelfieCoin() {
        elencoCondivisioni = new ArrayList<>();

        double sc = selfie.getEsercente().getContratto().getSelfieCoin();
        double scRid = (sc / 100) * (100 - selfie.getEsercente().getContratto().getSelfieCoinRiduzioneNoLink());

        CondivisioneItem condivisioneJS = new CondivisioneItem();
        condivisioneJS.setTipo(1);
        condivisioneJS.setMessaggio("Gudagna " + (int) sc + " selfie coin ");

        CondivisioneItem condivisioneFB = new CondivisioneItem();
        condivisioneFB.setTipo(2);
        condivisioneFB.setMessaggio("Gudagna " + (int) scRid + " selfie coin");


        if (selfie.getEsercente().getIdFacebook() != null && !selfie.getEsercente().getIdFacebook().isEmpty()) {
            condivisioneFB.setAbilitato(1);
            condivisioneJS.setAbilitato(1);
        } else {
            condivisioneFB.setAbilitato(0);
            condivisioneJS.setAbilitato(0);
        }

        CondivisioneItem condivisioneINS = new CondivisioneItem();
        condivisioneINS.setTipo(3);
        condivisioneINS.setMessaggio("In arrivo");


        elencoCondivisioni.add(condivisioneJS);
        elencoCondivisioni.add(condivisioneFB);
        elencoCondivisioni.add(condivisioneINS);


    }

    public class CondivisioniAdapter extends BaseAdapter {

        private LayoutInflater inflter;
        private Context context;
        private List<CondivisioneItem> elencoCondivisioni;

        public CondivisioniAdapter(Context context, List<CondivisioneItem> elencoCondivisioni) {
            this.context = context;
            this.elencoCondivisioni = elencoCondivisioni;
            this.inflter = getLayoutInflater();
        }

        public int getCount() {
            return elencoCondivisioni.size();
        }

        public CondivisioneItem getItem(int position) {
            return elencoCondivisioni.get(position);
        }

        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = inflter.inflate(R.layout.item_condivisione, null);
            ImageView icon = (ImageView) view.findViewById(R.id.item_condivisione_icona);
            TextView names = (TextView) view.findViewById(R.id.item_condivisione_test);
            CondivisioneItem condivisioneItem = getItem(position);
            switch (condivisioneItem.getTipo()) {
                case 1:
                    icon.setImageResource(R.mipmap.floating_action_button_scheda_esercente);
                    break;
                case 2:
                    icon.setImageResource(R.mipmap.facebook);
                    break;
                case 3:
                    icon.setImageResource(R.mipmap.instagram);
                    break;
            }

            names.setText(condivisioneItem.getMessaggio());
            return view;
        }

        @Override
        public View getDropDownView(int position, View view, ViewGroup parent) {
            view = inflter.inflate(R.layout.item_condivisione, null);
            ImageView icon = (ImageView) view.findViewById(R.id.item_condivisione_icona);
            TextView names = (TextView) view.findViewById(R.id.item_condivisione_test);
            CondivisioneItem condivisioneItem = getItem(position);
            switch (condivisioneItem.getTipo()) {
                case 1:
                    icon.setImageResource(R.mipmap.floating_action_button_scheda_esercente);
                    break;
                case 2:
                    icon.setImageResource(R.mipmap.facebook);
                    break;
                case 3:
                    icon.setImageResource(R.mipmap.instagram);
                    break;
            }

            names.setText(condivisioneItem.getMessaggio());
            return view;
        }
    }
}
