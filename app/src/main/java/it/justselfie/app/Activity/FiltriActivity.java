package it.justselfie.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uxcam.UXCam;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Adapter.AdapterCategoriaSelezionabili;
import it.justselfie.app.Model.Categoria;
import it.justselfie.app.Model.SortFilter;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 04/01/2017.
 */

public class FiltriActivity extends AnalyticsActivity {

    private String[] ordinamenti = new String[]{"Casuale", "Per distanza", "Per prezzo"};

    @InjectView(R.id.activity_filtri_ordinamenti)
    Spinner spinnerOrdinamenti;

    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;

    @InjectView(R.id.activity_filtri_prezzo_max)
    SeekBar seekBarPrezzoMax;

    @InjectView(R.id.activity_filtri_prezzo_min)
    SeekBar seekBarPrezzoMin;

    @InjectView(R.id.activity_filtri_prezzo_max_text)
    TextView textViewPrezzoMax;

    @InjectView(R.id.activity_filtri_prezzo_min_text)
    TextView textViewPrezzoMin;

    @InjectView(R.id.activity_filtri_categorie)
    RecyclerView recyclerViewCategorie;

    private SortFilter sortFilter;



    private List<Categoria> categorie;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Filtri");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case android.R.id.home:
                applicaFiltri();
                break;
        }
        return true;

    }

    private void applicaFiltri(){
        int tipoOrdinamento =   spinnerOrdinamenti.getSelectedItemPosition()-1;
        int prezzoMinimo = seekBarPrezzoMin.getProgress();
        int prezzoMassimo = seekBarPrezzoMax.getProgress();
        HashMap<Integer, Boolean> categorieSelezionate = ((AdapterCategoriaSelezionabili)recyclerViewCategorie.getAdapter()).getCategorieSelezionate();
        boolean filtroPrezzoMassimoAbilitao = prezzoMassimo != 0;
        boolean filtroPrezzoMinimoAbilitato = prezzoMinimo != 0;
        sortFilter.setOrdinamento(tipoOrdinamento);
        sortFilter.setCategorieSelezionate(categorieSelezionate);
        sortFilter.setFiltroPrezzoMassimo(filtroPrezzoMassimoAbilitao);
        sortFilter.setFiltroPrezzoMinimo(filtroPrezzoMinimoAbilitato);
        sortFilter.setPrezzoMassimo(prezzoMassimo);
        sortFilter.setPrezzoMinimo(prezzoMinimo);
        Intent data = new Intent();
        Bundle b = new Bundle();
        b.putSerializable("filtri", sortFilter);
        data.putExtras(b);
        if (getParent() == null) {
            setResult(AppCompatActivity.RESULT_OK, data);
        } else {
            getParent().setResult(AppCompatActivity.RESULT_OK, data);
        }
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtri);
        ButterKnife.inject(this);
        setupToolbar();
        spinnerOrdinamenti.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, ordinamenti));
        setupFiltri();
        new CategorieDownloader().execute();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setupFiltri() {
        sortFilter = (SortFilter) getIntent().getExtras().getSerializable("filtri");
        seekBarPrezzoMax.setMax(1000);
        seekBarPrezzoMax.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i == 1000) {
                    textViewPrezzoMax.setText("+" + i + "€");
                    textViewPrezzoMax.setVisibility(View.VISIBLE);
                }
                else if(i == 0)
                    textViewPrezzoMax.setText("");
                else {
                    textViewPrezzoMax.setText(i + "€");

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarPrezzoMin.setMax(1000);
        seekBarPrezzoMin.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(i == 0)
                    textViewPrezzoMin.setText("");
                else {
                    textViewPrezzoMin.setText(i + "€");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarPrezzoMin.setProgress(sortFilter.getPrezzoMinimo());
        seekBarPrezzoMax.setProgress(sortFilter.getPrezzoMassimo());
        spinnerOrdinamenti.setSelection(sortFilter.getOrdinamento()+1);
        recyclerViewCategorie.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewCategorie.setAdapter(new AdapterCategoriaSelezionabili(this, new ArrayList<>(), sortFilter.getCategorieSelezionate()));
    }

    void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    void showView(JSONObject object) {
        try {
            JSONArray array = object.getJSONArray("elencoCategorie");
            categorie = new Gson().fromJson(array.toString(), new TypeToken<List<Categoria>>(){}.getType());
            ((AdapterCategoriaSelezionabili)recyclerViewCategorie.getAdapter()).setElencoCategorie(categorie);
        } catch (Exception ex) {
            showError(ex.getMessage());
        }
    }


    private class CategorieDownloader extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {
            try {
                return new JSONObject(NetworkManager.doApiPOSTRequest(FiltriActivity.this, NetworkManager.endpoint_esercente_categorie, "{}"));
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            try {
                if (object.getBoolean("success")) {
                    showView(object.getJSONObject("response"));
                } else {
                    showError(object.getString("response"));
                }
            } catch (Exception ex) {
                showError(ex.getMessage());
            }
        }
    }
}
