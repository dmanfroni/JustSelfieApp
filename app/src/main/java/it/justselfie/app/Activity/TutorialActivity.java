package it.justselfie.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.analytics.HitBuilders;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.CustomViews.viewpagerindicator.CirclePageIndicator;
import it.justselfie.app.R;

/**
 * Created by Davide on 11/04/2016.
 */
public class TutorialActivity extends AnalyticsActivity {

    @InjectView(R.id.activity_tutorial_viewpager)
    ViewPager viewPager;

    @InjectView(R.id.activity_tutorial_circlepageindicator)
    CirclePageIndicator circlePageIndicator;

    private int fromLeftMenu = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.inject(this);
        viewPager.setAdapter(new TutorialPageAdapter(getSupportFragmentManager()));
        circlePageIndicator.setViewPager(viewPager);
        if(getIntent().getExtras() != null && !getIntent().getExtras().containsKey("nuovoUtente")){
            fromLeftMenu = 1;
        }
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Tutorial");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }




    private class TutorialPageAdapter extends FragmentStatePagerAdapter {
        public TutorialPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 6 ;
        }

        @Override
        public Fragment getItem(int position) {
            int resource = 0;
            switch (position) {
                case 0:
                    resource = R.mipmap.tut01;
                    break;
                case 1:
                    resource = R.mipmap.tut02;
                    break;
                case 2:
                    resource = R.mipmap.tut03;
                    break;
                case 3:
                    resource = R.mipmap.tut04;
                    break;
                case 4:
                    resource = R.mipmap.tut05;
                    break;
                case 5:
                    resource = R.mipmap.tut06;
                    break;

            }
            return TutorialFragmentItem.newInstance(resource);

        }
    }


    public static class TutorialFragmentItem extends Fragment {

        public static TutorialFragmentItem newInstance(int res) {
            TutorialFragmentItem fr = new TutorialFragmentItem();
            Bundle b = new Bundle();
            b.putInt("resId", res);
            fr.setArguments(b);
            return fr;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
            super.onCreateView(inflater, root, savedInstanceState);
            View v = inflater.inflate(R.layout.item_tutorial, null);
            ImageView imageView = (ImageView) v.findViewById(R.id.item_tutorial_image);
            imageView.setImageResource(getArguments().getInt("resId"));
            if(getArguments().getInt("resId") == R.mipmap.tut06 && ((TutorialActivity) getActivity()).fromLeftMenu == 0){
                v.findViewById(R.id.item_tutorial_button).setVisibility(View.VISIBLE);
                v.findViewById(R.id.item_tutorial_button).setOnClickListener(view -> {
                    Intent intent = new Intent(getActivity(), CodicePromoActivity.class);
                    if(getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("nuovoUtente")){
                        intent.putExtra("nuovoUtente", 1);
                    }
                    getActivity().startActivity(intent);
                    getActivity().finish();
                });
            }else{
                v.findViewById(R.id.item_tutorial_button).setVisibility(View.GONE);
            }
            return v;
        }
    }

    @Override
    public void onBackPressed(){
        if(fromLeftMenu == 1)
            super.onBackPressed();
    }


}
