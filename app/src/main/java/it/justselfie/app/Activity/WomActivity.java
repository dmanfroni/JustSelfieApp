package it.justselfie.app.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.uxcam.UXCam;

import org.json.JSONObject;

import java.util.HashMap;

import bolts.AppLinks;
import it.justselfie.app.App;
import it.justselfie.app.AppConstants;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.Tools.Tools;

/**
 * Created by Davide on 30/11/2016.
 */

public class WomActivity extends AppCompatActivity {



    private HashMap<String, String> tagProperties;


    private void startGoogleAnalyticsFromDeepLink(String deeplink){


        String[] params = deeplink.replace("justselfie://", "").split("/");
        Tracker t = ((App)getApplication()).getDefaultTracker();

        t.setScreenName("Deeplink");
        tagProperties.put("fonte", "deeplink");
        if(params.length >= 2){
            String comando = params[0];
            String id = params[1];
            if(params.length >= 3){
                String utm = params[2];
                utm = "http://justselfie.it/" + utm;
                if(PreferenceManager.getUtente(this) != null){
                    t.set("&uid", PreferenceManager.getUtente(this).getId() + "");
                }
                t.send(new HitBuilders.ScreenViewBuilder().setCampaignParamsFromUrl(utm).setNewSession().build());
                t.send(new HitBuilders.EventBuilder().setCategory("User actions").setAction("Open Deeplink").build());
            }
            tagProperties.put("contenuto", comando);
            startAppWithParams(Tools.decodeCommand(comando), Integer.parseInt(id));
        }else{
            tagProperties.put("contenuto", "home");
            startAppWithoutParams();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startTrackingUXCAM();
        tagProperties = new HashMap<>();
        try {
            if (getIntent().getData() != null) {
                String s = getIntent().getDataString();
                if (s != null && s.startsWith("justselfie://")) {
                    startGoogleAnalyticsFromDeepLink(s);
                    return;
                }
            }
        } catch (Exception ex) {}
        startAppWithoutParams();
    }

    private void startTrackingUXCAM() {
        if (getString(R.string.demo).matches("0")){
            UXCam.startWithKey(getString(R.string.uxcamera_token), getString(R.string.uxcamera_tag_build_release));
        }else{
            UXCam.startWithKey(getString(R.string.uxcamera_token), getString(R.string.uxcamera_tag_build_debug));
        }
        Utente utente = PreferenceManager.getUtente(this);
        if ( utente != null) {
            UXCam.tagUsersName(utente.getId() + "@" + utente.getNome());
        }
    }


    private void startAppWithoutParams() {
        if (PreferenceManager.getUtente(this) == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, JustSelfieActivity.class);
            startActivity(intent);
        }
        finish();
    }

    private void startAppWithParams(int viewDaAprire, int idContenuto) {
        if (PreferenceManager.getUtente(this) == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            UXCam.addTagWithProperties("Avvio", tagProperties);
            Intent intent = new Intent(this, JustSelfieActivity.class);
            intent.putExtra("view", viewDaAprire);
            intent.putExtra("id", idContenuto);
            startActivity(intent);
        }
        finish();
    }

}
