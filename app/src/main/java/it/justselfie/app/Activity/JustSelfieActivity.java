package it.justselfie.app.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.HitBuilders;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.uxcam.UXCam;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.AppConstants;
import it.justselfie.app.Fragments.EsercentiListaFragment;
import it.justselfie.app.Fragments.OfferteListaFragment;
import it.justselfie.app.Fragments.ProfiloFragment;
import it.justselfie.app.Fragments.ScoreFragment;
import it.justselfie.app.Fragments.WallFragment;
import it.justselfie.app.Fragments.WelcomeFragment;
import it.justselfie.app.Interface.SortFilterable;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Sconto;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.Model.SortFilter;
import it.justselfie.app.R;
import it.justselfie.app.Tools.FCMManager;
import it.justselfie.app.Tools.GpsTracker;
import it.justselfie.app.Tools.OnGeoCodingTaskCompleted;
import it.justselfie.app.Tools.PreferenceManager;
import me.leolin.shortcutbadger.ShortcutBadger;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by dmanfroni on 18/04/2016.
 */
public class JustSelfieActivity extends AnalyticsActivity {

    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;

    @InjectView(R.id.justselfie_bottombar)
    AHBottomNavigation bottomNavigation;

    @InjectView(R.id.justselfie_rootview)
    ViewGroup rootView;

    @InjectView(R.id.justselfie_contenitore)
    FrameLayout contenitore;

    @InjectView(R.id.justselfie_filter_button)
    ImageButton filtriButton;

    @InjectView(R.id.justselfie_search_bar)
    View ricercaLayout;

    @InjectView(R.id.justselfie_search_button)
    ImageView ricercaButton;

    @InjectView(R.id.justselfie_search_text)
    EditText ricercaTesto;

    @InjectView(R.id.justselfie_gps_button)
    ImageView gpsButton;


    private CallbackManager facebookReadCallbackManager;
    private Sconto tempSconto;
    private Selfie tempSelfie;
    private Drawer drawer;
    private SortFilter sortFilter;
    private GpsTracker gpsTracker;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    @Override
    public void onResume() {
        super.onResume();
        drawer.setSelectionAtPosition(0, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.tagScreenName("HOME");
        if (PreferenceManager.getUtente(this) != null) {
            super.getTracker().set("&uid", PreferenceManager.getUtente(this).getId() + "");
            super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
        }

        setContentView(R.layout.activity_justselfie);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setFacebookReadCallbackManager(CallbackManager.Factory.create());
        ButterKnife.inject(this);
        sortFilter = new SortFilter();
        if (PreferenceManager.getUtente(this).getUserIdFB() != 0)
            AccessToken.refreshCurrentAccessTokenAsync();
        FCMManager.setupRegistrationToken(this);
        setupToolbar();
        setupNavigationDrawer();
        setupBottomBar();
        setupFiltriBar();
        setupHome();
        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("nuovoUtente")){
            WelcomeFragment erroreFragment = WelcomeFragment.newInstance();
            erroreFragment.show(getSupportFragmentManager(), "welcome");
        }
        PreferenceManager.setCountNotification(0, this);
        ShortcutBadger.removeCount(this);
    }

    @Override
    protected void startGoogleAnalytics() {

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    protected void setupNavigationDrawer() {
        drawer = new DrawerBuilder(this)
                .withRootView(rootView)
                .withToolbar(toolbar)
                .withCloseOnClick(true)
                .withSliderBackgroundDrawableRes(R.mipmap.menu)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    switch (position) {
                        case 0:
                            bottomNavigation.setCurrentItem(0, false);
                            getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, OfferteListaFragment.newInstance()).commit();
                            break;
                        case 1:
                            Intent intent = new Intent(this, WebViewActivity.class);
                            intent.putExtra("url", getString(R.string.endpoint_faq));
                            startActivity(intent);
                            break;
                        case 2:
                            Intent intent7 = new Intent(Intent.ACTION_VIEW);
                            Uri data = Uri.parse("mailto:?subject=" + "Assistenza App JustSelfie");
                            intent7.setData(data);
                            intent7.putExtra(Intent.EXTRA_EMAIL, new String[]{"supporto@justselfie.it"});
                            startActivity(Intent.createChooser(intent7, "Invia email"));
                            break;
                        case 3:
                            Intent intent3 = new Intent(this, WebViewActivity.class);
                            intent3.putExtra("url", getString(R.string.endpoint_privacy));
                            startActivity(intent3);
                            break;
                        case 4:
                            Intent intent2 = new Intent(JustSelfieActivity.this, TutorialActivity.class);
                            intent2.putExtra("fromLeftMenu", 1);
                            startActivity(intent2);
                            break;
                        case 5:
                            Intent intent8 = new Intent(JustSelfieActivity.this, CodicePromoActivity.class);
                            intent8.putExtra("fromLeftMenu", 1);
                            startActivity(intent8);
                            break;
                        case 6:
                            LoginManager.getInstance().logOut();
                            PreferenceManager.setUtente(null, JustSelfieActivity.this);
                            startActivity(new Intent(JustSelfieActivity.this, LoginActivity.class));
                            finish();
                            break;
                    }
                    drawer.closeDrawer();
                    return true;
                })
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withIdentifier(1).withName("Home").withIcon(R.mipmap.icon_home).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true),
                        new PrimaryDrawerItem().withIdentifier(2).withName("Faq").withIcon(R.mipmap.icon_faq2).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true),
                        new PrimaryDrawerItem().withIdentifier(3).withName("Info e supporto").withIcon(R.mipmap.icon_info).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true),
                        new PrimaryDrawerItem().withIdentifier(4).withName("Privacy").withIcon(R.mipmap.icon_privacy).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true),
                        new PrimaryDrawerItem().withIdentifier(5).withName("Tutorial").withIcon(R.mipmap.icon_tutorial).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true),
                        new PrimaryDrawerItem().withIdentifier(5).withName("Codice promo").withIcon(R.mipmap.icona_codice_promo).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true),
                        new PrimaryDrawerItem().withIdentifier(7).withName("Disconnetti").withIcon(R.mipmap.icon_logout).withTextColor(Color.WHITE).withIconColor(Color.WHITE).withIconTintingEnabled(true)
                ).build();
    }

    private void setupBottomBar() {
        bottomNavigation.addItem(new AHBottomNavigationItem(R.string.home, R.mipmap.icona_vetrina, R.color.ah_bottombar_background));
        bottomNavigation.addItem(new AHBottomNavigationItem(R.string.js_point, R.mipmap.icon_jspoint, R.color.ah_bottombar_background));
        bottomNavigation.addItem(new AHBottomNavigationItem(R.string.profilo, R.mipmap.icon_profile, R.color.ah_bottombar_background));
        bottomNavigation.addItem(new AHBottomNavigationItem(R.string.premi, R.mipmap.selfie_coin_tapbar_icon, R.color.ah_bottombar_background));
        bottomNavigation.addItem(new AHBottomNavigationItem(R.string.wall, R.mipmap.icon_wall, R.color.ah_bottombar_background));
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#303030"));
        bottomNavigation.setAccentColor(Color.parseColor("#F39200"));
        bottomNavigation.setInactiveColor(Color.parseColor("#FFFFFF"));
        bottomNavigation.setForceTint(true);
        bottomNavigation.setOnTabSelectedListener((position, wasSelected) -> {
            if (!wasSelected) aggiornaContenuto(position);
            return true;
        });
    }

    private void setupFiltriBar() {
        ricercaTesto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                sortFilter.setTesto(editable.toString());
                Fragment fr = getSupportFragmentManager().findFragmentById(R.id.justselfie_contenitore);
                if (fr instanceof SortFilterable)
                    ((SortFilterable) fr).applySortFilter(sortFilter);
            }
        });
        gpsButton.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(JustSelfieActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.REQUEST_CODE_PERMISSIONS);
                    return;
                }
            }
            Fragment fr = getSupportFragmentManager().findFragmentById(R.id.justselfie_contenitore);
            if (fr instanceof OnGeoCodingTaskCompleted) {
                gpsTracker = new GpsTracker(JustSelfieActivity.this, (OnGeoCodingTaskCompleted) fr);
                if (gpsTracker.canGetLocation) {
                    gpsTracker.startUsingGPS();
                } else {
                    gpsTracker.showSettingsAlert();
                }
            }
        });
        filtriButton.setOnClickListener(view -> {
            Intent intent = new Intent(JustSelfieActivity.this, FiltriActivity.class);
            intent.putExtra("filtri", sortFilter);
            startActivityForResult(intent, AppConstants.REQUEST_CODE_FILTRI);
        });
    }

    private void setupHome() {
        setupHome((getIntent().getExtras() != null && getIntent().getExtras().containsKey("view")) ? getIntent().getExtras().getInt("view") : AppConstants.ACTION_HOME_VIEW);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.REQUEST_CODE_PERMISSIONS);

        } else {

            Location location = ((LocationManager) getSystemService(LOCATION_SERVICE)).getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                PreferenceManager.setLatitudine(location.getLatitude(), this);
                PreferenceManager.setLongitudine(location.getLongitude(), this);
                Fragment fr = getSupportFragmentManager().findFragmentById(R.id.justselfie_contenitore);
                if (fr instanceof OnGeoCodingTaskCompleted) {
                    ((OnGeoCodingTaskCompleted)fr).onLocationListener(location.getLatitude(), location.getLongitude());
                }
            }

        }

    }

    private void setupHome(int view) {
        switch (view) {
            case AppConstants.ACTION_HOME_VIEW:
                bottomNavigation.setCurrentItem(0, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, OfferteListaFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_SCONTI_VIEW:
                bottomNavigation.setCurrentItem(3, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, ScoreFragment.newInstance(2)).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
            case AppConstants.ACTION_SELFIES_VIEW:
                bottomNavigation.setCurrentItem(3, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, ScoreFragment.newInstance(3)).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
            case AppConstants.ACTION_MERCHANT_VIEW:
                bottomNavigation.setCurrentItem(1, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, EsercentiListaFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.VISIBLE);
                if (getIntent().getExtras().containsKey("id")) {
                    int idEsercente = getIntent().getExtras().getInt("id");
                    Ricerca ricerca = new Ricerca();
                    ricerca.setIdEsercente(idEsercente);
                    Intent intent = new Intent(this, EsercenteActivity.class);
                    intent.putExtra("ricerca", ricerca);
                    startActivity(intent);
                }
                break;
            case AppConstants.ACTION_OFFER_VIEW:
                bottomNavigation.setCurrentItem(0, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, OfferteListaFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.VISIBLE);
                if (getIntent().getExtras().containsKey("id")) {
                    int idEsercente = getIntent().getExtras().getInt("id");
                    Ricerca ricerca = new Ricerca();
                    ricerca.setIdEsercente(idEsercente);
                    Intent intent = new Intent(this, EsercenteActivity.class);
                    intent.putExtra("ricerca", ricerca);
                    startActivity(intent);
                }
                break;
            case AppConstants.ACTION_JSCARD_VIEW:
                bottomNavigation.setCurrentItem(3, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, ScoreFragment.newInstance(0)).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
            case AppConstants.ACTION_OFFER_PENDING_VIEW:
                bottomNavigation.setCurrentItem(3, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, ScoreFragment.newInstance(1)).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
            default:
                bottomNavigation.setCurrentItem(0, false);
                getSupportFragmentManager().beginTransaction().add(R.id.justselfie_contenitore, OfferteListaFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void aggiornaContenuto(int position) {
        switch (position) {
            case 0:
                getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, OfferteListaFragment.newInstance(sortFilter)).commit();
                ricercaLayout.setVisibility(View.VISIBLE);
                break;
            case 1:
                getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, EsercentiListaFragment.newInstance(sortFilter)).commit();
                ricercaLayout.setVisibility(View.VISIBLE);
                break;
            case 2:
                getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, ProfiloFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
            case 3:
                getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, ScoreFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
            case 4:
                getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, WallFragment.newInstance()).commit();
                ricercaLayout.setVisibility(View.GONE);
                break;
        }

    }

    public void vediPremi(int view) {
        bottomNavigation.setCurrentItem(3, false);
        getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, ScoreFragment.newInstance(view)).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getFacebookReadCallbackManager().onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQUEST_CODE_CAMERA_SCONTO && resultCode == AppCompatActivity.RESULT_OK) {
            String path = data.getData().getPath();
            Intent intent = new Intent(this, CondivisioneActivity.class);
            intent.putExtra("foto", path);
            intent.putExtra("oggetto", tempSconto);
            startActivity(intent);
        }
        if (requestCode == AppConstants.REQUEST_CODE_CAMERA_SELFIE && resultCode == AppCompatActivity.RESULT_OK) {
            String path = data.getData().getPath();
            Intent intent = new Intent(this, CondivisioneActivity.class);
            intent.putExtra("foto", path);
            intent.putExtra("oggetto", tempSelfie);
            startActivity(intent);
        }
        if (requestCode == AppConstants.REQUEST_CODE_FILTRI && resultCode == AppCompatActivity.RESULT_OK) {
            this.sortFilter = (SortFilter) data.getSerializableExtra("filtri");
            this.sortFilter.setTesto(ricercaTesto.getText().toString());
            Fragment fr = getSupportFragmentManager().findFragmentById(R.id.justselfie_contenitore);
            if (fr instanceof SortFilterable)
                ((SortFilterable) fr).applySortFilter(sortFilter);
        }
    }

    public void startCameraSconto(Sconto sconto) {
        tempSconto = sconto;
        Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, AppConstants.REQUEST_CODE_CAMERA_SCONTO);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
            return;
        }
        Fragment fr = getSupportFragmentManager().findFragmentById(R.id.justselfie_contenitore);
        if (fr instanceof WallFragment) {
            if (!((WallFragment) fr).tornaIndietro()) {
                bottomNavigation.setCurrentItem(0, false);
                getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, OfferteListaFragment.newInstance()).commit();
            }
        } else if (!(fr instanceof OfferteListaFragment)) {
            bottomNavigation.setCurrentItem(0, false);
            getSupportFragmentManager().beginTransaction().replace(R.id.justselfie_contenitore, OfferteListaFragment.newInstance()).commit();
        } else {
            if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                Toast.makeText(getBaseContext(), "Premi ancora per uscire!",
                        Toast.LENGTH_SHORT).show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }


    public CallbackManager getFacebookReadCallbackManager() {
        return facebookReadCallbackManager;
    }

    public void setFacebookReadCallbackManager(CallbackManager facebookReadCallbackManager) {
        this.facebookReadCallbackManager = facebookReadCallbackManager;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_CODE_PERMISSIONS:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    new AlertDialog.Builder(JustSelfieActivity.this)
                            .setTitle("Errore")
                            .setMessage("JustSelfie è un App che si basa sulle foto. Riprova quando sarai piu convinto")
                            .setNeutralButton("Ok", (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                                finish();
                            }).create().show();
                } else {
                    Location location = ((LocationManager) getSystemService(LOCATION_SERVICE)).getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        PreferenceManager.setLatitudine(location.getLatitude(), this);
                        PreferenceManager.setLongitudine(location.getLongitude(), this);
                        Fragment fr = getSupportFragmentManager().findFragmentById(R.id.justselfie_contenitore);
                        if (fr instanceof OnGeoCodingTaskCompleted) {
                            ((OnGeoCodingTaskCompleted)fr).onLocationListener(location.getLatitude(), location.getLongitude());
                        }
                    }
                }
        }
    }
}
