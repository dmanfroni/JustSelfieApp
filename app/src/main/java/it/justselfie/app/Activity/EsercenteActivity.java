package it.justselfie.app.Activity;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.AppConstants;
import it.justselfie.app.CustomViews.snackbar.SnackBarCarrello;
import it.justselfie.app.CustomViews.snackbar.SnackBarCarrelloLayout;
import it.justselfie.app.CustomViews.snackbar.SnackBarSelfieCoin;
import it.justselfie.app.CustomViews.snackbar.SnackBarSelfieCoinLayout;
import it.justselfie.app.Fragments.InfoContattiFragment;
import it.justselfie.app.Fragments.PromoFragment;
import it.justselfie.app.Fragments.WallFragment;
import it.justselfie.app.Model.Acquisto;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.Campagna;
import it.justselfie.app.Model.Esercente;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Sconto;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 08/11/2016.
 */

public class EsercenteActivity extends AnalyticsActivity {

    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;

    @InjectView(R.id.activity_esercente_scheda_immagine_copertina)
    ImageView immagineCopertina;


    @InjectView(R.id.activity_esercente_scheda_immagine_profilo)
    ImageView immagineProfilo;

    @InjectView(R.id.activity_esercente_tablayout)
    TabLayout tabLayout;

    @InjectView(R.id.activity_esercente_viewpager)
    ViewPager viewPager;

    @InjectView(R.id.activity_esercente_progressbar)
    ProgressBar progressBar;

    @InjectView(R.id.activity_esercente_coordinatorlayout)
    CoordinatorLayout coordinatorLayout;


    private Esercente esercente;
    private Campagna campagna;
    private Sconto sconto;
    private List<Selfie> selfie;
    private Ricerca ricerca;
    private Acquisto acquisto;
    private Utente utente;
    private PromoFragment promoFragment;
    private WallFragment wallFragment;
    private InfoContattiFragment infoContattiFragment;
    private SnackBarCarrello snackbarCarrello;
    private TextView snackBarCarrelloTesto;
    private Button snackBarCarrelloButton;
    private SnackBarSelfieCoin snackbarSelfieCoin;
    private TextView snackBarSelfieCoinTesto;
    private Button snackBarSelfieCoinButton;
    private int maxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 40;
    private boolean isAvatarShown = true;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQUEST_CODE_CARRELLO && resultCode == AppCompatActivity.RESULT_OK) {
            //acquisto = (Acquisto) data.getSerializableExtra("acquisto");
            //aggiornaBadgeAcquisto();
            return;
        }
        if (requestCode == AppConstants.REQUEST_CODE_CAMERA_SCONTO && resultCode == AppCompatActivity.RESULT_OK) {
            String path = data.getData().getPath();
            Intent intent = new Intent(this, CondivisioneActivity.class);
            intent.putExtra("foto", path);
            sconto.setEsercente(esercente);
            intent.putExtra("oggetto", sconto);
            startActivity(intent);
        }
        if (requestCode == AppConstants.REQUEST_CODE_CAMERA_SELFIE && resultCode == AppCompatActivity.RESULT_OK) {
            String path = data.getData().getPath();
            Selfie selfie4 = new Selfie();
            selfie4.setUtente(utente);
            selfie4.setEsercente(esercente);
            Intent intent = new Intent(this, CondivisioneActivity.class);
            intent.putExtra("foto", path);
            intent.putExtra("oggetto", selfie4);
            startActivity(intent);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onBackPressed();
        return true;

    }

    public void startAnimation() {
        if (getIntent().getExtras().containsKey("imgProf")) {
            Picasso.with(this).load(getString(R.string.radiceImmagini) + getIntent().getExtras().getString("imgProf")).centerCrop()
                    .fit().placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagineProfilo);

            immagineProfilo.postDelayed(() -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int cx = immagineProfilo.getMeasuredWidth() / 2;
                    int cy = immagineProfilo.getMeasuredHeight() / 2;
                    int finalRadius = Math.max(immagineProfilo.getWidth(), immagineProfilo.getHeight()) / 2;
                    Animator anim = ViewAnimationUtils.createCircularReveal(immagineProfilo, cx, cy, 0, finalRadius);

                    immagineProfilo.setVisibility(View.VISIBLE);
                    anim.setDuration(500);
                    anim.start();
                }
            }, 500);
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        setContentView(R.layout.activity_esercente_2);
        ButterKnife.inject(this);
        setupToolbar();
        acquisto = new Acquisto();
        utente = PreferenceManager.getUtente(this);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.activity_esercente_collapsingtoolbarlayout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.activity_esercente_appbarlayout);
        ricerca = getIntent().getExtras().containsKey("ricerca") ? (Ricerca) getIntent().getExtras().getSerializable("ricerca") : new Ricerca();
        ricerca.setIdUtente(utente.getId());
        if (getIntent().getExtras().containsKey("imgHead")) {
            Picasso.with(this).load(getString(R.string.radiceImmagini) + getIntent().getExtras().getString("imgHead")).centerCrop()
                    .fit().error(R.mipmap.no_image).into(immagineCopertina);
        } else {
            immagineCopertina.setVisibility(View.INVISIBLE);
        }
        if (getIntent().getExtras().containsKey("nome")) {
            collapsingToolbarLayout.setTitle(getIntent().getExtras().getString("nome"));
        }
        startAnimation();
        appBarLayout.addOnOffsetChangedListener((appBarLayout1, verticalOffset) -> {

            if (maxScrollSize == 0)
                maxScrollSize = appBarLayout1.getTotalScrollRange();

            int percentage = (Math.abs(verticalOffset)) * 100 / maxScrollSize;

            if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && isAvatarShown) {
                isAvatarShown = false;
                immagineProfilo.animate().scaleY(0).scaleX(0).setDuration(200).start();
            }

            if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !isAvatarShown) {
                isAvatarShown = true;
                immagineProfilo.animate()
                        .scaleY(1).scaleX(1)
                        .start();
            }
        });
        setupSnackBars();
        new SchedaEsercenteDownloader().execute();


    }

    @Override
    public void onResume() {
        super.onResume();
        aggiornaBadgeAcquisto();
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Esercente");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setupSnackBars() {

        snackbarCarrello = SnackBarCarrello.make(coordinatorLayout, "", SnackBarCarrello.LENGTH_INDEFINITE);
        SnackBarCarrelloLayout layoutCarrello = (SnackBarCarrelloLayout) snackbarCarrello.getView();
        snackBarCarrelloButton = (Button) layoutCarrello.findViewById(R.id.snackbar_action);
        snackBarCarrelloTesto = (TextView) layoutCarrello.findViewById(R.id.snackbar_text);
        snackBarCarrelloButton.setOnClickListener(view -> {
            acquisto.setEsercente(esercente);
            acquisto.setIdCampagna(campagna);
            Intent intent = new Intent(EsercenteActivity.this, CarrelloActivity.class);
            intent.putExtra("acquisto", acquisto);
            startActivityForResult(intent, AppConstants.REQUEST_CODE_CARRELLO);
        });

        snackbarSelfieCoin = SnackBarSelfieCoin.make(coordinatorLayout, "", SnackBarSelfieCoin.LENGTH_INDEFINITE);
        SnackBarSelfieCoinLayout layoutSelfieCoin = (SnackBarSelfieCoinLayout) snackbarSelfieCoin.getView();
        snackBarSelfieCoinButton = (Button) layoutSelfieCoin.findViewById(R.id.snackbar_action);
        snackBarSelfieCoinTesto = (TextView) layoutSelfieCoin.findViewById(R.id.snackbar_text);

        snackBarSelfieCoinTesto.setTextColor(Color.BLACK);
        snackBarSelfieCoinButton.setOnClickListener(view -> {
            riscuotiSelfieCoin();

        });
        snackBarSelfieCoinButton.setText("Guadagna Selfie Coin");


    }

    void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    void showView(JSONObject object) {
        try {
            if (object.has("esercente"))
                esercente = new Gson().fromJson(object.getJSONObject("esercente").toString(), Esercente.class);
            if (object.has("campagna"))
                campagna = new Gson().fromJson(object.getJSONObject("campagna").toString(), Campagna.class);
            if (object.has("sconto"))
                sconto = new Gson().fromJson(object.getJSONObject("sconto").toString(), Sconto.class);
            if (object.has("selfie"))
                selfie = new Gson().fromJson(object.getJSONArray("selfie").toString(), new TypeToken<List<Selfie>>() {
                }.getType());

            buildView();
        } catch (Exception ex) {
            showError(ex.getMessage());
        }
    }

    void buildView() {
        progressBar.setVisibility(View.GONE);
        if (esercente != null) {
            if (!getIntent().getExtras().containsKey("imgProf")) {
                Picasso.with(this).load(getString(R.string.radiceImmagini) + esercente.getImmagineProfilo()).centerCrop()
                        .fit().placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagineProfilo, new Callback() {
                    @Override
                    public void onSuccess() {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            int cx = immagineProfilo.getMeasuredWidth() / 2;
                            int cy = immagineProfilo.getMeasuredHeight() / 2;
                            int finalRadius = Math.max(immagineProfilo.getWidth(), immagineProfilo.getHeight()) / 2;
                            Animator anim = ViewAnimationUtils.createCircularReveal(immagineProfilo, cx, cy, 0, finalRadius);
                            immagineProfilo.setVisibility(View.VISIBLE);
                            anim.setDuration(500);
                            anim.start();
                        }
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
            if (!getIntent().getExtras().containsKey("imgHead")) {
                Picasso.with(this).load(getString(R.string.radiceImmagini) + esercente.getImmagineCopertina()).centerCrop()
                        .fit().placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagineCopertina, new Callback() {
                    @Override
                    public void onSuccess() {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            int cx = immagineCopertina.getMeasuredWidth() / 2;
                            int cy = immagineCopertina.getMeasuredHeight() / 2;
                            int finalRadius = Math.max(immagineCopertina.getWidth(), immagineCopertina.getHeight()) / 2;
                            Animator anim = ViewAnimationUtils.createCircularReveal(immagineCopertina, cx, cy, 0, finalRadius);
                            immagineCopertina.setVisibility(View.VISIBLE);
                            anim.setDuration(500);
                            anim.start();
                        }
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
            if (!getIntent().getExtras().containsKey("nome")) {
                collapsingToolbarLayout.setTitle(esercente.getNome());
            }
            infoContattiFragment = InfoContattiFragment.newInstance(
                    esercente.getNome(),
                    esercente.getDescrizione(),
                    esercente.getIndirizzo(),
                    esercente.getEmail(),
                    esercente.getTelefono(),
                    esercente.getLatitudine(),
                    esercente.getLongitudine()
            );
            String selfieCoingGuadagnati = "+" + esercente.getContratto().getSelfieCoin();
            SpannableString sommaSpannable = new SpannableString(selfieCoingGuadagnati);
            sommaSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, selfieCoingGuadagnati.length(), 0);
            sommaSpannable.setSpan(new RelativeSizeSpan(1.2f), 0, selfieCoingGuadagnati.length(), 0);
            sommaSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, selfieCoingGuadagnati.length(), 0);
            snackBarSelfieCoinTesto.setText(sommaSpannable);
            snackBarSelfieCoinTesto.append("\nSelfieCoin");

        }
        if (campagna != null) {
            promoFragment = PromoFragment.newInstance(campagna, esercente.getCategoria().getColore());
        }
        if (sconto != null) {
            promoFragment = PromoFragment.newInstance(sconto);
        }
        if (sconto == null && campagna == null) {
            promoFragment = PromoFragment.newInstance(esercente.getContratto().getSelfieCoin());
        }
        if (selfie != null) {
            final ArrayList<Selfie> selfieCopy = new ArrayList<>(selfie);
            wallFragment = WallFragment.newInstance(selfieCopy);
        }else{
            final ArrayList<Selfie> selfieCopy = new ArrayList<>();
            wallFragment = WallFragment.newInstance(selfieCopy);
        }

        tabLayout.addTab(tabLayout.newTab().setText("Info e contatti"));
        tabLayout.addTab(tabLayout.newTab().setText("Promo"));
        tabLayout.addTab(tabLayout.newTab().setText("Wall"));
        PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pageAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1, false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        if (snackbarCarrello.isShown()) snackbarCarrello.dismiss();
                        if (snackbarSelfieCoin.isShown()) snackbarSelfieCoin.dismiss();
                        break;
                    case 1:
                        if(campagna != null) {
                            aggiornaBadgeAcquisto();
                        }
                        if (snackbarSelfieCoin.isShown()) snackbarSelfieCoin.dismiss();
                        break;
                    case 2:
                        if (snackbarCarrello.isShown()) snackbarCarrello.dismiss();
                        snackbarSelfieCoin.show();
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void aggiornaAcquisto(AcquistoItem acquistoItem) {
        boolean found = false;
        for (int i = 0; i < acquisto.getOfferteAcquistate().size(); i++) {
            found = acquisto.getOfferteAcquistate().get(i).getOfferta().getId() == acquistoItem.getOfferta().getId();
            if (found) {
                if (acquistoItem.getQuantita() == 0) {
                    acquisto.getOfferteAcquistate().remove(i);
                } else {
                    acquisto.getOfferteAcquistate().get(i).setQuantita(acquistoItem.getQuantita());
                }
                break;
            }
        }
        if (!found)
            acquisto.getOfferteAcquistate().add(acquistoItem);
        aggiornaBadgeAcquisto();

    }

    public void aggiornaBadgeAcquisto() {
        int somma = 0;
        double totale = 0.0;
        DecimalFormat f = new DecimalFormat("0.00");
        for (AcquistoItem acquistoItem : acquisto.getOfferteAcquistate()) {
            somma = somma + acquistoItem.getQuantita();
            totale = totale + (acquistoItem.getQuantita() * acquistoItem.getOfferta().getPrezzoScontato());
        }
        SpannableString sommaSpannable = new SpannableString(somma + "");
        sommaSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, (somma + "").length(), 0);

        SpannableString importoSpannable = new SpannableString(f.format(totale) + "€");
        importoSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, f.format(totale).length() + 1, 0);


        if (somma > 0) {
            snackBarCarrelloTesto.setText("Offerte: ");
            snackBarCarrelloTesto.append(sommaSpannable);
            snackBarCarrelloTesto.append("\n");
            snackBarCarrelloTesto.append("Tot. ");
            snackBarCarrelloTesto.append(importoSpannable);
            snackbarCarrello.show();

        } else {
            snackbarCarrello.dismiss();
        }
        if(promoFragment != null)promoFragment.setShowSnackBar(somma > 0);
    }

    public void riscuotiSconto(){
        Intent startCustomCameraIntent = new Intent(EsercenteActivity.this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, AppConstants.REQUEST_CODE_CAMERA_SCONTO);
    }

    public void riscuotiSelfieCoin(){
        Intent startCustomCameraIntent = new Intent(EsercenteActivity.this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, AppConstants.REQUEST_CODE_CAMERA_SELFIE);
    }


    private class SchedaEsercenteDownloader extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            try {
                String s = new Gson().toJson(ricerca);
                JSONObject jsonObject = new JSONObject(NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_esercente_scheda, s));
                Thread.sleep(500);
                return jsonObject;
            } catch (Exception ex) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            try {
                if (object.getBoolean("success")) {
                    showView(object.getJSONObject("response"));
                } else {
                    showError(object.getString("response"));
                }
            } catch (Exception ex) {
                showError(ex.getMessage());
            }
        }
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public String getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Info e contatti";
                case 1:
                    return "Promo";
                case 2:
                    return "Wall";
                default:
                    return "Promo";
            }

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return infoContattiFragment;
                case 1:
                    return promoFragment;
                case 2:
                    return wallFragment;
                default:
                    return promoFragment;
            }
        }
    }
}
