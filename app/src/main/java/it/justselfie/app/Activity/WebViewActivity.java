package it.justselfie.app.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.google.android.gms.analytics.HitBuilders;
import com.uxcam.UXCam;

import it.justselfie.app.R;

/**
 * Created by Davide on 22/11/2016.
 */

public class WebViewActivity extends AnalyticsActivity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ((WebView)findViewById(R.id.activity_webview)).getSettings().setJavaScriptEnabled(true);
        ((WebView)findViewById(R.id.activity_webview)).loadUrl(getIntent().getExtras().getString("url"));
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("WebView");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }
}
