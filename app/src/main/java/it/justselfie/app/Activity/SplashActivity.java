package it.justselfie.app.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.analytics.AnalyticsReceiver;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.uxcam.UXCam;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import it.justselfie.app.App;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;

/**
 * Created by Davide on 25/10/2016.
 */

public class SplashActivity extends AnalyticsActivity {

    private HashMap<String, String> tagProperties;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tagProperties = new HashMap<>();
        startTrackingUXCAM();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        if(getIntent().getData() != null){
            String s = getIntent().getData().toString();
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        }

        printKeyHash();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    SplashActivity splashActivity = SplashActivity.this;
                    long start = System.currentTimeMillis();
                    if (NetworkManager.isNetworkAvaliable(splashActivity)) {
                        Utente utente = PreferenceManager.getUtente(splashActivity);
                        List<Utente> oldFriends = utente.getAmiciFB();
                        if(utente != null){
                            String s = new Gson().toJson(utente);
                            JSONObject utenteJSON =  new JSONObject(NetworkManager.doApiPOSTRequest(SplashActivity.this, NetworkManager.endpoint_utente_scheda, s));
                            utente = new Gson().fromJson(utenteJSON.getJSONObject("response").getJSONObject("utente").toString(), Utente.class);
                            utente.setAmiciFB(oldFriends);
                            PreferenceManager.setUtente(utente, splashActivity);
                        }
                    }
                    while ((System.currentTimeMillis() - start) < 3000) Thread.sleep(1000);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                if(PreferenceManager.getUtente(SplashActivity.this) == null){
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }else{
                    startActivity(new Intent(SplashActivity.this, JustSelfieActivity.class));
                }
                finish();

            }
        }.execute();
    }

    private void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getString(R.string.app_package_name), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void startTrackingUXCAM() {
        if (getString(R.string.demo).matches("0")){
            UXCam.startWithKey(getString(R.string.uxcamera_token), getString(R.string.uxcamera_tag_build_release));
        }else{
            UXCam.startWithKey(getString(R.string.uxcamera_token), getString(R.string.uxcamera_tag_build_debug));
        }
        Utente utente = PreferenceManager.getUtente(this);
        if ( utente != null) {
            UXCam.tagUsersName(utente.getId() + "@" + utente.getNome());
        }
        tagProperties.put("fonte", "home");
        tagProperties.put("contenuto", "home");
        UXCam.addTagWithProperties("Avvio", tagProperties);


    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Splash");
        if(PreferenceManager.getUtente(this) != null){
            super.getTracker().set("&uid", PreferenceManager.getUtente(this).getId() + "");
        }
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());

    }

}
