package it.justselfie.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.uxcam.UXCam;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 16/12/2016.
 */

public class CodicePromoActivity extends AnalyticsActivity{
    @InjectView(R.id.activity_codice_promo_annulla_button)
    ImageButton annullaButton;

    @InjectView(R.id.activity_codice_promo_riscuoti_button)
    ImageButton riscuotiButton;

    @InjectView(R.id.activity_codice_promo_text)
    TextView codicePromoText;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Codice Promo");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codice_promo);
        ButterKnife.inject(this);
        riscuotiButton.setOnClickListener((v)->{
            if(codicePromoText.getText().length() == 0){
                codicePromoText.setError("Inserire il codice");
                codicePromoText.requestFocus();
            }else{
                String codicePromo = codicePromoText.getText().toString();
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... voids) {
                        try{
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("idUtente", PreferenceManager.getUtente(CodicePromoActivity.this).getId());
                            jsonObject.put("codicePromo", codicePromo);
                            return NetworkManager.doApiPOSTRequest(CodicePromoActivity.this, NetworkManager.endpoint_utente_codicePromo, jsonObject.toString());
                        }catch (Exception ex){
                            return "";
                        }

                    }

                    @Override
                    protected void onPostExecute(String response){
                        if(response == null || response.isEmpty()){
                            show("Errore", 0);
                        }else{
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if(jsonObject.getBoolean("success")){
                                    show(jsonObject.getJSONObject("response").getString("messaggio"),
                                            jsonObject.getJSONObject("response").getInt("auth"));
                                }else {
                                    show(jsonObject.getString("response"), 0);
                                }
                            } catch (JSONException e) {
                                show(e.getMessage(), 0);
                            }
                        }
                    }
                }.execute();
            }
        });
        annullaButton.setOnClickListener((v)-> finishActivity());

    }

    private void show(String messaggio, int auth){
        new AlertDialog.Builder(this)
                .setMessage(messaggio)
                .setTitle(auth == 1 ? "Congratulazioni" : "Errore")
                .setNeutralButton("Ok", (dialogInterface, i) -> {if(auth == 1) finishActivity();})
                .create().show();

    }

    private void finishActivity(){
        if((getIntent().getExtras() != null && getIntent().getExtras().containsKey("nuovoUtente"))){
            Intent intent = new Intent(this, JustSelfieActivity.class);
            intent.putExtra("nuovoUtente", 1);
            startActivity(intent);
        }else if(getIntent().getExtras() == null){
            startActivity(new Intent(this, JustSelfieActivity.class));
        }else{
            finish();
        }
    }


}
