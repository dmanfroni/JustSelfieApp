package it.justselfie.app.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Nearable;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.AppConstants;
import it.justselfie.app.Database.DatabaseHelper;
import it.justselfie.app.Database.QueryBuilder;
import it.justselfie.app.Fragments.ErroreFragment;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.ConvalidaGiftCard;
import it.justselfie.app.Model.ConvalidaOfferta;
import it.justselfie.app.Model.ConvalidaSconto;
import it.justselfie.app.Model.ConvalidaSelfie;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 11/11/2016.
 */

public class ConvalidaActivity extends AnalyticsActivity {

    @InjectView(R.id.activity_convalida_progress_bar)
    ProgressBar progressBar;

    @InjectView(R.id.activity_convalida_progress_layout)
    View progressLayout;

    @InjectView(R.id.activity_convalida_progress_text)
    TextView progressText;

    @InjectView(R.id.activity_convalida_scrollview)
    NestedScrollView scrollView;

    @InjectView(R.id.activity_convalida_risultato_immagine)
    ImageView risultatoImmagine;

    @InjectView(R.id.activity_convalida_risultato_testo_header)
    TextView testoHeader;

    @InjectView(R.id.activity_convalida_risultato_divisore)
    View divisore;

    @InjectView(R.id.activity_convalida_risultato_testo_footer)
    TextView testoFooter;

    @InjectView(R.id.activity_convalida_esercente_nome)
    TextView nomeEsercente;

    @InjectView(R.id.activity_convalida_data_ora)
    TextView dataOra;

    @InjectView(R.id.activity_convalida_risultato_home_button)
    Button pulsanteHome;

    @InjectView(R.id.activity_convalida_chiudi_button)
    Button pulsanteChiudi;


    private CallbackManager facebookReadCallbackManager;

    private String scanId;
    private BeaconManager beaconManager;
    private String uuidMatch;

    private ProgressDialog progressDialog;

    private ConvalidaOfferta convalidaOfferta;
    private int scontoSuccessivo = 0;
    private ConvalidaSconto convalidaSconto;
    private ConvalidaSelfie convalidaSelfie;
    private ConvalidaGiftCard convalidaGiftCard;
    private int selfieCoin = 0;
    private boolean found = false;
    private int pendingACTION;
    private final int PENDING_ACTION_SCAN_BEACON = 1;
    private final int PENDING_ACTION_SCAN_GENERAL_BEACON = 2;


    @Override
    protected void onStop() {
        super.onStop();
        if (beaconManager != null)
            beaconManager.stopNearableDiscovery(scanId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (beaconManager != null)
            beaconManager.disconnect();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Convalida");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookReadCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQUEST_CODE_BLUETOOTH_PERMISSION && resultCode == Activity.RESULT_OK) {
            scanBeacon();
        } else if (requestCode == AppConstants.REQUEST_CODE_BLUETOOTH_PERMISSION && resultCode == Activity.RESULT_CANCELED) {
            showFatalError(getString(R.string.errore_bluetooth_non_abilitato));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_CODE_PERMISSIONS:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    showFatalError(getString(R.string.errore_permission_location_coarse_denied));
                    new AlertDialog.Builder(ConvalidaActivity.this)
                            .setTitle("Errore")
                            .setMessage(R.string.errore_permission_location_coarse_denied)
                            .setNeutralButton("Ok", (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                                finish();
                            }).create().show();
                } else {
                    scanBeacon();
                }
        }
    }

    private void showFatalError(String message) {
        ErroreFragment erroreFragment = ErroreFragment.newInstance(message);
        erroreFragment.show(getSupportFragmentManager(), "error");
        erroreFragment.setButtonListener(view -> {
            erroreFragment.dismiss();
            finish();
        });
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_convalida);

        ButterKnife.inject(this);
        facebookReadCallbackManager = CallbackManager.Factory.create();
        if (getIntent().getExtras().containsKey("convalidaOfferta")) {
            convalidaOfferta = (ConvalidaOfferta) getIntent().getExtras().getSerializable("convalidaOfferta");
            uuidMatch = convalidaOfferta.getSelfie().getEsercente().getBeaconUUID();
        } else if (getIntent().getExtras().containsKey("convalidaSconto")) {
            convalidaSconto = (ConvalidaSconto) getIntent().getExtras().getSerializable("convalidaSconto");
            uuidMatch = convalidaSconto.getSelfie().getEsercente().getBeaconUUID();
        } else if (getIntent().getExtras().containsKey("convalidaSelfie")) {
            convalidaSelfie = (ConvalidaSelfie) getIntent().getExtras().getSerializable("convalidaSelfie");
            uuidMatch = convalidaSelfie.getSelfie().getEsercente().getBeaconUUID();
        } else if (getIntent().getExtras().containsKey("convalidaGiftCard")) {
            convalidaGiftCard = (ConvalidaGiftCard) getIntent().getExtras().getSerializable("convalidaGiftCard");
            convalidaGiftCard.setIdUtente(PreferenceManager.getUtente(this).getId());
        }
        pulsanteHome.setOnClickListener(view -> {
            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        });
        ShimmerFrameLayout container = (ShimmerFrameLayout) findViewById(R.id.shimmer_view_container);
        container.startShimmerAnimation();

        pulsanteChiudi.setOnClickListener((v) -> finish());
        if (convalidaGiftCard == null) {
            pendingACTION = PENDING_ACTION_SCAN_BEACON;
            if (PreferenceManager.getShowFriendsRequestDialog(this))
                mostraDialogPermessi();
            else {
                doFacebookAccess();
            }
        } else {
            pendingACTION = PENDING_ACTION_SCAN_GENERAL_BEACON;
            scanBeacon();
        }
    }

    private void mostraDialogPermessi() {
        View promptView = getLayoutInflater().inflate(R.layout.dialog_request_friends, null);
        final CheckBox show = (CheckBox) promptView.findViewById(R.id.dialog_request_friends_checkbox);
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Autenticazione richiesta")
                .setCancelable(false)
                .setView(promptView)
                .setPositiveButton("Ok procedi", (dialogInterface, i) -> {
                    boolean notShowNextTime = show.isChecked();
                    PreferenceManager.setShowFriendsRequestDialog(!notShowNextTime, ConvalidaActivity.this);
                    doFacebookAccess();
                })
                .setNegativeButton("No grazie", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    finish();
                })
                .create();
        alertDialog.show();

    }

    private void doFacebookAccess() {

        LoginManager.getInstance().registerCallback(facebookReadCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getRecentlyDeniedPermissions().size() > 0) {
                    AlertDialog dialog = new AlertDialog.Builder(ConvalidaActivity.this)
                            .setMessage(R.string.errore_facebook_publish_permissions_denied)
                            .setCancelable(false)
                            .setNeutralButton("Ok", (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                                finish();
                            }).create();
                    dialog.show();

                } else {
                    progressLayout.setVisibility(View.VISIBLE);
                    scanBeacon();

                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                showFatalError(getString(R.string.errore_facebook_publish_permissions_authentication));
                error.printStackTrace();

            }
        });
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
    }

    private boolean checkPermissions() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            ErroreFragment erroreFragment = ErroreFragment.newInstance(getString(R.string.errore_bluetooth_non_supportato));
            erroreFragment.show(getSupportFragmentManager(), "error");
            erroreFragment.setButtonListener(view -> {
                erroreFragment.dismiss();
                finish();
            });
            return false;
        } else if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, AppConstants.REQUEST_CODE_BLUETOOTH_PERMISSION);
            return false;
        } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, AppConstants.REQUEST_CODE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void scanBeacon() {
        if (pendingACTION == PENDING_ACTION_SCAN_GENERAL_BEACON)
            startScanGeneralBeacon();
        else if (pendingACTION == PENDING_ACTION_SCAN_BEACON)
            startScanBeacon();
    }

    private void startScanGeneralBeacon() {
        if (checkPermissions()) {
            progressLayout.setVisibility(View.VISIBLE);
            Snackbar snackbar = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                snackbar = Snackbar.make(scrollView, "Non riesci a convalidare?", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("CLICCA QUI", view -> {
                    new AlertDialog.Builder(ConvalidaActivity.this)
                            .setMessage(R.string.message_beacon_scan)
                            .setTitle("Attenzione")
                            .setNeutralButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss())
                            .create().show();
                });
            }
            Snackbar finalSnackbar = snackbar;
            progressText.setText(R.string.message_beacon_scan);
            found = false;
            beaconManager = new BeaconManager(ConvalidaActivity.this);
            beaconManager.connect(() -> scanId = beaconManager.startNearableDiscovery());

            beaconManager.setNearableListener(nearables -> {
                for (Nearable nearable : nearables) {
                    if (nearable.rssi >= -70) {
                        if(finalSnackbar != null)
                            finalSnackbar.dismiss();
                        startConvalidaGiftCard(nearable.identifier);
                    }
                }
            });
        }
    }

    private void startScanBeacon() {
        if (checkPermissions()) {
            progressLayout.setVisibility(View.VISIBLE);
            Snackbar snackbar = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                snackbar = Snackbar.make(scrollView, "Non riesci a convalidare?", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("CLICCA QUI", view -> {
                    new AlertDialog.Builder(ConvalidaActivity.this)
                            .setMessage(R.string.message_beacon_scan)
                            .setTitle("Attenzione")
                            .setNeutralButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss())
                            .create().show();
                });
            }
            Snackbar finalSnackbar = snackbar;
            progressText.setText(R.string.message_beacon_scan);
            found = false;
            beaconManager = new BeaconManager(ConvalidaActivity.this);
            //beaconManager.start
            beaconManager.connect(() -> scanId = beaconManager.startNearableDiscovery());
            beaconManager.setNearableListener(nearables -> {
                for (Nearable nearable : nearables) {
                    if (nearable.rssi >= -70) {
                            if (getString(R.string.demo).matches("0") && nearable.identifier.equalsIgnoreCase(uuidMatch)) {
                            if(finalSnackbar != null)
                                finalSnackbar.dismiss();
                            startConvalida();
                        }else if(getString(R.string.demo).matches("1")){
                            if(finalSnackbar != null)
                                finalSnackbar.dismiss();
                            startConvalida();
                        }
                    }
                }
            });
        }
    }

    private void startConvalida(){
        if (!found) {
            found = true;
            if (beaconManager != null)
                beaconManager.stopNearableDiscovery(scanId);
            beaconManager = null;
            if (convalidaOfferta != null)
                convalidaOfferta();
            if (convalidaSconto != null)
                convalidaSconto();
            if (convalidaSelfie != null)
                convalidaSelfie();
        }
    }

    private void startConvalidaGiftCard(String identifier){
        if (!found) {
            found = true;
            if (beaconManager != null)
                beaconManager.stopNearableDiscovery(scanId);
            beaconManager = null;
            progressText.setText("Locale verificato! Stiamo processando il tuo buono");
            convalidaGiftCard.setBeaconUUID(identifier);
            convalidaGiftCard();
        }
    }

    private void convalidaOfferta() {
        progressText.setText("Locale verificato! Stiamo processando il tuo acquisto...");
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                InputStream inputStream = null;//You can get an inputStream using any IO API
                try {
                    inputStream = new FileInputStream(convalidaOfferta.getSelfie().getSelfie());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                String encodedString = Base64.encodeToString(bytes, Base64.NO_WRAP);
                convalidaOfferta.setImmagineBase64(encodedString);
                String json = new Gson().toJson(convalidaOfferta);
                return NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_convalida_offerta, json);
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getBoolean("success")) {
                        progressText.setText(getText(R.string.message_validation_purchase_success));
                        convalidaOfferta.getSelfie().setId(jsonObject.getJSONObject("response").getInt("idSelfie"));
                        convalidaOfferta.getSelfie().setSelfie(jsonObject.getJSONObject("response").getString("nomeSelfie"));
                        scontoSuccessivo = jsonObject.getJSONObject("response").getInt("scontoSuccessivo");
                        if (convalidaOfferta.getSelfie().getTipoCondivisione() == 1) {
                            pubblicaPostLink(convalidaOfferta.getSelfie(), convalidaOfferta.getUtentiTagOfferta());
                        } else {
                            pubblicaFoto(convalidaOfferta.getSelfie(), convalidaOfferta.getUtentiTagOfferta());
                        }
                    }
                } catch (Exception ex) {
                    Toast.makeText(ConvalidaActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    private void convalidaSconto() {
        progressText.setText("Locale verificato! Stiamo processando il tuo sconto...");

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(convalidaSconto.getSelfie().getSelfie());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                String encodedString = Base64.encodeToString(bytes, Base64.NO_WRAP);
                convalidaSconto.setImmagineBase64(encodedString);
                String json = new Gson().toJson(convalidaSconto);
                return NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_convalida_sconto, json);
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getBoolean("success")) {
                        progressText.setText(getText(R.string.message_validation_discount_success));
                        convalidaSconto.getSelfie().setId(jsonObject.getJSONObject("response").getInt("idSelfie"));
                        convalidaSconto.getSelfie().setSelfie(jsonObject.getJSONObject("response").getString("nomeSelfie"));
                        selfieCoin = jsonObject.getJSONObject("response").getInt("selfieCoin");
                        if (convalidaSconto.getSelfie().getTipoCondivisione() == 1) {
                            pubblicaPostLink(convalidaSconto.getSelfie());
                        } else {
                            pubblicaFoto(convalidaSconto.getSelfie());
                        }
                    }
                } catch (Exception ex) {
                    Toast.makeText(ConvalidaActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    private void convalidaSelfie() {
        progressText.setText("Locale verificato! Stiamo processando il tuo selfie...");
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                if (convalidaSelfie.getSelfie().getId() == 0) {
                    InputStream inputStream = null;
                    try {
                        inputStream = new FileInputStream(convalidaSelfie.getSelfie().getSelfie());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    byte[] bytes;
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    ByteArrayOutputStream output = new ByteArrayOutputStream();
                    try {
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, bytesRead);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bytes = output.toByteArray();
                    String encodedString = Base64.encodeToString(bytes, Base64.NO_WRAP);
                    convalidaSelfie.setImmagineBase64(encodedString);
                } else {
                    convalidaSelfie.setImmagineBase64("");
                }
                String json = new Gson().toJson(convalidaSelfie);
                return NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_convalida_selfie, json);
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getBoolean("success")) {

                        selfieCoin = jsonObject.getJSONObject("response").getInt("selfieCoin");
                        if (convalidaSelfie.getSelfie().getId() == 0) {
                            progressText.setText(getText(R.string.message_validation_selfie_success));
                            convalidaSelfie.getSelfie().setId(jsonObject.getJSONObject("response").getInt("idSelfie"));
                            convalidaSelfie.getSelfie().setSelfie(jsonObject.getJSONObject("response").getString("nomeSelfie"));
                            if (convalidaSelfie.getSelfie().getTipoCondivisione() == 1) {
                                pubblicaPostLink(convalidaSelfie.getSelfie());
                            } else {
                                pubblicaFoto(convalidaSelfie.getSelfie());
                            }
                        } else {
                            mostraRisultatoSelfie();
                        }
                    }
                } catch (Exception ex) {
                    Toast.makeText(ConvalidaActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    private void convalidaGiftCard() {
        progressText.setText("Locale verificato! Stiamo processando la tua JS CARD...");
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                String json = new Gson().toJson(convalidaGiftCard);
                return NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_convalida_giftcard, json);
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getBoolean("success")) {
                        List<Utente> oldFriends = PreferenceManager.getUtente(ConvalidaActivity.this).getAmiciFB();
                        Utente utente = new Gson().fromJson(jsonObject.getJSONObject("response").getJSONObject("utente").toString(), Utente.class);
                        utente.setAmiciFB(oldFriends);
                        PreferenceManager.setUtente(utente, ConvalidaActivity.this);
                        String es = jsonObject.getJSONObject("response").getString("nomeEsercente");
                        mostraRisultatoGiftCard(es);
                    }
                } catch (Exception ex) {
                    Toast.makeText(ConvalidaActivity.this, "Errore", Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    public void pubblicaPostLink(Selfie selfie) {
        Bundle params = new Bundle();
        String tags = "";
        for (Utente utente : selfie.getAmiciTaggatiFB()) {
            if (!tags.isEmpty())
                tags += ",";
            tags += utente.getUserIdFB();
        }
        params.putString("message", selfie.getTesto());
        params.putString("link", getString(R.string.endpoint_condivisione_justselfie) + selfie.getId());
        params.putString("place", selfie.getEsercente().getIdFacebook());

        if (!tags.isEmpty())
            params.putString("tags", tags);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", "ALL_FRIENDS");
            params.putString("privacy", jsonObject.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        new GraphRequest(
                accessToken,
                "/v2.8/me/feed",
                params,
                HttpMethod.POST,
                response -> {
                    FacebookRequestError f = response.getError();
                    if (f == null) {
                        if (convalidaOfferta != null)
                            mostraRisultatoAcquisto();
                        if (convalidaSconto != null)
                            mostraRisultatoSconto();
                        if (convalidaSelfie != null)
                            mostraRisultatoSelfie();
                    } else {
                        String errorMessage = f.getErrorMessage();
                        ErroreFragment erroreFragment = ErroreFragment.newInstance(errorMessage);
                        erroreFragment.show(getSupportFragmentManager(), "errore");
                        erroreFragment.setButtonListener(view -> {
                            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        });
                    }
                }
        ).executeAsync();

    }

    public void pubblicaPostLink(Selfie selfie, List<Utente> utentiExtra) {
        Bundle params = new Bundle();
        String tags = "";
        for (Utente utente : selfie.getAmiciTaggatiFB()) {
            if (!tags.isEmpty())
                tags += ",";
            tags += utente.getUserIdFB();
        }
        for (Utente utente : utentiExtra) {
            if (!tags.isEmpty())
                tags += ",";
            tags += utente.getUserIdFB();
        }
        params.putString("message", selfie.getTesto());
        params.putString("link", getString(R.string.endpoint_condivisione_justselfie) + selfie.getId());
        params.putString("place", selfie.getEsercente().getIdFacebook());
        if (!tags.isEmpty())
            params.putString("tags", tags);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", "ALL_FRIENDS");
            params.putString("privacy", jsonObject.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/feed",
                params,
                HttpMethod.POST,
                response -> {
                    FacebookRequestError f = response.getError();
                    if (f == null) {
                        if (convalidaOfferta != null)
                            mostraRisultatoAcquisto();
                        if (convalidaSconto != null)
                            mostraRisultatoSconto();
                        if (convalidaSelfie != null)
                            mostraRisultatoSelfie();
                    } else {
                        String errorMessage = f.getErrorMessage();
                        ErroreFragment erroreFragment = ErroreFragment.newInstance(errorMessage);
                        erroreFragment.show(getSupportFragmentManager(), "errore");
                        erroreFragment.setButtonListener(view -> {
                            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        });
                    }


                }
        ).executeAsync();

    }

    public void pubblicaFoto(Selfie selfie) {
        Bundle params = new Bundle();
        JSONArray jsonArray = new JSONArray();
        try {
            for (Utente utente : selfie.getAmiciTaggatiFB()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("tag_uid", utente.getUserIdFB());
                jsonArray.put(jsonObject);
            }
            params.putString("tags", jsonArray.toString());
        } catch (Exception ex) {

        }

        params.putString("caption", selfie.getTesto());
        params.putString("url", getString(R.string.radiceSelfie) + selfie.getSelfie());
        params.putString("place", selfie.getEsercente().getIdFacebook());
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", "ALL_FRIENDS");
            params.putString("privacy", jsonObject.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/photos",
                params,
                HttpMethod.POST,
                response -> {
                    FacebookRequestError f = response.getError();
                    if (f == null) {
                        if (convalidaOfferta != null)
                            mostraRisultatoAcquisto();
                        if (convalidaSconto != null)
                            mostraRisultatoSconto();
                        if (convalidaSelfie != null)
                            mostraRisultatoSelfie();
                    } else {
                        String errorMessage = f.getErrorMessage();
                        ErroreFragment erroreFragment = ErroreFragment.newInstance(errorMessage);
                        erroreFragment.show(getSupportFragmentManager(), "errore");
                        erroreFragment.setButtonListener(view -> {
                            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        });
                    }

                }
        ).executeAsync();
    }

    public void pubblicaFoto(Selfie selfie, List<Utente> utentiExtra) {
        Bundle params = new Bundle();
        String tags = "";
        for (Utente utente : selfie.getAmiciTaggatiFB()) {
            if (!tags.isEmpty())
                tags += ",";
            tags += utente.getUserIdFB();
        }
        for (Utente utente : utentiExtra) {
            if (!tags.isEmpty())
                tags += ",";
            tags += utente.getUserIdFB();
        }
        params.putString("caption", selfie.getTesto());
        params.putString("url", getString(R.string.radiceSelfie) + selfie.getSelfie());
        params.putString("place", selfie.getEsercente().getIdFacebook());
        if (!tags.isEmpty())
            params.putString("tags", tags);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", "ALL_FRIENDS");
            params.putString("privacy", jsonObject.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/photos",
                params,
                HttpMethod.POST,
                response -> {
                    FacebookRequestError f = response.getError();
                    if (f == null) {
                        if (convalidaOfferta != null)
                            mostraRisultatoAcquisto();
                        if (convalidaSconto != null)
                            mostraRisultatoSconto();
                        if (convalidaSelfie != null)
                            mostraRisultatoSelfie();
                    } else {
                        String errorMessage = f.getErrorMessage();
                        ErroreFragment erroreFragment = ErroreFragment.newInstance(errorMessage);
                        erroreFragment.show(getSupportFragmentManager(), "errore");
                        erroreFragment.setButtonListener(view -> {
                            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        });
                    }

                }
        ).executeAsync();

    }

    public void mostraRisultatoAcquisto() {
        pulsanteHome.setEnabled(false);
        final Snackbar snackbar = Snackbar
                .make(scrollView, "Mostra la schermata all'operatore di cassa", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", view -> {
            snackbar.dismiss();
            pulsanteHome.setEnabled(true);
        });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.parseColor("#F19004"));
        snackbar.show();

        dataOra.setText(getDataOra());
        nomeEsercente.setText("Presso " + convalidaOfferta.getSelfie().getEsercente().getNome());


        DatabaseHelper db = new DatabaseHelper(this);
        db.delete(QueryBuilder.tabellaAcquisti, convalidaOfferta.getId());
        db.close();
        risultatoImmagine.setImageResource(R.mipmap.illustrazione_offerta_benvenuto);
        testoHeader.setText("Il ricapitolo del tuo acquisto:\n");
        double importoTotale = 0.0;
        DecimalFormat f = new DecimalFormat("0.00");
        for (AcquistoItem acquistoItem : convalidaOfferta.getAcquisto().getOfferteAcquistate()) {
            SpannableString importoParziale = new SpannableString("€" + f.format(acquistoItem.getOfferta().getPrezzoScontato() * acquistoItem.getQuantita()));
            importoParziale.setSpan(new StyleSpan(Typeface.BOLD), 0, importoParziale.length(), 0);
            importoParziale.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, importoParziale.length(), 0);
            SpannableString qnt = new SpannableString(" (x" + acquistoItem.getQuantita() + ")");
            qnt.setSpan(new StyleSpan(Typeface.BOLD), 0, qnt.length(), 0);

            testoHeader.append("\n\n- " + acquistoItem.getOfferta().getNome());
            testoHeader.append(qnt);
            testoHeader.append(": ");
            testoHeader.append(importoParziale);
            importoTotale = importoTotale + (acquistoItem.getQuantita() * acquistoItem.getOfferta().getPrezzoScontato());
        }
        if(convalidaOfferta.getAcquisto().getSelfieCoinUsati() > 0){
            SpannableString importoParziale = new SpannableString("-€" + f.format(convalidaOfferta.getAcquisto().getSelfieCoinUsati()/100));
            importoParziale.setSpan(new StyleSpan(Typeface.BOLD), 0, importoParziale.length(), 0);
            importoParziale.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, importoParziale.length(), 0);
            SpannableString qnt = new SpannableString(" (x1)");
            qnt.setSpan(new StyleSpan(Typeface.BOLD), 0, qnt.length(), 0);
            testoHeader.append("\n\n- JS Card da " + ((int)convalidaOfferta.getAcquisto().getSelfieCoinUsati()/100) + "€");
            testoHeader.append(qnt);
            testoHeader.append(": ");
            testoHeader.append(importoParziale);
            importoTotale = importoTotale - ((int)convalidaOfferta.getAcquisto().getSelfieCoinUsati()/100);
            importoTotale = importoTotale < 0 ? 0 : importoTotale;
        }
        SpannableString importoTotaleString = new SpannableString("€" + f.format(importoTotale));
        importoTotaleString.setSpan(new StyleSpan(Typeface.BOLD), 0, importoTotaleString.length(), 0);
        importoTotaleString.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, importoTotaleString.length(), 0);
        importoTotaleString.setSpan(new RelativeSizeSpan(2f), 0, importoTotaleString.length(), 0);

        testoHeader.append("\n\nImporto totale dovuto: ");
        testoHeader.append(importoTotaleString);
        testoFooter.setText("Ricorda:\n\n " +
                "Torna entro " + convalidaOfferta.getSelfie().getEsercente().getContratto().getScontroScontrinoValidita() + " giorni " +
                "per ottenere uno sconto pari a " + scontoSuccessivo + "% per il tuo prossimo acquisto");
        progressLayout.setVisibility(View.GONE);
        pulsanteHome.setOnClickListener(view -> {
            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("scoreView", 3);
            startActivity(i);
        });
        scrollView.setVisibility(View.VISIBLE);

    }

    public void mostraRisultatoSconto() {
        pulsanteHome.setEnabled(false);
        final Snackbar snackbar = Snackbar
                .make(scrollView, "Mostra la schermata all'operatore di cassa", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", view -> {
            snackbar.dismiss();
            pulsanteHome.setEnabled(true);
        });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.parseColor("#F19004"));
        snackbar.show();

        dataOra.setText(getDataOra());
        nomeEsercente.setText("Presso " + convalidaSconto.getSelfie().getEsercente().getNome());


        risultatoImmagine.setImageResource(R.mipmap.illustrazione_sconto_percentuale);
        testoHeader.setText("Complimenti!\n\n");

        SpannableString sconto = new SpannableString(convalidaSconto.getSconto().getSconto() + "% di sconto");
        sconto.setSpan(new StyleSpan(Typeface.BOLD), 0, sconto.length(), 0);
        sconto.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, sconto.length(), 0);
        sconto.setSpan(new RelativeSizeSpan(1.5f), 0, sconto.length(), 0);


        testoHeader.append("Hai riscattato con successo il ");
        testoHeader.append(sconto);
        testoHeader.append(" sul totale dei tuoi acquisti");

        SpannableString selfieCoinString = new SpannableString(selfieCoin + "");
        selfieCoinString.setSpan(new StyleSpan(Typeface.BOLD), 0, selfieCoinString.length(), 0);
        selfieCoinString.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, selfieCoinString.length(), 0);
        selfieCoinString.setSpan(new RelativeSizeSpan(1.5f), 0, selfieCoinString.length(), 0);

        testoFooter.setText("Inoltre...\n\nHai guadagnato ");
        testoFooter.append(selfieCoinString);
        testoFooter.append(" selfie coin");
        pulsanteHome.setOnClickListener(view -> {
            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(i);
        });
        progressLayout.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }

    public void mostraRisultatoSelfie() {
        pulsanteHome.setEnabled(false);
        final Snackbar snackbar = Snackbar
                .make(scrollView, "Mostra la schermata all'operatore di cassa", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", view -> {
            snackbar.dismiss();
            pulsanteHome.setEnabled(true);
        });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.parseColor("#F19004"));
        snackbar.show();

        dataOra.setText(getDataOra());
        nomeEsercente.setText("Presso " + convalidaSelfie.getSelfie().getEsercente().getNome());


        risultatoImmagine.setImageResource(R.mipmap.illustrazione_selfiecoin);
        testoHeader.setText("Foto pubblicata con successo ");
        SpannableString selfieCoinString = new SpannableString(selfieCoin + "");
        selfieCoinString.setSpan(new StyleSpan(Typeface.BOLD), 0, selfieCoinString.length(), 0);
        selfieCoinString.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, selfieCoinString.length(), 0);
        selfieCoinString.setSpan(new RelativeSizeSpan(1.5f), 0, selfieCoinString.length(), 0);
        testoFooter.setText("Hai guadagnato ");
        testoFooter.append(selfieCoinString);
        testoFooter.append(" selfie coin");
        pulsanteHome.setOnClickListener(view -> {
            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(i);
        });
        progressLayout.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }

    public void mostraRisultatoGiftCard(String esercente) {
        pulsanteHome.setEnabled(false);
        final Snackbar snackbar = Snackbar
                .make(scrollView, "Mostra la schermata all'operatore di cassa", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", view -> {
            snackbar.dismiss();
            pulsanteHome.setEnabled(true);
        });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.parseColor("#F19004"));
        snackbar.show();

        switch (convalidaGiftCard.getValoreEuro()) {
            case 5:
                risultatoImmagine.setImageResource(R.mipmap.illustrazione_giftcard_5eu);
                break;
            case 10:
                risultatoImmagine.setImageResource(R.mipmap.illustrazione_giftcard_10eu);
                break;
            case 20:
                risultatoImmagine.setImageResource(R.mipmap.illustrazione_giftcard_20eu);
                break;
        }

        dataOra.setText(getDataOra());
        nomeEsercente.setText("Presso " + esercente);
        testoHeader.setText("JS Card convalidata con successo!");
        testoFooter.setText("Hai diritto ad un buono pari a ");
        SpannableString buonoString = new SpannableString(convalidaGiftCard.getValoreEuro() + "€");
        buonoString.setSpan(new StyleSpan(Typeface.BOLD), 0, buonoString.length(), 0);
        buonoString.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, buonoString.length(), 0);
        buonoString.setSpan(new RelativeSizeSpan(1.5f), 0, buonoString.length(), 0);
        testoFooter.append(buonoString);
        pulsanteHome.setOnClickListener(view -> {
            Intent i = new Intent(ConvalidaActivity.this, JustSelfieActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        });
        progressLayout.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }

    public String getDataOra() {
        DateFormat df6 = new SimpleDateFormat("E, dd-MM-yyyy HH:mm");
        String str6 = df6.format(new Date());
        String orario = str6.split(" ")[2];
        orario = " alle ore " + orario;
        String finalString = str6.split(" ")[0] + " " + str6.split(" ")[1] + orario;
        return finalString;
    }

    @Override
    public void onBackPressed() {

    }


}
