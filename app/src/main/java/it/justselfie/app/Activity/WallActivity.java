package it.justselfie.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.uxcam.UXCam;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Adapter.AdapterWall;
import it.justselfie.app.Fragments.WallFragment;
import it.justselfie.app.Model.Ricerca;
import it.justselfie.app.Model.Selfie;
import it.justselfie.app.R;
import it.justselfie.app.Tools.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 26/11/2016.
 */

public class WallActivity extends AppCompatActivity {


    @InjectView(R.id.activity_wall_framelayout)
    FrameLayout frameLayout;


    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Fragment fr = getSupportFragmentManager().findFragmentById(R.id.activity_wall_framelayout);
        if (fr instanceof WallFragment) {
            if(((WallFragment)fr).goBack()){}else super.onBackPressed();
        }
        return true;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private ArrayList<Selfie> elencoSelfie;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_wall);
        ButterKnife.inject(this);
        setupToolbar();
        elencoSelfie = (ArrayList<Selfie>) getIntent().getExtras().getSerializable("elencoSelfie");
        getSupportFragmentManager().beginTransaction().add(R.id.activity_wall_framelayout, WallFragment.newInstance(elencoSelfie)).commit();
    }


    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


}
