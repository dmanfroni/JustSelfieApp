package it.justselfie.app.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.Tracker;

import it.justselfie.app.App;

/**
 * Created by davide on 30/01/17.
 */

public abstract class AnalyticsActivity extends AppCompatActivity {

    private Tracker tracker;
    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        tracker = ((App) this.getApplication()).getDefaultTracker();
    }

    @Override
    public void onResume(){
        super.onResume();
        startGoogleAnalytics();
    }

    abstract protected void startGoogleAnalytics();

    public Tracker getTracker(){
        return this.tracker;
    }
}
