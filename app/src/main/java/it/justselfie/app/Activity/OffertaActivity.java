package it.justselfie.app.Activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.squareup.picasso.Picasso;
import com.uxcam.UXCam;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.DecimalFormat;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.Model.Offerta;
import it.justselfie.app.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 17/12/2016.
 */

public class OffertaActivity extends AnalyticsActivity {

    @InjectView(R.id.fragment_offerta_nome)
    TextView nomeOfferta;

    @InjectView(R.id.fragment_offerta_prezzo_pieno)
    TextView prezzoPieno;

    @InjectView(R.id.fragment_offerta_prezzo_scontato)
    TextView prezzoScontato;

    @InjectView(R.id.fragment_offerta_descrizione)
    WebView descrizioneOfferta;

    @InjectView(R.id.fragment_offerta_immagine)
    ImageView immagineOfferta;

    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;

    private Offerta offerta;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onBackPressed();
        return true;

    }


    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Offerta");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offerta);
        ButterKnife.inject(this);
        setupToolbar();
        offerta = (Offerta) getIntent().getExtras().getSerializable("offerta");
        if(offerta.getImmagine() != null && !offerta.getImmagine().isEmpty()){
            Picasso.with(this)
                    .load(getString(R.string.radiceImmagini) + offerta.getImmagine())
                    .centerCrop()
                    .fit()
                    .placeholder(R.mipmap.placeholder).error(R.mipmap.no_image).into(immagineOfferta);
        }
        nomeOfferta.setText(StringEscapeUtils.unescapeHtml4(offerta.getNome()));
        descrizioneOfferta.getSettings().setAllowFileAccess(true);
        descrizioneOfferta.getSettings().setPluginState(WebSettings.PluginState.ON);
        descrizioneOfferta.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        descrizioneOfferta.getSettings().setJavaScriptEnabled(true);
        descrizioneOfferta.getSettings().setLoadWithOverviewMode(true);
        descrizioneOfferta.getSettings().setUseWideViewPort(true);
        descrizioneOfferta.getSettings().setDefaultFontSize(25);
        String descrizione =  StringEscapeUtils.unescapeHtml4(offerta.getDescrizione());
        if(descrizione != null) {
            String content =
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                            "<html><head>" +
                            "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />" +
                            "<meta name=\"viewport\" content=\"target-densityDpi=device-dpi, width=device-width\"/>" +
                            "<style>@font-face {font-family: \'NeutraTextLight\';src: url(\'file:///android_asset/fonts/NeutraTextLight.otf\');}"+
                            "html, body {font-size: 20px; font-family: \'NeutraTextLight\';color: #3D3D3B;width:100%;height: auto;margin: 0px;padding: 4px;}</style></head><body style=\"margin: 0; padding: 0\">";

            content += descrizione + "</body></html>";
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP){
                descrizioneOfferta.loadData(content, "text/html; charset=utf-8", "UTF-8");
            }else{
                descrizioneOfferta.loadDataWithBaseURL("file:///android_asset/", content, "text/html; charset=utf-8", "UTF-8", null);
            }
        }else
            descrizioneOfferta.setVisibility(View.GONE);


        DecimalFormat f = new DecimalFormat("0.00");

        double prPieno = offerta.getPrezzoPieno();
        double prScontato = offerta.getPrezzoScontato();

        SpannableString text = new SpannableString(String.valueOf(f.format(prPieno) + "€"));
        text.setSpan(new StrikethroughSpan(), 0, text.length(), 0);
        prezzoPieno.setText(text, TextView.BufferType.SPANNABLE);
        prezzoScontato.setText(String.valueOf(f.format(prScontato) + "€"));
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }





}


