package it.justselfie.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.uxcam.UXCam;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Fragments.ErroreFragment;
import it.justselfie.app.Model.Utente;
import it.justselfie.app.R;
import it.justselfie.app.Tools.NetworkManager;
import it.justselfie.app.Tools.PreferenceManager;
import it.justselfie.app.Tools.Tools;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.CalligraphyUtils;

/**
 * Created by Davide on 24/10/2016.
 */

public class LoginActivity extends AnalyticsActivity {


    @InjectView(R.id.activity_login_tablayout)
    TabLayout tabLayout;
    @InjectView(R.id.activity_login_viewpager)
    ViewPager viewPager;
    @InjectView(R.id.activity_login_accesso_fb_button)
    ImageButton accessoFbButton;

    private CallbackManager facebookReadCallbackManager;

    private Utente utente;

    private ProgressDialog progressDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        facebookReadCallbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        utente = PreferenceManager.getUtente(this);
        accessoFbButton.setOnClickListener((v) -> doFacebookAccess());
        changeFontInViewGroup(tabLayout, "fonts/NeutraTextDemi.otf");
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Login");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    void changeFontInViewGroup(ViewGroup viewGroup, String fontPath) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View child = viewGroup.getChildAt(i);
            if (TextView.class.isAssignableFrom(child.getClass())) {
                CalligraphyUtils.applyFontToTextView(child.getContext(), (TextView) child, fontPath);
            } else if (ViewGroup.class.isAssignableFrom(child.getClass())) {
                changeFontInViewGroup((ViewGroup) viewGroup.getChildAt(i), fontPath);
            }
        }
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public String getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Accedi";
                case 1:
                    return "Registrati";
                default:
                    return "Accedi";
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return AccessoFragment.newInstance();
                case 1:
                    return RegistrazioneFragment.newInstance();
                default:
                    return RegistrazioneFragment.newInstance();
            }
        }
    }

    private void registraWom(int idUtenteB, int idSelfie) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("idSelfie", idSelfie);
                    jsonObject.put("idUtente", idUtenteB);
                    NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_wom_registra, jsonObject.toString());
                } catch (Exception ex) {

                }
                return null;
            }

        }.execute();
    }

    private void doFacebookAccess() {

        LoginManager.getInstance().registerCallback(facebookReadCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getRecentlyDeniedPermissions().size() > 0) {
                    Toast.makeText(LoginActivity.this, "Ci dispiace :( Devi accettare tutti i permessi per offrirti una migliore esperienza su JustSelfie", Toast.LENGTH_LONG).show();
                } else {
                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setMessage("Accesso in corso");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                    String token = loginResult.getAccessToken().getToken();
                    readFacebookData();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                ErroreFragment erroreFragment = ErroreFragment.newInstance(error.getMessage());
                erroreFragment.show(getSupportFragmentManager(), "errore");
                error.printStackTrace();

            }
        });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends", "email", "public_profile"));
    }

    private void readFacebookData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), (object, response) -> {
            try {
                utente = new Utente();
                utente.setSesso(object.getString("gender"));
                if (object.has("email"))
                    utente.setEmail(object.getString("email"));
                else
                    utente.setEmail(object.getString("id") + "@justselfie.it");
                utente.setNome(object.getString("name"));
                String id = object.getString("id");
                utente.setUserIdFB(Long.parseLong(id));
                utente.setImmagineProfilo("https://graph.facebook.com/" + utente.getUserIdFB() + "/picture?type=large");
                readFacebookFriends();

            } catch (Exception ex) {
                progressDialog.dismiss();
                ex.printStackTrace();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "gender, name, email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void recursiveFriendsReader(GraphResponse lastGraphResponse) {
        try {
            if (lastGraphResponse != null) {
                JSONObject object = lastGraphResponse.getJSONObject();
                if (object != null) {
                    JSONArray amici = object.getJSONArray("data");
                    for (int i = 0; i < amici.length(); i++) {
                        JSONObject friendObject = amici.getJSONObject(i);
                        Utente amico = new Utente();
                        amico.setNome(friendObject.getString("name"));
                        amico.setUserIdFB(Long.parseLong(friendObject.getString("id")));
                        utente.getAmiciFB().add(amico);
                    }
                }
                GraphRequest nextResultsRequests = lastGraphResponse.getRequestForPagedResults(GraphResponse.PagingDirection.NEXT);
                if (nextResultsRequests != null) {
                    nextResultsRequests.setCallback(response -> recursiveFriendsReader(response));
                    nextResultsRequests.executeAsync();
                } else {
                    autenticazioneFB();
                }
            }
        } catch (Exception ex) {
            progressDialog.dismiss();
            ex.printStackTrace();
        }
    }

    private void readFacebookFriends() {
        new GraphRequest().newMyFriendsRequest(AccessToken.getCurrentAccessToken(), (array, response) -> {
            try {
                recursiveFriendsReader(response);
            } catch (Exception ex) {
                progressDialog.dismiss();
                ex.printStackTrace();
            }
        }).executeAsync();
    }

    private void autenticazioneFB() {
        new AsyncTask<Void, Void, JSONObject>() {
            @Override
            protected JSONObject doInBackground(Void... params) {
                try {
                    String response = NetworkManager.doApiPOSTRequest(getApplicationContext(), NetworkManager.endpoint_utente_autenticazioneFB, new Gson().toJson(utente));
                    return new JSONObject(response);
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(JSONObject result) {

                progressDialog.dismiss();
                try {
                    if (result.getBoolean("success")) {
                        JSONObject response = result.getJSONObject("response");
                        if (response.getInt("auth") > 0) {
                            utente = new Gson().fromJson(response.getJSONObject("utente").toString(), Utente.class);
                            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("wom"))
                                registraWom(utente.getId(), getIntent().getExtras().getInt("idSelfie"));
                            PreferenceManager.setUtente(utente, LoginActivity.this);
                            int nuovoUtente = response.getInt("nuovoUtente");
                            start(nuovoUtente, utente);
                        } else if (response.getInt("auth") < 0) {
                            ErroreFragment erroreFragment = ErroreFragment.newInstance("Email gia esistente sul nostro sistema. Inserisci la tua email e la password e collega l'account Facebook dalla schermata di profilo");
                            erroreFragment.show(getSupportFragmentManager(), "errore");
                        } else if (response.getInt("auth") == 0) {
                            ErroreFragment erroreFragment = ErroreFragment.newInstance("Account facebook associato ad un altra mail");
                            erroreFragment.show(getSupportFragmentManager(), "errore");
                        }

                    } else {
                        throw new Exception(result.getString("response"));
                    }
                } catch (Exception ex) {
                    ErroreFragment erroreFragment = ErroreFragment.newInstance(ex.getMessage());
                    erroreFragment.show(getSupportFragmentManager(), "errore");
                }
            }
        }.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookReadCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void start(int nuovoUtente, Utente utente) {
        this.utente = utente;
        if (utente != null) {
            super.getTracker().set("&uid", PreferenceManager.getUtente(this).getId() + "");
            super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
            super.getTracker().send(new HitBuilders.EventBuilder()
                    .setCategory("User actions")
                    .setAction(nuovoUtente == 1 ? "User Sign Up" : "User Logged In")
                    .build());
            UXCam.tagUsersName(utente.getId() + "@" + utente.getNome());
        }

        Intent intent = null;
        if (nuovoUtente == 1) {
            intent = new Intent(LoginActivity.this, TutorialActivity.class);
            intent.putExtra("nuovoUtente", 1);
        } else {
            intent = new Intent(LoginActivity.this, JustSelfieActivity.class);
        }
        startActivity(intent);
        finish();
    }


    public static class AccessoFragment extends Fragment {

        @InjectView(R.id.fragment_login_accesso_email)
        EditText emailTextView;

        @InjectView(R.id.fragment_login_accesso_password)
        EditText passwordTextView;

        @InjectView(R.id.fragment_login_accesso_button)
        ImageButton accessoJSButton;

        @InjectView(R.id.fragment_login_accesso_password_recovery)
        TextView passwordRecovery;

        private ProgressDialog progressDialog;

        private Utente utente;

        private LoginActivity loginActivity;

        public static AccessoFragment newInstance() {
            return new AccessoFragment();
        }

        public AccessoFragment() {

        }

        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
            super.onCreateView(inflater, root, bundle);
            View view = inflater.inflate(R.layout.fragment_login_accesso, null);
            ButterKnife.inject(this, view);
            return view;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            loginActivity = (LoginActivity) getActivity();
            accessoJSButton.setOnClickListener((v) -> doJustSelfieAccess());
            passwordRecovery.setOnClickListener((v) -> {
                Intent intent3 = new Intent(loginActivity, WebViewActivity.class);
                intent3.putExtra("url", getString(R.string.endpoint_password_recovery));
                startActivity(intent3);
            });
        }

        private void doJustSelfieAccess() {
            if (emailTextView.getText().toString().trim().isEmpty()) {
                emailTextView.setError("Inserire l'email");
                emailTextView.requestFocus();
                return;
            }
            if (!Tools.validate(emailTextView.getText().toString().trim())) {
                emailTextView.setError("Formato email non valido");
                emailTextView.requestFocus();
                return;
            }
            if (passwordTextView.getText().toString().trim().isEmpty()) {
                passwordTextView.setError("Inserire la password");
                passwordTextView.requestFocus();
                return;
            }
            if (passwordTextView.getText().toString().trim().length() < 4) {
                passwordTextView.setError("Minimo 4 caratteri");
                passwordTextView.requestFocus();
                return;
            }
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Accesso in corso");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            utente = new Utente();
            utente.setEmail(emailTextView.getText().toString().trim());
            String hashPassword = NetworkManager.sStringToHMACMD5(passwordTextView.getText().toString().trim());
            utente.setPassword(hashPassword);
            autenticazioneJS();
        }

        private void autenticazioneJS() {
            new AsyncTask<String, Void, JSONObject>() {
                @Override
                protected JSONObject doInBackground(String... params) {
                    try {
                        String parameters = new Gson().toJson(utente);
                        return new JSONObject(NetworkManager.doApiPOSTRequest(loginActivity, NetworkManager.endpoint_utente_autenticazioneJS, parameters));
                    } catch (Exception ex) {
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(JSONObject result) {
                    progressDialog.dismiss();
                    try {
                        if (result.getBoolean("success")) {
                            JSONObject response = result.getJSONObject("response");
                            int auth = response.getInt("auth");
                            if (auth == -1) {
                                emailTextView.setError("L'email non esiste");
                                emailTextView.requestFocus();
                                return;
                            } else if (auth == 0) {
                                passwordTextView.setError("Password non valida");
                                passwordTextView.requestFocus();
                                return;
                            }
                            utente = new Gson().fromJson(response.getJSONObject("utente").toString(), Utente.class);
                            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("wom"))
                                ((LoginActivity) getActivity()).registraWom(utente.getId(), getActivity().getIntent().getExtras().getInt("idSelfie"));
                            PreferenceManager.setUtente(utente, loginActivity);
                            int nuovoUtente = response.getInt("nuovoUtente");
                            loginActivity.start(nuovoUtente, utente);
                        } else {
                            throw new Exception(result.getString("response"));
                        }
                    } catch (Exception ex) {
                        Toast.makeText(loginActivity, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }.execute(new String[]{emailTextView.getText().toString(), passwordTextView.getText().toString()});
        }
    }

    public static class RegistrazioneFragment extends Fragment {

        @InjectView(R.id.fragment_registrazione_email)
        EditText emailTextView;

        @InjectView(R.id.fragment_registrazione_nome)
        EditText nomeTextView;

        // @InjectView(R.id.fragment_registrazione_telefono)
        EditText telefonoTextView;

        @InjectView(R.id.fragment_registrazione_password)
        EditText passwordTextView;

        @InjectView(R.id.fragment_registrazione_conferma_password)
        EditText confermaPasswordTextView;

        @InjectView(R.id.fragment_registrazione_button)
        ImageButton registrazioneButton;

        private ProgressDialog progressDialog;

        private Utente utente;

        private LoginActivity loginActivity;

        public static RegistrazioneFragment newInstance() {
            return new RegistrazioneFragment();
        }

        public RegistrazioneFragment() {

        }

        public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
            super.onCreateView(inflater, root, bundle);
            View view = inflater.inflate(R.layout.fragment_login_registrazione, null);
            ButterKnife.inject(this, view);

            return view;
        }

        @Override
        public void onViewCreated(View view, Bundle bundle) {
            super.onViewCreated(view, bundle);
            loginActivity = (LoginActivity) getActivity();
            registrazioneButton.setOnClickListener((v) -> doRegistrazioneJS());
        }

        private void doRegistrazioneJS() {
            if (emailTextView.getText().toString().trim().isEmpty()) {
                emailTextView.setError("Inserire l'email");
                emailTextView.requestFocus();
                return;
            }
            if (!Tools.validate(emailTextView.getText().toString().trim())) {
                emailTextView.setError("Formato email non valido");
                emailTextView.requestFocus();
                return;
            }
            if (nomeTextView.getText().toString().trim().isEmpty()) {
                nomeTextView.setError("Inserire il nome");
                nomeTextView.requestFocus();
                return;
            }
            if (passwordTextView.getText().toString().trim().isEmpty()) {
                passwordTextView.setError("Inserire la password");
                passwordTextView.requestFocus();
                return;
            }
            if (!passwordTextView.getText().toString().trim().matches(confermaPasswordTextView.getText().toString().trim())) {
                confermaPasswordTextView.setError("Le password non coincidono");
                confermaPasswordTextView.requestFocus();
                return;
            }
            progressDialog = new ProgressDialog(loginActivity);
            progressDialog.setMessage("Accesso in corso");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            utente = new Utente();
            utente.setNome(nomeTextView.getText().toString().trim());
            utente.setEmail(emailTextView.getText().toString().trim());
            String hashPassword = NetworkManager.sStringToHMACMD5(passwordTextView.getText().toString().trim());
            utente.setPassword(hashPassword);
            registrazioneJS();
        }

        private void registrazioneJS() {
            new AsyncTask<String, Void, JSONObject>() {
                @Override
                protected JSONObject doInBackground(String... params) {
                    try {
                        String response = NetworkManager.doApiPOSTRequest(loginActivity, NetworkManager.endpoint_utente_registrazioneJS, new Gson().toJson(utente));
                        return new JSONObject(response);
                    } catch (Exception ex) {
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(JSONObject result) {
                    progressDialog.dismiss();
                    try {
                        if (result.getBoolean("success")) {
                            JSONObject response = result.getJSONObject("response");
                            if (response.getInt("auth") > 0) {
                                utente = new Gson().fromJson(response.getJSONObject("utente").toString(), Utente.class);
                                if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("wom"))
                                    ((LoginActivity) getActivity()).registraWom(utente.getId(), getActivity().getIntent().getExtras().getInt("idSelfie"));

                                PreferenceManager.setUtente(utente, loginActivity);
                                int nuovoUtente = response.getInt("nuovoUtente");
                                loginActivity.start(nuovoUtente, utente);

                            } else {
                                emailTextView.setError("Email gia esistente");
                                emailTextView.requestFocus();
                            }
                        } else {
                            throw new Exception(result.getString("response"));
                        }
                    } catch (Exception ex) {
                        Toast.makeText(loginActivity, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    ;
                }
            }.execute();
        }
    }

}
