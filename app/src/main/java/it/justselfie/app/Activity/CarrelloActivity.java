package it.justselfie.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.justselfie.app.Adapter.AdapterCarrello;
import it.justselfie.app.AppConstants;
import it.justselfie.app.CustomViews.snackbar.SnackBarCarrello;
import it.justselfie.app.CustomViews.SimpleDividerItemDecoration;
import it.justselfie.app.CustomViews.snackbar.SnackBarCarrelloLayout;
import it.justselfie.app.Model.Acquisto;
import it.justselfie.app.Model.AcquistoItem;
import it.justselfie.app.R;
import it.justselfie.app.Tools.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Davide on 09/11/2016.
 */

public class CarrelloActivity extends AnalyticsActivity {

    @InjectView(R.id.activity_carrello_recyclerview)
    RecyclerView recyclerView;

    @InjectView(R.id.justselfie_toolbar)
    Toolbar toolbar;

    @InjectView(R.id.activity_carrello_coordinatorlayout)
    CoordinatorLayout coordinatorLayout;

    @InjectView(R.id.activity_carrello_omaggio_layout)
    View omaggioLayout;

    /**
     * Layout Omaggio disponibile
     */

    @InjectView(R.id.activity_carrello_omaggio__disponibile_layout)
    View omaggioDisponibileLayout;

    @InjectView(R.id.activity_carrello_omaggio_disponibile_text)
    TextView omaggioDisponibileTesto;

    @InjectView(R.id.activity_carrello_omaggio_disponibile_button)
    View omaggioDisponibileButton;


    /**
     * Layout Omaggio selezionato
     */

    @InjectView(R.id.activity_carrello_omaggio_selezionato_layout)
    View omaggioSelezionatoLayout;

    @InjectView(R.id.activity_carrello_omaggio_selezionato_nome)
    TextView omaggioSelezionatoNome;

    @InjectView(R.id.activity_carrello_omaggio_selezionato_importo)
    TextView omaggioSelezionatoImporto;

    @InjectView(R.id.activity_carrello_omaggio_selezionato_immagine)
    ImageView omaggioSelezionatoIcona;

    @InjectView(R.id.activity_carrello_omaggio_selezionato_rimuovi_button)
    View omaggioSelezionatoRimuoviButton;





    private Acquisto acquisto;
    private SnackBarCarrello snackbar;
    private TextView snacbKbarTesto;
    private Button snackBarButton;
    private int omg = 0;
    private int selfieCoinDisponibili = 0;
    public static final int OMAGGIO_JS_CARD_5 = 1;
    public static final int OMAGGIO_JS_CARD_10 = 2;
    public static final int OMAGGIO_JS_CARD_20 = 3;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finishActivity();
        return true;

    }

    private void finishActivity(){
        Intent data = new Intent();
        Bundle b = new Bundle();
        b.putSerializable("acquisto", acquisto);
        data.putExtras(b);
        if (getParent() == null) {
            setResult(AppCompatActivity.RESULT_OK, data);
        } else {
            getParent().setResult(AppCompatActivity.RESULT_OK, data);
        }
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQUEST_CODE_CAMERA_ACQUISTO && resultCode == AppCompatActivity.RESULT_OK) {
            String path = data.getData().getPath();
            Intent intent = new Intent(this, CondivisioneActivity.class);
            intent.putExtra("foto", path);
            intent.putExtra("oggetto", acquisto);
            startActivity(intent);
        }
    }

    @Override
    protected void startGoogleAnalytics(){
        super.getTracker().setScreenName("Checkout");
        super.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_carrello);
        ButterKnife.inject(this);
        selfieCoinDisponibili = PreferenceManager.getUtente(this).getSelfieCoin();
        setupToolbar();
        acquisto = (Acquisto) getIntent().getExtras().getSerializable("acquisto");
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView.setAdapter(new AdapterCarrello(this, acquisto.getOfferteAcquistate()));
        snackbar = SnackBarCarrello.make(coordinatorLayout, "", SnackBarCarrello.LENGTH_INDEFINITE);
        SnackBarCarrelloLayout layout = (SnackBarCarrelloLayout) snackbar.getView();
        snackBarButton = (Button)layout.findViewById(R.id.snackbar_action);
        snacbKbarTesto = (TextView) layout.findViewById(R.id.snackbar_text);
        snackBarButton.setOnClickListener(view -> {
            Intent startCustomCameraIntent = new Intent(CarrelloActivity.this, CameraActivity.class);
            startActivityForResult(startCustomCameraIntent, AppConstants.REQUEST_CODE_CAMERA_ACQUISTO);
        });
        snackBarButton.setText("Scatta e convalida");
        updateCarrello();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setupOmaggioDisponibileLayout(){
        if(selfieCoinDisponibili < 500){
            omaggioLayout.setVisibility(View.GONE);
        }else {

            omaggioSelezionatoLayout.setVisibility(View.GONE);
            omaggioDisponibileLayout.setVisibility(View.VISIBLE);
            List<String> omaggi = new ArrayList<String>();
            omaggi.add("Nessun omaggio");
            if (selfieCoinDisponibili >= 500)
                omaggi.add("JS Card da 5€ (500 Selfie Coin)");
            if (selfieCoinDisponibili >= 1000)
                omaggi.add("JS Card da 10€ (1000 Selfie Coin)");
            if (selfieCoinDisponibili >= 2000)
                omaggi.add("JS Card da 20€ (2000 Selfie Coin)");
            String[] omaggiArray = new String[omaggi.size()];
            for (int i = 0; i < omaggi.size(); i++) omaggiArray[i] = omaggi.get(i);
            omaggioDisponibileButton.setOnClickListener(view -> {
                new AlertDialog.Builder(CarrelloActivity.this)
                        .setSingleChoiceItems(omaggiArray, -1, (dialogInterface, i) -> {
                            omg = i;

                            dialogInterface.dismiss();
                            updateCarrello();
                        }).create().show();

            });
            omaggioDisponibileTesto.setText("Hai ");
            SpannableString importoSpannable = new SpannableString("1 sconto");
            importoSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#F19004")), 0, 8, 0);
            omaggioDisponibileTesto.append(importoSpannable);
            omaggioDisponibileTesto.append(" a disposizione");
            omaggioSelezionatoRimuoviButton.setOnClickListener((v)->rimuoviOmaggio());
        }
    }

    public void updateCarrello() {
        int somma = 0;
        double totale = 0.0;
        DecimalFormat f = new DecimalFormat("0.00");
        for (AcquistoItem acquistoItem : acquisto.getOfferteAcquistate()) {
            somma = somma + acquistoItem.getQuantita();
            totale = totale + (acquistoItem.getQuantita() * acquistoItem.getOfferta().getPrezzoScontato());
        }
        switch(omg){
            case OMAGGIO_JS_CARD_5:
                acquisto.setSelfieCoinUsati(500);
                totale = totale -5;
                totale = totale < 0 ? 0 : totale;
                omaggioSelezionatoLayout.setVisibility(View.VISIBLE);
                omaggioDisponibileLayout.setVisibility(View.GONE);
                omaggioSelezionatoIcona.setImageResource(R.mipmap.giftcard_5);
                omaggioSelezionatoNome.setText("JS Card da 5€");
                omaggioSelezionatoImporto.setText("-5€");
                break;
            case OMAGGIO_JS_CARD_10:
                acquisto.setSelfieCoinUsati(1000);
                totale = totale -10;
                totale = totale < 0 ? 0 : totale;
                omaggioSelezionatoLayout.setVisibility(View.VISIBLE);
                omaggioDisponibileLayout.setVisibility(View.GONE);
                omaggioSelezionatoIcona.setImageResource(R.mipmap.giftcard_10);
                omaggioSelezionatoNome.setText("JS Card da 10€");
                omaggioSelezionatoImporto.setText("-10€");
                break;
            case OMAGGIO_JS_CARD_20:
                acquisto.setSelfieCoinUsati(2000);
                totale = totale - 20;
                totale = totale < 0 ? 0 : totale;
                omaggioSelezionatoLayout.setVisibility(View.VISIBLE);
                omaggioDisponibileLayout.setVisibility(View.GONE);
                omaggioSelezionatoIcona.setImageResource(R.mipmap.giftcard_20);
                omaggioSelezionatoNome.setText("JS Card da 20€");
                omaggioSelezionatoImporto.setText("-20€");
                break;
            default:
                setupOmaggioDisponibileLayout();
                break;
        }

        SpannableString sommaSpannable = new SpannableString(somma + "");
        sommaSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, (somma + "").length(), 0);

        SpannableString importoSpannable = new SpannableString(f.format(totale) + "€");
        importoSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, f.format(totale).length()+1, 0);

        if(somma > 0){
            snacbKbarTesto.setText("Offerte: ");
            snacbKbarTesto.append(sommaSpannable);
            snacbKbarTesto.append("\n");
            snacbKbarTesto.append("Tot. ");
            snacbKbarTesto.append(importoSpannable);
            snackbar.show();
        }else {
            finish();
        }
        recyclerView.setAdapter(new AdapterCarrello(this, acquisto.getOfferteAcquistate()));
    }



    public void aggiornaAcquisto(AcquistoItem acquistoItem) {
        boolean found;
        for (int i = 0; i < acquisto.getOfferteAcquistate().size(); i++) {
            found = acquisto.getOfferteAcquistate().get(i).getOfferta().getId() == acquistoItem.getOfferta().getId();
            if (found) {
                acquisto.getOfferteAcquistate().get(i).setQuantita(acquistoItem.getQuantita());
                break;
            }
        }
        updateCarrello();
    }

    public void rimuoviAcquisto(AcquistoItem acquistoItem) {
        boolean found ;
        for (int i = 0; i < acquisto.getOfferteAcquistate().size(); i++) {
            found = acquisto.getOfferteAcquistate().get(i).getOfferta().getId() == acquistoItem.getOfferta().getId();
            if (found) {
                acquisto.getOfferteAcquistate().remove(i);
                break;
            }
        }
        updateCarrello();
    }

    public void rimuoviOmaggio(){
        omg = 0;
        acquisto.setSelfieCoinUsati(0);
        updateCarrello();
    }


}
